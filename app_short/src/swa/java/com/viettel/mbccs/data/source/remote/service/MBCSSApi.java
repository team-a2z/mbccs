package com.viettel.mbccs.data.source.remote.service;

import com.viettel.mbccs.data.model.EmptyObject;
import com.viettel.mbccs.data.model.LoginInfo;
import com.viettel.mbccs.data.model.UserInfo;
import com.viettel.mbccs.data.source.remote.request.*;
import com.viettel.mbccs.data.source.remote.response.*;

import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by eo_cuong on 5/10/17.
 */

public interface MBCSSApi {

    @POST("/ApigwGateway/CoreService/UserLogin")
//    @POST("/ApigwGateway/CoreService/UserLogin")
    Observable<LoginInfo> login(@Body LoginRequest loginRequest);

    @POST("/ApigwGateway/CoreService/UserForgotPassword")
    Observable<DataResponse> forgotPassword(@Body DataRequest<UserPassRequest> request);

    @POST("/ApigwGateway/CoreService/UserChangePassword")
    Observable<DataResponse> changePassword(@Body UserPassRequest request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetListOrderResponse>>> getListOrder(
            @Body DataRequest<GetListOrderRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetListChannelByOwnerTypeIdResponse>>> getListChannelByOwnerTypeId(
            @Body DataRequest<GetListChannelByOwnerTypeIdRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetSaleTransTypeResponse>>> getSaleTransType(
            @Body DataRequest<BaseRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetAllInfoResponse>>> WS_GetAllInfo(
            @Body DataRequest<GetAllInfoRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<TelecomServiceAndSaleProgramResponse>>> getTelecomserviceAndSaleProgram(
            @Body DataRequest<GetTelecomServiceAndSaleProgramRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetSerialsResponse>>> getSerials(
            @Body DataRequest<GetSerialRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetOrderInfoResponse>>> getOrderInfo(
            @Body DataRequest<GetOrderInfoRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetReasonResponse>>> getReason(
            @Body DataRequest<GetReasonRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<CreateSaleTransFromOrderResponse>>> createSaleTransFromOrder(
            @Body DataRequest<CreateSaleTransFromOrderRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<UpdateSaleOrderResponse>>> updateSaleOrder(
            @Body DataRequest<UpdateSaleOrderRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetTotalStockResponse>>> getModelSales(
            @Body DataRequest<GetTotalStockRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetInfoSaleTranResponse>>> getSaleTransInfo(
            @Body DataRequest<GetInfoSaleTranRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<CreateSaleTransRetailResponse>>> createSaleTransRetail(
            @Body DataRequest<GetInfoSaleTranRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetDistributedChannelResponse>>> getDistributedChannelInfo(
            @Body DataRequest<GetDistributedChannelRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<CreateDistributedChannelResponse>>> createDistributedChannel(
            @Body DataRequest<CreateDistributedChannelRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<CreateSaleTransChannelResponse>>> createSaleTransChannel(
            @Body DataRequest<CreateSaleTransChannelRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetListStockModelResponse>>> getListStockModel(
            @Body DataRequest<GetListStockModelRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetListStockModelAllResponse>>> getListStockModelAll(
            @Body DataRequest<GetListStockModelRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetListSerialResponse>>> getListSerial(
            @Body DataRequest<GetListSerialRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetListProvinceResponse>>> getListProvince(
            @Body DataRequest<GetListProvinceRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetListTTKDResponse>>> getListTTKD(
            @Body DataRequest<GetListTTKDRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetListShopResponse>>> getListShop(
            @Body DataRequest<GetListShopRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<CreateOrderResponse>>> createSaleOrders(
            @Body DataRequest<KPPOrderRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<RegisterCustomerInfoResponse>>> registerCustomerInfo(
            @Body DataRequest<RegisterCustomerInfoRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetRegisterSubInfoResponse>>> getRegiterSubInfo(
            @Body DataRequest<GetRegisterSubInfoRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetListBusTypeIdRequireResponse>>> getListBusTypeIdRequire(
            @Body DataRequest<GetListBusTypeIdRequireRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetApDomainByTypeResponse>>> getApDomainByType(
            @Body DataRequest<GetApDomainByTypeRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetOTPResponse>>> getOTP(
            @Body DataRequest<GetOTPRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<CheckOTPResponse>>> checOTP(
            @Body DataRequest<CheckOTPRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetAllSubInfoResponse>>> getAllSubInfo(
            @Body DataRequest<GetAllSubInfoRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<UpdateAllSubInfoResponse>>> updateAllSubInfo(
            @Body DataRequest<UpdateAllSubInfoRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetProvinceResponse>>> getProvince(
            @Body DataRequest<GetProvinceRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetDistrictResponse>>> getDistrict(
            @Body DataRequest<GetDistrictRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetPrecinctResponse>>> getPrecinct(
            @Body DataRequest<GetPrecinctRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<CheckIdNoResponse>>> checkIdNo(
            @Body DataRequest<CheckIdNoRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetAnypayAuthorizeResponse>>> getAnypayAuthorize(
            @Body DataRequest<GetAnypayAuthorizeRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<SellAnypayToCustomerResponse>>> saleAnypayToCustomer(
            @Body DataRequest<SellAnypayToCustomerRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<SellAnypayToChannelResponse>>> saleAnypayToChannel(
            @Body DataRequest<SellAnypayToChannelRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<TransferAnyPayResponse>>> transferAnyPay(
            @Body DataRequest<TransferAnyPayRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<RefillAnyPayResponse>>> refillAnyPay(
            @Body DataRequest<RefillAnyPayRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<DataResponse>>> checkChangeSimDebit(
            @Body DataRequest<CheckDebitChangeSimRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<DataResponse>>> checkCalledIsdnChangeSim(
            @Body DataRequest<CheckCalledIsdnsRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<ChangeSimResponse>>> changeSim(
            @Body DataRequest<ChangeSimRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetSurveyKPPResponse>>> getSurveyKPP(
            @Body DataRequest<GetSurveyKPPRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<SendSurveyKPPResponse>>> sendSurveyKPP(
            @Body DataRequest<SendSurveyKPPRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetHotNewsCSKPPResponse>>> getHotNews(
            @Body DataRequest<GetHotNewsCSKPPRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetHotNewsInfoCSKPPResponse>>> getHotNewsInfo(
            @Body DataRequest<GetHotNewsInfoCSKPPRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetKPPFeedbackResponse>>> getKPPFeedback(
            @Body DataRequest<GetKPPFeedbackRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetKPPFeedbackInfoResponse>>> getKPPFeedbackInfo(
            @Body DataRequest<GetKPPFeedbackInfoRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<KPPFeedbackResponse>>> kppFeedback(
            @Body DataRequest<KPPFeedbackRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<KPPRespondFeedbackResponse>>> kppResponseFeedback(
            @Body DataRequest<KPPRespondFeedbackRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<UploadImageResponse>>> uploadImage(
            @Body DataRequest<UploadImageRequest> request);

    //    @POST("/ApigwGateway/CoreService/UserRouting")
    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<UserInfo>>> getUserInfo(
            @Body DataRequest<GetUserInfoRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<DownloadImageResponse>>> downloadImage(
            @Body DataRequest<DownloadImageRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetListIdImageResponse>>> getListIdImage(
            @Body DataRequest<GetListIdImageRequest> request);

    /*get list stock trans detail by tran id*/
    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<ListStockTransDetailsReponse>>> getListStockTransDetail(
            @Body DataRequest<GetListStockTransDetailRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<BaseCreateCmdNoteResponse>>> createExpStock(
            @Body DataRequest<CreateExpStockRequest> requestDataRequest);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetListProductResponse>>> getListProduct(
            @Body DataRequest<GetListProductRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<DataResponse>>> isKppManager(
            @Body DataRequest<IsKPPManagerRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetSurveyKPPResponse>>> getSurveyKPPList(
            @Body DataRequest<GetSurveyKPPRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<EmptyObject>>> sendSurvey(
            @Body DataRequest<SendSurveyKPPRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetListTeamResponse>>> getListTeam(
            @Body DataRequest<GetListTeamRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetListDsLamByTeamIdResponse>>> getListDsLamByTeamId(
            @Body DataRequest<GetListDsLamByTeamIdRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetReceiverChangeAddressResponse>>> receiverChangeAddress(
            @Body AddressRequest<GetReceiverChangeAddressRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetListSearchTransResponse>>> getListSearchTrans(
            @Body DataRequest<GetListSearchTransRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetSaleTransDetailResponse>>> getSaleTransDetail(
            @Body DataRequest<GetSaleTransDetailRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<EmptyObject>>> createInvoiceBill(
            @Body DataRequest<GetCreateInvoiceBillRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetRegisterSubResponse>>> getRegisterSub(
            @Body DataRequest<GetRegisterSubRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<InputOrderResponse>>> getListInvoice(
            @Body DataRequest<InputOrderRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<EmptyObject>>> importInvoiceList(
            @Body DataRequest<InputOrderRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<BaseCreateCmdNoteResponse>>> createExpStockNotNote(
            @Body DataRequest<CreateExpStockNotNoteRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetListRegTypeResponse>>> getListRegType(
            @Body DataRequest<GetListRegTypeRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetListSubTypeResponse>>> getListSubType(
            @Body DataRequest<GetListSubTypeRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<ConnectSubscriberResponse>>> connectSubscriber(
            @Body DataRequest<ConnectSubscriberRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<BaseCreateCmdNoteResponse>>> createImportStock(
            @Body DataRequest<CreateImportStockRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetListExpCmdResponse>>> getListExpCmd(
            @Body DataRequest<GetListExpCmdRequest> requestDataRequest);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<ViewInforSerialResponse>>> viewInfoSerial(
            @Body DataRequest<ViewInforSerialRequest> requestDataRequest);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetListOwneCodeReponse>>> getListOwnerCode(
            @Body DataRequest<GetListOwnerCodeRequest> requestDataRequest);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<BaseCreateCmdNoteResponse>>> createImportCmd(
            @Body DataRequest<CreateImportCmdRequest> requestDataRequest);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<BaseCreateCmdNoteResponse>>> createImportNote(
            @Body DataRequest<CreateImportNoteRequest> requestDataRequest);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<BaseCreateCmdNoteResponse>>> createImportNoteNoCMD(
            @Body DataRequest<CreateImportNoteRequest> requestDataRequest);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<EmptyObject>>> destroyStockTrans(
            @Body DataRequest<DestroyStockTransRequest> requestDataRequest);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetListBusTypeResponse>>> getListBusType(
            @Body DataRequest<GetListBusTypeRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<BaseCreateCmdNoteResponse>>> createExpCmd(
            @Body DataRequest<CreateExpCmdRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<BaseCreateCmdNoteResponse>>> createExpNote(
            @Body DataRequest<CreateExpNoteRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<BaseCreateCmdNoteResponse>>> createExpNoteNoCmd(
            @Body DataRequest<CreateExpNoteNoCmdRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetStockTransSerialDetailResponse>>> getStockTransDetailSerial(
            @Body DataRequest<GetStockTransSerialDetailRequest> request);

    // Assign Task
    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetTaskPrepareAssignStaffResponse>>> getTaskPrepareAssignStaff(
            @Body DataRequest<GetTaskPrepareAssignStaffRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<AssignTaskForStaffResponse>>> assignTaskForStaff(
            @Body DataRequest<AssignTaskForStaffRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetInfoTaskForUpdateResponse>>> getInfoTaskForUpdate(
            @Body DataRequest<GetInfoTaskForUpdateRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetAccessoryForUpdateTaskResponse>>> getAccessoryForUpdateTask(
            @Body DataRequest<GetAccessoryForUpdateTaskRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetStockModelForUpdateTaskResponse>>> getStockModelForUpdateTask(
            @Body DataRequest<GetStockModelForUpdateTaskRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<UpdateTaskResponse>>> updateTask(
            @Body DataRequest<UpdateTaskRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<CloseTaskResponse>>> closeTask(
            @Body DataRequest<CloseTaskRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetStaffToAssignTaskResponse>>> getStaffToAssignTask(
            @Body DataRequest<GetStaffToAssignTaskRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetChannelRouterResponse>>> getChannelRouter(
            @Body DataRequest<GetChannelRouterRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetChannelWorkTypeResponse>>> getChannelWorkType(
            @Body DataRequest<GetChannelWorkTypeRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetListChannelResponse>>> getListChannel(
            @Body DataRequest<GetListChannelRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<CreateTaskExtendResponse>>> createTaskExtend(
            @Body DataRequest<CreateTaskExtendRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetQuotaByProductCodeResponse>>> getQuotaByProductCode(
            @Body DataRequest<GetQuotaByProductCodeRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetBankInfoResponse>>> getBankInfo(
            @Body DataRequest<GetBankInfoRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetCurrBillCycleResponse>>> getCurrBillCycle(
            @Body DataRequest<GetCurrBillCycleRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<SearchProductResponse>>> searchProduct(
            @Body DataRequest<SearchProductRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<SearchAdvancedProductResponse>>> searchAdvancedProduct(
            @Body DataRequest<SearchAdvancedProductRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetProductSearchResponse>>> getProductSearch(
            @Body DataRequest<GetProductSearchRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetProductInfoResponse>>> getProductInfo(
            @Body DataRequest<GetProductInfoRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetListApParamsResponse>>> getApParam(
            @Body DataRequest<GetListApParamsRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetChangeSimPriceResponse>>> getChangeSimPrice(
            @Body DataRequest<GetChangeSimPriceRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<FindShopByNameResponse>>> findShopByName(
            @Body DataRequest<FindShopByNameRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<FindStaffByNameResponse>>> findStaffByName(
            @Body DataRequest<FindStaffByNameRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<FindChannelByNameResponse>>> findChannelByName(
            @Body DataRequest<FindChannelByNameRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetListStockTypeBySaleServiceCodeResponse>>> getListStockTypeBySaleServiceCode(
            @Body DataRequest<GetListStockTypeBySaleServiceCodeRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetListChannelTypeResponse>>> getListChannelType(
            @Body DataRequest<GetListChannelTypeRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetVasForIsdnResponse>>> getVasForIsdn(
            @Body DataRequest<GetVasForIsdnRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetVasInfoResponse>>> getVasInfo(
            @Body DataRequest<GetVasInfoRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<RegisterVasResponse>>> registerVas(
            @Body DataRequest<RegisterVasRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetListBtsesResponse>>> getListBtses(
            @Body DataRequest<GetListBtsesRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetDashBoardInfoResponse>>> getDashBoardInfo(
            @Body DataRequest<GetDashBoardInfoRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetChannelByLocationResponse>>> getChannelByLocation(
            @Body DataRequest<GetChannelByLocationRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetChannelDetailResponse>>> getChannelDetail(
            @Body DataRequest<GetChannelDetailRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<UpdateChannelInfoResponse>>> updateChannelInfo(
            @Body DataRequest<UpdateChannelInfoRequest> request);

    @POST("/ApigwGateway/CoreService/UserLogout")
    Observable<LogoutResponse> logout(@Body LogoutRequest loginRequest);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetListSerialOfOrderResponse>>> getListSerialOfOrder(
            @Body DataRequest<GetListSerialOfOrderRequest> request);

    @POST("/ApigwGateway/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<IsSupervisorResponse>>> isSupervisor(
            @Body DataRequest<IsSupervisorRequest> request);

    @POST("/JsonAPI/webresources/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetAreaInfoResponse>>> getAreaInfo(
            @Body DataRequest<GetAreaInfoRequest> request);

    @POST("/JsonAPI/webresources/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetListBtsesByAreaCodeResponse>>> getListBtsesByAreaCode(
            @Body DataRequest<GetListBtsesByAreaCodeRequest> request);

    @POST("/JsonAPI/webresources/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetListShopsByAreaCodeResponse>>> getListShopsByAreaCode(
            @Body DataRequest<GetListShopsByAreaCodeRequest> request);

    @POST("/JsonAPI/webresources/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<GetListEventsByAreaCodeResponse>>> getListEventsByAreaCode(
            @Body DataRequest<GetListEventsByAreaCodeRequest> request);

    @POST("/JsonAPI/webresources/CoreService/UserRouting")
    Observable<ServerDataResponse<BaseResponse<UpdateBtsGpsResponse>>> updateBtsGps(
            @Body DataRequest<UpdateBtsGpsRequest> request);
}

