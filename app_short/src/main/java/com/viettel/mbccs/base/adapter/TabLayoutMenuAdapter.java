package com.viettel.mbccs.base.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;

import java.util.List;

/**
 * Created by minhnx on 6/25/17.
 */

public class TabLayoutMenuAdapter extends FragmentPagerAdapter {
    private List<String> lstTitles;
    private List<Fragment> lstFragments;

    public TabLayoutMenuAdapter(FragmentManager fm) {
        super(fm);
    }

    public TabLayoutMenuAdapter(FragmentManager fm, List<String> titles, List<Fragment> fragments) {
        super(fm);
        this.lstTitles = titles;
        this.lstFragments = fragments;
    }

    @Override
    public int getItemPosition(Object object) {
        return PagerAdapter.POSITION_NONE;
    }

    public void setData(List<String> titles, List<Fragment> fragments) {
        this.lstTitles = titles;
        this.lstFragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return lstFragments.get(position);
    }

    @Override
    public int getCount() {
        return this.lstTitles == null ? 0 : this.lstTitles.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return lstTitles.get(position);
    }
}
