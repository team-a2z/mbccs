package com.viettel.mbccs.base.filterdialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.viettel.mbccs.R;
import com.viettel.mbccs.callback.OnListenerItemRecyclerView;
import com.viettel.mbccs.databinding.ItemFilterBinding;
import com.viettel.mbccs.widget.viewholderbinding.BaseRecyclerViewAdapterBinding;
import com.viettel.mbccs.widget.viewholderbinding.BaseViewHolderBinding;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by eo_cuong on 7/18/17.
 */

public class FilterAdapter
        extends BaseRecyclerViewAdapterBinding<FilterAdapter.FilterViewHolder, String> {

    private boolean hasContainAll = false;

    List<String> fullData = new ArrayList<>();

    OnListenerItemRecyclerView<String> mOnListenerItemRecyclerView;

    public void setOnListenerItemRecyclerView(
            OnListenerItemRecyclerView onListenerItemRecyclerView) {
        mOnListenerItemRecyclerView = onListenerItemRecyclerView;
    }

    public FilterAdapter(Context context, List<String> list) {
        super(context, list);
        fullData.addAll(list);
    }

    public FilterAdapter(Context context, List<String> list, boolean hasContainAll) {
        super(context, list);
        this.hasContainAll = hasContainAll;
        fullData.addAll(list);
        if (hasContainAll){
            mList.add(0, mContext.getString(R.string.all_));
        }

    }

    public void filter(String s) {
        mList.clear();
        for (String st : fullData) {
            if (st.toLowerCase().contains(s.toLowerCase())) {
                mList.add(st);
            }
        }

        if (mList.size() == fullData.size() && hasContainAll) {
            mList.add(0, mContext.getString(R.string.all_));
        }

        notifyDataSetChanged();
    }

    @Override
    protected FilterViewHolder getViewHolder(ViewGroup parent) {
        return new FilterViewHolder(
                ItemFilterBinding.inflate(LayoutInflater.from(mContext), parent, false));
    }

    class FilterViewHolder extends BaseViewHolderBinding<ItemFilterBinding, String> {

        public FilterViewHolder(ItemFilterBinding binding) {
            super(binding);
            mBinding.layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnListenerItemRecyclerView != null) {
                        mOnListenerItemRecyclerView.onClickItem(mList.get(getAdapterPosition()),
                                getAdapterPosition());
                    }
                }
            });
        }

        @Override
        public void bindData(String s) {
            super.bindData(s);

            mBinding.setText(s);
        }
    }
}
