package com.viettel.mbccs.base;

import android.content.Context;
import android.content.res.Resources;
import android.databinding.ObservableBoolean;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.viettel.mbccs.R;
import com.viettel.mbccs.data.model.Function;
import com.viettel.mbccs.data.model.LoginInfo;
import com.viettel.mbccs.data.source.UserRepository;
import com.viettel.mbccs.databinding.FragmentSubMenuBinding;
import com.viettel.mbccs.databinding.ItemGridMenuBinding;
import com.viettel.mbccs.databinding.ItemMenuBinding;
import com.viettel.mbccs.screen.main.fragments.menu.MenuPresenter;
import com.viettel.mbccs.utils.ActivityUtils;
import com.viettel.mbccs.utils.GsonUtils;
import com.viettel.mbccs.variable.Constants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Anh Vu Viet on 5/19/2017.
 */

public class BaseSubMenuFragment extends BaseDataBindFragment<FragmentSubMenuBinding, BaseSubMenuFragment> {

    public static final String EXTRA_MENU_ITEM = "EXTRA_MENU_ITEM";

    protected Function mFunction;
    protected List<Function> mFunctionList = new ArrayList<>();

    protected GridLayoutManager mGridLayoutManager;
    protected LinearLayoutManager mLinearLayoutManager;

    protected MenuPresenter.MenuAdapter mMenuAdapter;
    protected MenuPresenter.OnMenuClickListener mOnMenuClickListener;

    public ObservableBoolean isGrid;

    private List<String> functionCodeList = null;

    public static BaseSubMenuFragment getInstance(Function function, List<String> functionCodeList) {

        Bundle args = new Bundle();
        args.putParcelable(BaseSubMenuActivity.EXTRA_MENU_ITEM, function);
        args.putString(Constants.BundleConstant.ITEM_LIST, GsonUtils.Object2String(functionCodeList));

        BaseSubMenuFragment fragment = new BaseSubMenuFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    protected int getIdLayoutRes() {
        return R.layout.fragment_sub_menu;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {
        try {
            mFunction = getArguments().getParcelable(EXTRA_MENU_ITEM);

            if (getArguments().getString(Constants.BundleConstant.ITEM_LIST) != null)
                functionCodeList = GsonUtils.String2ListObject(getArguments().getString(Constants.BundleConstant.ITEM_LIST), String[].class);

            isGrid = new ObservableBoolean();
            isGrid.set(false);

            mGridLayoutManager = new GridLayoutManager(getContext(), 3);
            mLinearLayoutManager = new LinearLayoutManager(getContext());

            initMenuList();

            mMenuAdapter = new SubMenuAdapter(getContext(), mFunctionList);
            mBinding.setPresenter(this);
            mBinding.executePendingBindings();

            switchView();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initMenuList() {
        try {
            LoginInfo loginInfo = UserRepository.getInstance().getUser();

            if (functionCodeList != null) {
                for (Function f : loginInfo.getFunction()) {
                    if (f.getParentCode().equals(mFunction.getFunctionCode()) && functionCodeList.contains(f.getFunctionCode())) {
                        if (!mFunctionList.contains(f)) {
                            mFunctionList.add(f);
                        }
                    }
                }
            } else {
                for (Function f : loginInfo.getFunction()) {
                    if (f.getParentCode().equals(mFunction.getFunctionCode())) {
                        if (!mFunctionList.contains(f)) {
                            mFunctionList.add(f);
                        }
                    }
                }
            }

            Comparator<Function> comparator = new Comparator<Function>() {
                @Override
                public int compare(Function left, Function right) {
                    return left.getOrderIndex() - right.getOrderIndex();
                }
            };

            Collections.sort(mFunctionList, comparator);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public MenuPresenter.OnMenuClickListener getOnMenuClickListener() {
        if (mOnMenuClickListener == null) {
            mOnMenuClickListener = new MenuPresenter.OnMenuClickListener() {
                @Override
                public void onMenuClick(Function item) {
                    ActivityUtils.gotoMenu(getContext(), item.getFunctionCode());
                }
            };
        }
        return mOnMenuClickListener;
    }

    public String getToolbarTitle() {
        try {
            return getString(mFunction.getFunctionNameId());
        } catch (Resources.NotFoundException e) {
            return mFunction.getFunctionName();
        }
    }

    public void switchView() {
        isGrid.set(!isGrid.get());
        if (isGrid.get()) {
            mBinding.subMenuView.setLayoutManager(mGridLayoutManager);
            //            mBinding.subMenuView.addItemDecoration(mItemDecoration);
        } else {
            mBinding.subMenuView.setLayoutManager(mLinearLayoutManager);
            //            mBinding.subMenuView.removeItemDecoration(mItemDecoration);
        }
        mBinding.subMenuView.setAdapter(mMenuAdapter);
    }

    public MenuPresenter.MenuAdapter getMenuAdapter() {
        return mMenuAdapter;
    }

    public class SubMenuAdapter extends MenuPresenter.MenuAdapter {
        public SubMenuAdapter(Context context, List<Function> list) {
            super(context, list);
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (!isGrid.get()) {
                return new SubMenuListViewHolder(ItemMenuBinding.inflate(getLayoutInflater(), parent, false));
            } else {
                return new SubMenuGridViewHolder(ItemGridMenuBinding.inflate(getLayoutInflater(), parent, false));
            }
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if (!isGrid.get()) {
                ((SubMenuListViewHolder) holder).bind(mList.get(position));
            } else {
                ((SubMenuGridViewHolder) holder).bind(mList.get(position));
            }
        }
    }

    public class SubMenuListViewHolder extends MenuPresenter.MenuAdapter.MenuViewHolder {

        public SubMenuListViewHolder(ItemMenuBinding binding) {
            super(binding);
        }

        @Override
        protected MenuPresenter.OnMenuClickListener getOnMenuClickListener() {
            return BaseSubMenuFragment.this.getOnMenuClickListener();
        }
    }

    public class SubMenuGridViewHolder extends RecyclerView.ViewHolder {
        private ItemGridMenuBinding binding;

        private MenuPresenter.OnMenuClickListener onMenuClickListener;

        public SubMenuGridViewHolder(ItemGridMenuBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            onMenuClickListener = getOnMenuClickListener();
        }

        public void bind(final Function item) {
            try {
                binding.setImageText(itemView.getContext().getString(item.getFunctionNameId()));
            } catch (Resources.NotFoundException e) {
                binding.setImageText(item.getFunctionName());
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                binding.setImage(binding.getRoot().getResources().getDrawable(item.getIcon(), null));
            } else {
                binding.setImage(binding.getRoot().getResources().getDrawable(item.getIcon()));
            }
            binding.setOnClicked(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onMenuClickListener != null) {
                        onMenuClickListener.onMenuClick(item);
                    }
                }
            });
        }
    }
}
