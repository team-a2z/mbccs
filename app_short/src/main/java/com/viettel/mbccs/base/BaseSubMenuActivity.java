package com.viettel.mbccs.base;

import android.content.Context;
import android.content.res.Resources;
import android.databinding.ObservableBoolean;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.viettel.mbccs.R;
import com.viettel.mbccs.base.adapter.TabLayoutMenuAdapter;
import com.viettel.mbccs.data.model.Function;
import com.viettel.mbccs.data.model.LoginInfo;
import com.viettel.mbccs.data.source.UserRepository;
import com.viettel.mbccs.databinding.ActivitySubMenuBinding;
import com.viettel.mbccs.databinding.ItemGridMenuBinding;
import com.viettel.mbccs.databinding.ItemMenuBinding;
import com.viettel.mbccs.screen.main.fragments.menu.MenuPresenter;
import com.viettel.mbccs.utils.ActivityUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Anh Vu Viet on 5/19/2017.
 */

public class BaseSubMenuActivity extends BaseDataBindActivity<ActivitySubMenuBinding, BaseSubMenuActivity> {

    public static final String EXTRA_MENU_ITEM = "EXTRA_MENU_ITEM";

    protected Function mFunction;
    protected List<Function> mFunctionList = new ArrayList<>();

    protected GridLayoutManager mGridLayoutManager;
    protected LinearLayoutManager mLinearLayoutManager;

    //    protected RecyclerView.ItemDecoration mItemDecoration;

    protected MenuPresenter.MenuAdapter mMenuAdapter;
    protected MenuPresenter.OnMenuClickListener mOnMenuClickListener;

    public ObservableBoolean isGrid;
    public ObservableBoolean showTabLayout;

    private TabLayoutMenuAdapter fragmentAdapter;

    public BaseSubMenuActivity() {
        try {
            fragmentAdapter = new TabLayoutMenuAdapter(getSupportFragmentManager(), null, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected int getIdLayout() {
        return R.layout.activity_sub_menu;
    }

    @Override
    protected void initData() {
        try {
            mFunction = getIntent().getParcelableExtra(EXTRA_MENU_ITEM);
            isGrid = new ObservableBoolean();
            showTabLayout = new ObservableBoolean();
            isGrid.set(false);

            mBinding.tabLayout.setupWithViewPager(mBinding.vpPager);

            mGridLayoutManager = new GridLayoutManager(this, 3);
            mLinearLayoutManager = new LinearLayoutManager(this);
            //        mItemDecoration = new SpacesItemDecoration(
            //                (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX,
            //                        getResources().getDimension(R.dimen.dp_0_6),
            //                        getResources().getDisplayMetrics()), mGridLayoutManager);
            initMenuList();
            mMenuAdapter = new SubMenuAdapter(this, mFunctionList);
            mBinding.setPresenter(this);
            mBinding.executePendingBindings();
            switchView();

            if (mFunction.getFunctionCode().equals(Function.TopMenu.MENU_QUAN_LY_KHO))
                showTabLayout.set(true);

            List<String> lstTitles = new ArrayList<>();
            List<Fragment> lstFragments = new ArrayList<>();

            String[] titles = getResources().getStringArray(R.array.manage_warehouse_tabs_desc);
            if (titles != null) {
                for (String title : titles) {
                    lstTitles.add(title);
                }
            }

            List<String> viewStockFuncCode1 = new ArrayList<>();
            viewStockFuncCode1.add(Function.MenuId.MENU_XUAT_KHO_CAP_DUOI);
            viewStockFuncCode1.add(Function.MenuId.MENU_NHAP_KHO_CAP_TREN);
            viewStockFuncCode1.add(Function.MenuId.MENU_TRA_HANG_CAP_TREN);
            viewStockFuncCode1.add(Function.MenuId.MENU_NHAP_KHO_CAP_DUOI);

            List<String> viewStockFuncCode2 = new ArrayList<>();
            viewStockFuncCode2.add(Function.MenuId.MENU_NV_XAC_NHAN_HANG);
            viewStockFuncCode2.add(Function.MenuId.MENU_NHAN_VIEN_TRA_HANG_CAP_TREN);
            viewStockFuncCode2.add(Function.MenuId.MENU_NHAP_KHO_TU_NHAN_VIEN);
            viewStockFuncCode2.add(Function.MenuId.MENU_XUAT_KHO_CHO_NHAN_VIEN);

            List<String> viewStockFuncCode3 = new ArrayList<>();
            viewStockFuncCode3.add(Function.MenuId.MENU_XEM_KHO);
            viewStockFuncCode3.add(Function.MenuId.MENU_KENH_ORDER_HANG);
            viewStockFuncCode3.add(Function.MenuId.MENU_NHAP_HOA_DON);

            lstFragments.add(BaseSubMenuFragment.getInstance(mFunction, viewStockFuncCode1));
            lstFragments.add(BaseSubMenuFragment.getInstance(mFunction, viewStockFuncCode2));
            lstFragments.add(BaseSubMenuFragment.getInstance(mFunction, viewStockFuncCode3));

            fragmentAdapter.setData(lstTitles, lstFragments);
            fragmentAdapter.notifyDataSetChanged();
            mBinding.vpPager.setOffscreenPageLimit(1);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initMenuList() {
        try {
            LoginInfo loginInfo = UserRepository.getInstance().getUser();
            for (Function f : loginInfo.getFunction()) {
                if (f.getParentCode().equals(mFunction.getFunctionCode())) {
                    if (!mFunctionList.contains(f)) {
                        mFunctionList.add(f);
                    }
                }
            }

            Comparator<Function> comparator = new Comparator<Function>() {
                @Override
                public int compare(Function left, Function right) {
                    return left.getOrderIndex() - right.getOrderIndex();
                }
            };

            Collections.sort(mFunctionList, comparator);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public MenuPresenter.OnMenuClickListener getOnMenuClickListener() {
        if (mOnMenuClickListener == null) {
            mOnMenuClickListener = new MenuPresenter.OnMenuClickListener() {
                @Override
                public void onMenuClick(Function item) {
                    ActivityUtils.gotoMenu(BaseSubMenuActivity.this, item.getFunctionCode());
                }
            };
        }
        return mOnMenuClickListener;
    }

    public String getToolbarTitle() {
        try {
            return getString(mFunction.getFunctionNameId());
        } catch (Resources.NotFoundException e) {
            return mFunction.getFunctionName();
        }
    }

    public void switchView() {
        isGrid.set(!isGrid.get());
        if (isGrid.get()) {
            mBinding.subMenuView.setLayoutManager(mGridLayoutManager);
            //            mBinding.subMenuView.addItemDecoration(mItemDecoration);
        } else {
            mBinding.subMenuView.setLayoutManager(mLinearLayoutManager);
            //            mBinding.subMenuView.removeItemDecoration(mItemDecoration);
        }
        mBinding.subMenuView.setAdapter(mMenuAdapter);
    }

    public MenuPresenter.MenuAdapter getMenuAdapter() {
        return mMenuAdapter;
    }

    public class SubMenuAdapter extends MenuPresenter.MenuAdapter {
        public SubMenuAdapter(Context context, List<Function> list) {
            super(context, list);
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (!isGrid.get()) {
                return new SubMenuListViewHolder(ItemMenuBinding.inflate(getLayoutInflater(), parent, false));
            } else {
                return new SubMenuGridViewHolder(ItemGridMenuBinding.inflate(getLayoutInflater(), parent, false));
            }
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if (!isGrid.get()) {
                ((SubMenuListViewHolder) holder).bind(mList.get(position));
            } else {
                ((SubMenuGridViewHolder) holder).bind(mList.get(position));
            }
        }
    }

    public class SubMenuListViewHolder extends MenuPresenter.MenuAdapter.MenuViewHolder {

        public SubMenuListViewHolder(ItemMenuBinding binding) {
            super(binding);
        }

        @Override
        protected MenuPresenter.OnMenuClickListener getOnMenuClickListener() {
            return BaseSubMenuActivity.this.getOnMenuClickListener();
        }
    }

    public class SubMenuGridViewHolder extends RecyclerView.ViewHolder {
        private ItemGridMenuBinding binding;

        private MenuPresenter.OnMenuClickListener onMenuClickListener;

        public SubMenuGridViewHolder(ItemGridMenuBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            onMenuClickListener = getOnMenuClickListener();
        }

        public void bind(final Function item) {
            try {
                binding.setImageText(itemView.getContext().getString(item.getFunctionNameId()));
            } catch (Resources.NotFoundException e) {
                binding.setImageText(item.getFunctionName());
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                binding.setImage(binding.getRoot().getResources().getDrawable(item.getIcon(), null));
            } else {
                binding.setImage(binding.getRoot().getResources().getDrawable(item.getIcon()));
            }
            binding.setOnClicked(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onMenuClickListener != null) {
                        onMenuClickListener.onMenuClick(item);
                    }
                }
            });
        }
    }

    public TabLayoutMenuAdapter getFragmentAdapter() {
        return fragmentAdapter;
    }
}
