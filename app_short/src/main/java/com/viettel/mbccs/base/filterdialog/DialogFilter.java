package com.viettel.mbccs.base.filterdialog;

import android.databinding.Observable;
import android.databinding.ObservableField;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.viettel.mbccs.R;
import com.viettel.mbccs.base.BaseFragmentDialogFullScreen;
import com.viettel.mbccs.callback.OnListenerItemRecyclerView;
import com.viettel.mbccs.databinding.DialogFilterBinding;
import com.viettel.mbccs.variable.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eo_cuong on 7/18/17.
 */

public class DialogFilter<T> extends BaseFragmentDialogFullScreen {

    private onDialogFilterListener<T> mOnDialogFilterListener;
    private DialogFilterBinding mBinding;
    private List<T> data = new ArrayList<>();
    private List<String> dataString = new ArrayList<>();
    private List<String> dataStringRoot = new ArrayList<>();
    private boolean hasContainAll = false;

    private FilterAdapter mAdapter;

    public ObservableField<String> toolbarTitle = new ObservableField<>();

    public ObservableField<String> text = new ObservableField<>();

    public static DialogFilter newInstance(boolean hasContainAll) {
        DialogFilter dialogFilter = new DialogFilter();
        Bundle bundle = new Bundle();
        bundle.putBoolean(Constants.BundleConstant.CONTAIN_ALL, hasContainAll);
        dialogFilter.setArguments(bundle);
        return dialogFilter;
    }

    public static DialogFilter newInstance() {
        return new DialogFilter();
    }

    public void setTitle(String title) {
        if (toolbarTitle != null) {
            toolbarTitle.set(title);
        }
    }

    public void setData(List<T> list) {
        data.clear();
        data.addAll(list);
        setDataString();
    }

    private void setDataString() {
        dataString.clear();
        for (T t : data) {

            if (t == null)
                continue;

            dataString.add(t.toString());
            dataStringRoot.add(t.toString());
        }

        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    public void setOnDialogFilterListener(onDialogFilterListener<T> onDialogFilterListener) {
        mOnDialogFilterListener = onDialogFilterListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mBinding = DialogFilterBinding.inflate(inflater, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            hasContainAll = bundle.getBoolean(Constants.BundleConstant.CONTAIN_ALL);
        }

        mAdapter = new FilterAdapter(getActivity(), dataString, hasContainAll);
        text.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                mAdapter.filter(text.get());
            }
        });

        mAdapter.setOnListenerItemRecyclerView(new OnListenerItemRecyclerView<String>() {
            @Override
            public void onClickItem(String st, int position) {

                if (hasContainAll && st.equals(getString(R.string.all_))) {
                    mOnDialogFilterListener.onItemSelected(-1, null);
                    dismiss();
                    return;
                }

                for (int i = 0; i < dataStringRoot.size(); i++) {
                    if (st.trim().toLowerCase().equals(dataStringRoot.get(i).trim().toLowerCase())) {
                        mOnDialogFilterListener.onItemSelected(i, data.get(i));
                        dismiss();
                    }
                }
            }
        });

        mBinding.setPresenter(this);
    }

    public void onClose() {
        dismiss();
    }

    public interface onDialogFilterListener<T> {
        void onItemSelected(int position, T t);
    }

    public FilterAdapter getAdapter() {
        return mAdapter;
    }
}
