package com.viettel.mbccs.utils;

import com.viettel.mbccs.data.model.StockTransDetail;

import java.util.Comparator;

/**
 * Created by eo_cuong on 6/25/17.
 */

public class StockTransDetailCompare implements Comparator<StockTransDetail> {
    @Override
    public int compare(StockTransDetail s1, StockTransDetail s2) {
        return s1.getStockModelCode().compareTo(s2.getStockModelCode());
    }
}
