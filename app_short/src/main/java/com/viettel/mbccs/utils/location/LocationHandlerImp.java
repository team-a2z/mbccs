package com.viettel.mbccs.utils.location;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HuyQuyet on 3/11/17.
 */

public class LocationHandlerImp implements LocationHandler, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener {
    private static LocationHandlerImp INSTANCE;
    private Context context;
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private final List<AppLocationListener> appLocationListenerList;

    public static LocationHandler getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new LocationHandlerImp(context.getApplicationContext());
        }
        return INSTANCE;
    }

    public LocationHandlerImp(Context context) {
        this.context = context;
        appLocationListenerList = new ArrayList<>();
        buildGoogleApiClient();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (location == null) {
            locationRequest = new LocationRequest().setInterval(LocationConstants.LOCATION_FASTEST_UPDATE_INTERVAL)
                    .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                    .setSmallestDisplacement(LocationConstants.SMALLEST_DISTANT_UPDATE_METER);
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest,
                    this);
        } else {
            for (AppLocationListener appLocationListener : appLocationListenerList) {
                appLocationListener.onAppLocationChanged(location);
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        for (AppLocationListener appLocationListener : appLocationListenerList) {
            appLocationListener.onAppLocationConnectFailed(result);
        }
    }

    private void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(context).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onLocationClientConnect() {
        if (googleApiClient.isConnected()) {
            try {
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                                != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                if (location == null) {
                    locationRequest =
                            new LocationRequest().setInterval(LocationConstants.LOCATION_FASTEST_UPDATE_INTERVAL)
                                    .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                                    .setSmallestDisplacement(
                                            LocationConstants.SMALLEST_DISTANT_UPDATE_METER);
                    LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient,
                            locationRequest, this);
                } else {
                    for (AppLocationListener appLocationListener : appLocationListenerList) {
                        appLocationListener.onAppLocationChanged(location);
                        Log.i("LocationHandlerImp", "onLocationClientConnect --------------------: " + appLocationListener);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                googleApiClient.connect();
            }

        } else {
            googleApiClient.connect();
        }
    }

    @Override
    public GoogleApiClient getGoogleApiClient() {
        return googleApiClient;
    }

    @Override
    public boolean isConnected() {
        return googleApiClient.isConnected();
    }

    @Override
    public boolean isLocationServiceAvailable() {
        try {
            LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

            return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
                    || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public void onLocationChanged(Location location) {
        for (AppLocationListener appLocationListener : appLocationListenerList) {
            appLocationListener.onAppLocationChanged(location);
        }
    }

    @Override
    public void addListener(AppLocationListener appLocationListener) {
        synchronized (appLocationListenerList) {
            if (appLocationListenerList.contains(appLocationListener)) return;
            appLocationListenerList.add(appLocationListener);
        }
    }

    @Override
    public void removeListener(AppLocationListener appLocationListener) {
        synchronized (appLocationListenerList) {
            appLocationListenerList.remove(appLocationListener);
        }
    }
}
