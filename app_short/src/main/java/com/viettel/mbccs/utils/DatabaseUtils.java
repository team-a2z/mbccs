package com.viettel.mbccs.utils;

import android.graphics.Bitmap;

import com.activeandroid.query.Delete;
import com.viettel.mbccs.constance.UploadImageField;
import com.viettel.mbccs.data.model.ImageSelect;
import com.viettel.mbccs.data.model.database.UploadImage;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by HuyQuyet on 7/7/17.
 */

public class DatabaseUtils {

    public static List<String> getBitmapAndSaveDatabase(String custId, List<String> imageName,
                                                        List<ImageSelect> imageSelectList) {
        List<String> uploadImageList = new ArrayList<>();

        for (int i = 0; i < imageSelectList.size(); i++) {
            if (!imageSelectList.get(i).isFlag()) continue;
            UploadImage uploadImage =
                    setDataUploadImage(custId, imageName.get(i), imageSelectList.get(i).getContent());
            uploadImage.save();
            uploadImageList.add(String.valueOf(uploadImage.getImageName()));
        }
        return uploadImageList;
    }

    public static List<String> getBitmapAndSaveDatabase(String key, List<String> imageNames, Bitmap... images) {

        if (imageNames.size() != images.length)
            throw new InvalidParameterException("Image names and bitmaps are not the same length");

        List<String> uploadImageList = new ArrayList<>();
        int index = 0;

//        for (Bitmap image : images) {
//
//            if (image != null) {
//
//                UploadImage uploadImage =
//                        setDataUploadImage(key, imageNames.get(index), image, UploadImage.ImageType.FRONT);
//
//                new Delete().from(UploadImage.class).where(UploadImageField.IMAGE_NAME + " = ?", uploadImage.getImageName()).execute();
//
//                uploadImage.save();
//                uploadImageList.add(String.valueOf(uploadImage.getImageName()));
//
//                index++;
//            }
//
//        }

        return uploadImageList;
    }

    //    private static UploadImage setDataUploadImage(String custId, String name, Bitmap bitmap,
//                                                  @UploadImage.ImageType int imageType) {
//        String imageBase64 =
//                ImageUtils.encodeBitmapToBase64(bitmap, Bitmap.CompressFormat.JPEG, 75);
    private static UploadImage setDataUploadImage(String custId, String name, String content) {
        UploadImage uploadImage = new UploadImage();
        uploadImage.setIdImage(name);
        uploadImage.setContent(content);
        uploadImage.setImageType("jpg");
        uploadImage.setCustId(custId);
        uploadImage.setStatus(UploadImage.StatusUpload.WAITING);
        return uploadImage;
    }
}
