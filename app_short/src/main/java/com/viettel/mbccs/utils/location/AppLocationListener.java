package com.viettel.mbccs.utils.location;

import android.location.Location;
import android.support.annotation.NonNull;
import com.google.android.gms.common.ConnectionResult;

/**
 * Created by HuyQuyet on 3/15/17.
 */

public interface AppLocationListener {
    void onAppLocationChanged(Location location);

    void onAppLocationConnectFailed(@NonNull ConnectionResult result);
}
