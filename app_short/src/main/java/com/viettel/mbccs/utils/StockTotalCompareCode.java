package com.viettel.mbccs.utils;

import com.viettel.mbccs.data.model.StockTotal;

import java.util.Comparator;

/**
 * Created by eo_cuong on 6/25/17.
 */

public class StockTotalCompareCode implements Comparator<StockTotal> {
    @Override
    public int compare(StockTotal stockTotal, StockTotal t1) {
        return stockTotal.getStockModelCode().compareTo(t1.getStockModelCode());
    }
}
