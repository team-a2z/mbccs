package com.viettel.mbccs.utils.rx;

import android.app.Activity;

import com.viettel.mbccs.data.source.remote.response.BaseException;

import rx.Subscriber;

public abstract class CustomMBCCSSubscribe<T> extends Subscriber<T> {

    private Activity mContext;

    public CustomMBCCSSubscribe() {
        super();
    }

    public CustomMBCCSSubscribe(Activity context) {
        super();
        mContext = context;
    }

    private T object;

    @Override
    public void onCompleted() {
        onRequestFinish();
        onSuccess(object);
    }

    @Override
    public void onError(Throwable e) {
        onRequestFinish();
        BaseException exception;
        if (e instanceof BaseException) {
            exception = (BaseException) e;
        } else {
            exception = BaseException.toUnexpectedError(e);
        }
        onError(exception);
    }

    @Override
    public void onNext(T t) {
        object = t;
    }

    public abstract void onSuccess(T object);

    public abstract void onError(BaseException error);

    /**
     * Runs after request complete or error
     **/
    public void onRequestFinish() {

    }
}