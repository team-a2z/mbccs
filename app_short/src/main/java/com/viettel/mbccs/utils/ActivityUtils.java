package com.viettel.mbccs.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.google.common.base.Preconditions;
import com.viettel.mbccs.data.model.Function;
import com.viettel.mbccs.data.model.TaskShopManagement;
import com.viettel.mbccs.screen.assigntask.ListAssignTaskActivity;
import com.viettel.mbccs.screen.assigntask.ListUpdateTaskActivity;

/**
 * This provides methods to help Activities load their UI.
 */
public class ActivityUtils {

    public static final int GALLERY_REQUEST_CODE = 100;

    /**
     * The {@code fragment} is added to the container view with id {@code frameId}. The operation
     * is
     * performed by the {@code fragmentManager}.
     */
    public static void addFragmentToActivity(@NonNull FragmentManager fragmentManager,
                                             @NonNull Fragment fragment, int frameId) {
        Preconditions.checkNotNull(fragmentManager);
        Preconditions.checkNotNull(fragment);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(frameId, fragment);
        transaction.commit();
    }

    //public static void replaceFragment(@NonNull Activity activity, @NonNull Fragment fragment) {
    //    Preconditions.checkNotNull(activity);
    //    Preconditions.checkNotNull(fragment);
    //    FragmentManager fragmentManager = ((FragmentActivity) activity).getSupportFragmentManager();
    //    FragmentTransaction transaction = fragmentManager.beginTransaction();
    //    transaction.replace(R.id.fragment_container, fragment);
    //    transaction.commit();
    //}
    //
    //public static void replaceFragment(@NonNull Activity activity, @NonNull Fragment fragment,
    //        boolean isBackStack) {
    //    Preconditions.checkNotNull(activity);
    //    Preconditions.checkNotNull(fragment);
    //    FragmentManager fragmentManager = ((FragmentActivity) activity).getSupportFragmentManager();
    //    FragmentTransaction transaction = fragmentManager.beginTransaction();
    //    if (isBackStack) {
    //        transaction.addToBackStack(fragment.getClass().getSimpleName());
    //    }
    //    transaction.replace(R.id.fragment_container, fragment, fragment.getClass().getName())
    //            .commitAllowingStateLoss();
    //}
    //
    //public static void nextChildFragment(@Nullable Fragment parentFragment,
    //        @Nullable Fragment childFragment, boolean isBackStack) {
    //    Preconditions.checkNotNull(parentFragment);
    //    Preconditions.checkNotNull(childFragment);
    //    FragmentTransaction transaction =
    //            parentFragment.getChildFragmentManager().beginTransaction();
    //    if (isBackStack) {
    //        transaction.addToBackStack(childFragment.getClass().getSimpleName());
    //    }
    //    transaction.replace(R.id.frame_container, childFragment,
    //            childFragment.getClass().getSimpleName());
    //    transaction.commitAllowingStateLoss();
    //    parentFragment.getChildFragmentManager().executePendingTransactions();
    //}

    public static void hideKeyboard(@NonNull Activity activity) {
        View view = activity.getCurrentFocus();
        if (view == null) {
            return;
        }
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static boolean isAccessGalleryRequest(@NonNull Activity activity) {
        int permission = ContextCompat.checkSelfPermission(activity,
                Manifest.permission.READ_EXTERNAL_STORAGE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    GALLERY_REQUEST_CODE);
        }
        return permission == PackageManager.PERMISSION_GRANTED;
    }

    public static void removeFragmentByTag(@NonNull Activity activity, @NonNull String tag) {
        if (activity.isFinishing()) return;
        FragmentManager fragmentManager = ((FragmentActivity) activity).getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(tag);
        if (fragment != null) {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.remove(fragment).commitAllowingStateLoss();
            fragmentManager.popBackStack();
        }
    }

    //public static void supportStatusBarUnderKitkat(@NonNull Activity activity,
    //        @NonNull Toolbar toolbar) {
    //    toolbar.setPadding(0, getStatusBarHeight(activity), 0, 0);
    //    tintColorSupport(activity);
    //}

    //private static void tintColorSupport(@NonNull Activity activity) {
    //    // create our manager instance after the content view is set
    //    SystemBarTintManager tintManager = new SystemBarTintManager(activity);
    //    // enable status bar tint
    //    tintManager.setStatusBarTintEnabled(true);
    //    // set the transparent color of the status bar, 20% darker
    //    tintManager.setTintColor(activity.getResources().getColor(R.color.status_tint_color));
    //}

    //public static void setStatusBarColor(@NonNull Activity activity, @NonNull Toolbar toolbar,
    //        @ColorRes int color) {
    //    Window window = activity.getWindow();
    //    if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
    //        window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
    //                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
    //        supportStatusBarUnderKitkat(activity, toolbar);
    //    } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
    //        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
    //        window.setStatusBarColor(ContextCompat.getColor(activity, color));
    //    }
    //}
    //
    //public static void setGreyStatusBar(@NonNull Activity activity, @NonNull Toolbar toolbar) {
    //    setStatusBarColor(activity, toolbar, R.color.black_10);
    //}

    public static int getStatusBarHeight(@NonNull Activity activity) {
        int result = 0;
        int resourceId =
                activity.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = activity.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static int getNavigationBarHeight(@NonNull Activity activity) {
        int result = 0;
        int resourceId =
                activity.getResources().getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = activity.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static void showSystemUI(Activity activity) {
        activity.getWindow()
                .getDecorView()
                .setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    public static void hideSystemUI(Activity activity) {
        activity.getWindow()
                .getDecorView()
                .setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    public static void gotoMenu(Context context, String functionCode) {
        switch (functionCode) {
            case Function.MenuId.MENU_BAN_LE:
//                context.startActivity(new Intent(context, SaleRetailActivity.class));
                break;
            case Function.MenuId.MENU_BAN_CHO_KENH:
//                context.startActivity(new Intent(context, SaleChannelActivity.class));
                break;
            case Function.MenuId.MENU_BAN_HANG_THEO_DON_PHE_DUYET_DON_HANG:
//                context.startActivity(new Intent(context, SellOrdersActivity.class));
                break;
            case Function.MenuId.MENU_BAN_DICH_VU_VAS:
//                context.startActivity(new Intent(context, SellVasActivity.class));
                break;
            case Function.MenuId.MENU_LAP_HOA_DON:
//                context.startActivity(new Intent(context, MakeInvoiceActivity.class));
                break;
            case Function.MenuId.MENU_DAU_NOI_DI_DONG:
//                context.startActivity(new Intent(context, ConnectorMobileActivity.class));
                break;
            case Function.MenuId.MENU_DAU_NOI_CO_DINH:
                // TODO: 08/06/2017 Fake
//                context.startActivity(new Intent(context, ConnectorMobileActivity.class));
                break;
            case Function.MenuId.MENU_BAN_ANYPAY:
//                context.startActivity(new Intent(context, SellAnyPayActivity.class));
                break;
            case Function.MenuId.MENU_NAP_CHUYEN_ANYPAY:
//                context.startActivity(new Intent(context, TransferAnyPayActivity.class));
                break;
            case Function.MenuId.MENU_DANG_KY_THONG_TIN:
//                Intent intentDKTT = new Intent(context, CreateUpdateInformationActivity.class);
//                intentDKTT.putExtra(CreateUpdateInformationActivity.ARG_TYPE, true);
//                context.startActivity(intentDKTT);
                break;
            case Function.MenuId.MENU_CAP_NHAT_THONG_TIN:
//                Intent intentCNTT = new Intent(context, CreateUpdateInformationActivity.class);
//                intentCNTT.putExtra(CreateUpdateInformationActivity.ARG_TYPE, false);
//                context.startActivity(intentCNTT);
                break;
            case Function.MenuId.MENU_DOI_SIM:
//                context.startActivity(new Intent(context, ChangeSimActivity.class));
                break;
            case Function.MenuId.MENU_THAY_DOI_DIA_CHI_LAP_DAT:
//                context.startActivity(new Intent(context, InstallationAddressActivity.class));
                break;
            // TODO: 6/10/17 upload image offline
            //                        case Function.MenuId.MENU_UPLOAD_ANH:
            //                            context.startActivity(new Intent(context,
            //                                    UploadImageActivity.class));
            //                            break;

            case Function.MenuId.MENU_TAO_KENH_PHAN_PHOI:
//                context.startActivity(new Intent(context, BranchesActivity.class));
                break;
            case Function.MenuId.MENU_QUAN_LY_DBHC_BTS_KENH:
//                context.startActivity(new Intent(context, ManageAreaActivity.class));
                break;
            case Function.MenuId.MENU_QUAN_LY_KPI_KPP:
                break;
            case Function.MenuId.MENU_QUAN_LY_THONG_TIN_KPP:
//                context.startActivity(new Intent(context, ManageChannelActivity.class));
                break;
            case Function.MenuId.MENU_QUAN_LY_VAN_BAN_CSTT:
                break;

            case Function.MenuId.MENU_XAC_MINH:
                break;
            case Function.MenuId.MENU_GACH_NO:
                break;
            case Function.MenuId.MENU_THU_CUOC_NONG:
                break;
            case Function.MenuId.MENU_QUAN_LY_TIEN_DO_THU_CUOC:
                break;

            //                        case Function.MenuId.MENU_GIAO_VIEC_TO_DOI:
            //                            break;
            case Function.MenuId.MENU_GIAO_VIEC_PHAT_SINH:
                Intent intent2 = new Intent(context, ListAssignTaskActivity.class);
                intent2.putExtra(ListAssignTaskActivity.EXTRA_TASK_TYPE,
                        TaskShopManagement.TaskType.TYPE_PHAT_SINH);
                context.startActivity(intent2);
                break;
            case Function.MenuId.MENU_GIAO_VIEC_CS_KPP:
                Intent intent3 = new Intent(context, ListAssignTaskActivity.class);
                intent3.putExtra(ListAssignTaskActivity.EXTRA_TASK_TYPE,
                        TaskShopManagement.TaskType.TYPE_CSKPP);
                context.startActivity(intent3);
                break;
            case Function.MenuId.MENU_DONG_VIEC:
                context.startActivity(new Intent(context, ListUpdateTaskActivity.class));
                break;

            case Function.MenuId.MENU_XEM_KHO:
//                context.startActivity(new Intent(context, ViewWarehouseActivity.class));
                break;
            case Function.MenuId.MENU_NHAP_HOA_DON:
//                context.startActivity(new Intent(context, InputOrderActivity.class));
                break;
            case Function.MenuId.MENU_XUAT_KHO_CAP_DUOI:
//                context.startActivity(new Intent(context, ListXuatKhoCapDuoi.class));
                break;
            case Function.MenuId.MENU_NHAP_KHO_CAP_TREN:
//                context.startActivity(new Intent(context, ListNhapKhoCapTren.class));
                break;
            case Function.MenuId.MENU_TRA_HANG_CAP_TREN:
//                context.startActivity(new Intent(context, ListOrderReturnUpperActivity.class));
                break;
            case Function.MenuId.MENU_NHAP_KHO_CAP_DUOI:
//                context.startActivity(new Intent(context, ListOrderActivity.class));
                break;
            case Function.MenuId.MENU_XUAT_KHO_CHO_NHAN_VIEN:
//                context.startActivity(new Intent(context, XuatKhoChoNhanVienActivity.class));
                break;
            case Function.MenuId.MENU_NV_XAC_NHAN_HANG:
//                context.startActivity(new Intent(context, NvXacNhanHangActivity.class));
                break;
            case Function.MenuId.MENU_NHAN_VIEN_TRA_HANG_CAP_TREN:
//                context.startActivity(new Intent(context, ListNhanVienTraHangActivity.class));
                break;
            case Function.MenuId.MENU_NHAP_KHO_TU_NHAN_VIEN:
//                context.startActivity(new Intent(context, ListNhapKhoTuNhanVienActivity.class));
                break;
            case Function.MenuId.MENU_KENH_ORDER_HANG:
//                context.startActivity(new Intent(context, KPPOrderActivity.class));
                break;

            case Function.MenuId.MENU_TRA_CUU:
                break;
            //                        case Function.MenuId.MENU_TIEP_NHAN_BH:
            //                            break;
            case Function.MenuId.MENU_CHUYEN_MUC_BH:
                break;
            case Function.MenuId.MENU_TRA_BH:
                break;

            case Function.MenuId.MENU_SURVEY_KPP:
//                context.startActivity(new Intent(context, SurveyListActivity.class));
                //                            context.startActivity(
                //                                    new Intent(context, SurveyActivity.class));
                break;
            case Function.MenuId.MENU_HOTNEW_CS_KPP:
//                context.startActivity(new Intent(context, HotNewsCSKPPActivity.class));
                break;
            case Function.MenuId.MENU_KPP_FEEDBACK:
//                context.startActivity(new Intent(context, KPPFeedbackActivity.class));
                break;
            case Function.MenuId.MENU_TRA_CUU_SP:
//                context.startActivity(new Intent(context, SearchProductsActivity.class));
                break;

            case Function.MenuId.MENU_TAO_GIAY_NOP_TIEN:
//                context.startActivity(new Intent(context, CreateReturnMoneyTicketActivity.class));
                break;
            case Function.MenuId.MENU_PHE_DUYET_GIAY_NOP_TIEN:
//                context.startActivity(new Intent(context, ApproveReturnMoneyActivity.class));
                break;
            case Function.MenuId.MENU_DOI_SOAT_CONG_NO_GIAY_NOP_TIEN:
                break;
            case Function.MenuId.MENU_KHAI_BAO_GIA_KENH_CHAN_RET:
                break;

            //                        case Function.MenuId.MENU_BAO_CAO_PHAT_TRIEN_THUE_BAO:
            //                            break;
            case Function.MenuId.MENU_BAO_CAO_CHAM_SOC_KENH:
                break;
            case Function.MenuId.MENU_BAO_CAO_TAN_SUAT_CHAM_SOC_KENH:
                break;
            case Function.MenuId.MENU_BAO_CAO_TON_KHO:
//                Intent intent1 = new Intent(context, ChiTietXuatKhoNhanVienActivity.class);
//                StockTrans stockTrans = new StockTrans();
//                stockTrans.setStockTransId(1237);
//                stockTrans.setToOwnerId(1232);
//                stockTrans.setCreateDatetime("2017-01-02");
//                stockTrans.setStockTransStatusName("hang moi");
//                Bundle bundle = new Bundle();
//                bundle.putParcelable(Constants.BundleConstant.STOCK_TRANS, stockTrans);
//                intent1.putExtras(bundle);
//                context.startActivity(intent1);

                break;
            case Function.MenuId.MENU_BAO_CAO_GIAO_CHI_TIEU_BAN_HANG:
                //Intent intent = new Intent(context,
                //        ChiTietXuatKhoNhanVienActivity.class);
                //StockTrans stockTrans = new StockTrans();
                //stockTrans.setStockTransId(1237);
                //stockTrans.setToOwnerId(1232);
                //stockTrans.setCreateDatetime("2017-01-02");
                //stockTrans.setStockTransStatusName("hang moi");
                //Bundle bundle = new Bundle();
                //bundle.putParcelable(Constants.BundleConstant.STOCK_TRANS, stockTrans);
                //intent.putExtras(bundle);
                //context.startActivity(intent);

//                Intent intent = new Intent(context, LapLenh3XuatKhoChoNhanVienActivity.class);
//                context.startActivity(intent);
                break;
        }
    }
}
