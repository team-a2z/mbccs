package com.viettel.mbccs.utils;

import android.content.Context;
import android.telephony.TelephonyManager;

import java.lang.reflect.Method;

public final class TelephonyInfo {

    private static TelephonyInfo telephonyInfo;
    private String imeiSIM1;
    private String imeiSIM2;
    private String simSerial1;
    private String simSerial2;
    private boolean isSIM1Ready;
    private boolean isSIM2Ready;

    public String getImeiSIM1() {
        return imeiSIM1;
    }

    public String getImeiSIM2() {
        return imeiSIM2;
    }


    public boolean isSIM1Ready() {
        return isSIM1Ready;
    }

    public boolean isSIM2Ready() {
        return isSIM2Ready;
    }

    public boolean isDualSIM() {
        return imeiSIM2 != null;
    }

    public String getSimSerial1() {
        return simSerial1;
    }

    public String getSimSerial2() {
        return simSerial2;
    }

    private TelephonyInfo() {
    }

    public static TelephonyInfo getInstance(Context context) {

        if (telephonyInfo == null) {

            telephonyInfo = new TelephonyInfo();

            TelephonyManager telephonyManager = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE));

            telephonyInfo.imeiSIM1 = telephonyManager.getDeviceId();
            telephonyInfo.imeiSIM2 = null;

            try {
                telephonyInfo.imeiSIM1 = getDeviceIdBySlot(context, "getDeviceIdGemini", 0);
                telephonyInfo.imeiSIM2 = getDeviceIdBySlot(context, "getDeviceIdGemini", 1);
            } catch (GeminiMethodNotFoundException e) {

                try {
                    telephonyInfo.imeiSIM1 = getDeviceIdBySlot(context, "getDeviceId", 0);
                    telephonyInfo.imeiSIM2 = getDeviceIdBySlot(context, "getDeviceId", 1);
                } catch (GeminiMethodNotFoundException e1) {

                    try {
                        telephonyInfo.imeiSIM1 = getDeviceIdBySlot(context, "getDeviceIdGemini");
                        telephonyInfo.imeiSIM2 = null;
                    } catch (GeminiMethodNotFoundException e2) {

                        try {
                            telephonyInfo.imeiSIM1 = getDeviceIdBySlot(context, "getDeviceId");
                            telephonyInfo.imeiSIM2 = null;
                        } catch (GeminiMethodNotFoundException e3) {
                        }
                    }
                }
            }

            telephonyInfo.isSIM1Ready = telephonyManager.getSimState() == TelephonyManager.SIM_STATE_READY;
            telephonyInfo.isSIM2Ready = false;

            try {
                telephonyInfo.isSIM1Ready = getSIMStateBySlot(context, "getSimStateGemini", 0);
                telephonyInfo.isSIM2Ready = getSIMStateBySlot(context, "getSimStateGemini", 1);
            } catch (GeminiMethodNotFoundException e) {

                try {
                    telephonyInfo.isSIM1Ready = getSIMStateBySlot(context, "getSimState", 0);
                    telephonyInfo.isSIM2Ready = getSIMStateBySlot(context, "getSimState", 1);
                } catch (GeminiMethodNotFoundException e1) {
                    try {
                        telephonyInfo.isSIM1Ready = getSIMStateBySlot(context, "getSimStateGemini");
                        telephonyInfo.isSIM2Ready = false;
                    } catch (GeminiMethodNotFoundException e2) {
                        try {
                            telephonyInfo.isSIM1Ready = getSIMStateBySlot(context, "getSimState");
                            telephonyInfo.isSIM2Ready = false;
                        } catch (GeminiMethodNotFoundException e3) {

                        }
                    }
                }
            }

            //
            try {
                telephonyInfo.simSerial1 = getDeviceIdBySlot(context, "getSimSerialNumberGemini", 0);
                telephonyInfo.simSerial2 = getDeviceIdBySlot(context, "getSimSerialNumberGemini", 1);
            } catch (GeminiMethodNotFoundException e) {

                try {
                    telephonyInfo.simSerial1 = getDeviceIdBySlot(context, "getSimSerialNumber", 0);
                    telephonyInfo.simSerial2 = getDeviceIdBySlot(context, "getSimSerialNumber", 1);
                } catch (GeminiMethodNotFoundException e1) {
                    try {
                        telephonyInfo.simSerial1 = getDeviceIdBySlot(context, "getSimSerialNumberGemini");
                        telephonyInfo.simSerial2 = null;
                    } catch (GeminiMethodNotFoundException e2) {
                        try {
                            telephonyInfo.simSerial1 = getDeviceIdBySlot(context, "getSimSerialNumber");
                            telephonyInfo.simSerial2 = null;
                        } catch (GeminiMethodNotFoundException e3) {

                        }
                    }
                }
            }
        }

        return telephonyInfo;
    }

    private static String getDeviceIdBySlot(Context context, String predictedMethodName, int slotID) throws GeminiMethodNotFoundException {

        String imei = null;

        TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        try {

            Class<?> telephonyClass = Class.forName(telephony.getClass().getName());

            Class<?>[] parameter = new Class[1];
            parameter[0] = int.class;
            Method getSimID = telephonyClass.getMethod(predictedMethodName, parameter);

            Object[] obParameter = new Object[1];
            obParameter[0] = slotID;
            Object ob_phone = getSimID.invoke(telephony, obParameter);

            if (ob_phone != null) {
                imei = ob_phone.toString();

            }
        } catch (Exception e) {
            throw new GeminiMethodNotFoundException(predictedMethodName);
        }

        return imei;
    }

    private static String getDeviceIdBySlot(Context context, String predictedMethodName) throws GeminiMethodNotFoundException {

        String imei = null;

        TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        try {

            Class<?> telephonyClass = Class.forName(telephony.getClass().getName());

            Method getSimID = telephonyClass.getMethod(predictedMethodName);
            Object ob_phone = getSimID.invoke(telephony);

            if (ob_phone != null) {
                imei = ob_phone.toString();

            }
        } catch (Exception e) {
            throw new GeminiMethodNotFoundException(predictedMethodName);
        }

        return imei;
    }

    private static boolean getSIMStateBySlot(Context context, String predictedMethodName, int slotID) throws GeminiMethodNotFoundException {

        boolean isReady = false;

        TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        try {

            Class<?> telephonyClass = Class.forName(telephony.getClass().getName());

            Class<?>[] parameter = new Class[1];
            parameter[0] = int.class;
            Method getSimStateGemini = telephonyClass.getMethod(predictedMethodName, parameter);

            Object[] obParameter = new Object[1];
            obParameter[0] = slotID;
            Object ob_phone = getSimStateGemini.invoke(telephony, obParameter);

            if (ob_phone != null) {
                int simState = Integer.parseInt(ob_phone.toString());
                if (simState == TelephonyManager.SIM_STATE_READY) {
                    isReady = true;
                }
            }
        } catch (Exception e) {
            throw new GeminiMethodNotFoundException(predictedMethodName);
        }

        return isReady;
    }

    private static boolean getSIMStateBySlot(Context context, String predictedMethodName) throws GeminiMethodNotFoundException {

        boolean isReady = false;

        TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        try {

            Class<?> telephonyClass = Class.forName(telephony.getClass().getName());

            Method getSimStateGemini = telephonyClass.getMethod(predictedMethodName);
            Object ob_phone = getSimStateGemini.invoke(telephony);

            if (ob_phone != null) {
                int simState = Integer.parseInt(ob_phone.toString());
                if (simState == TelephonyManager.SIM_STATE_READY) {
                    isReady = true;
                }
            }
        } catch (Exception e) {
            throw new GeminiMethodNotFoundException(predictedMethodName);
        }

        return isReady;
    }

    private static class GeminiMethodNotFoundException extends Exception {

        private static final long serialVersionUID = -996812356902545308L;

        public GeminiMethodNotFoundException(String info) {
            super(info);
        }
    }
}