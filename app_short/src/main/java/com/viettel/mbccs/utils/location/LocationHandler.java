package com.viettel.mbccs.utils.location;

import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Created by HuyQuyet on 3/11/17.
 */

public interface LocationHandler {
    void onLocationClientConnect();

    GoogleApiClient getGoogleApiClient();

    boolean isConnected();

    boolean isLocationServiceAvailable();

    void addListener(AppLocationListener appLocationListener);

    void removeListener(AppLocationListener appLocationListener);
}
