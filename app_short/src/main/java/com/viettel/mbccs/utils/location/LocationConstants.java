package com.viettel.mbccs.utils.location;

/**
 * Created by nguyenhuyquyet on 9/14/17.
 */

public interface LocationConstants {
    int LOCATION_UPDATE_INTERVAL = 30000; //30 secs
    int LOCATION_FASTEST_UPDATE_INTERVAL = 10000; //10 secs
    float SMALLEST_DISTANT_UPDATE_METER = 20.0f; // 20 meters update
}
