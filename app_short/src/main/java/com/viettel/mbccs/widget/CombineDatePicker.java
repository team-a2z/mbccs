package com.viettel.mbccs.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.viettel.mbccs.R;

import java.util.Date;

/**
 * Created by Da Qiao on 9/7/2017.
 */

public class CombineDatePicker extends LinearLayout {

    private CustomDatePicker mFromDate;

    private CustomDatePicker mToDate;

    public CombineDatePicker(Context context) {
        this(context, null);
    }

    public CombineDatePicker(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CombineDatePicker(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public CombineDatePicker(Context context, AttributeSet attrs, int defStyleAttr, int
            defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    private void initView(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomDatePicker);
        boolean isThemeLight;
        try {
            isThemeLight = a.getBoolean(R.styleable.CustomDatePicker_pickerThemeLight, false);
        } finally {
            a.recycle();
        }

        setOrientation(HORIZONTAL);
        setGravity(Gravity.CENTER_VERTICAL);
        mFromDate = new CustomDatePicker(context, attrs);
        mToDate = new CustomDatePicker(context, attrs);
        LinearLayout.LayoutParams fromLP = new LayoutParams(0, LinearLayout.LayoutParams
                .WRAP_CONTENT);
        fromLP.weight = 1;
        LinearLayout.LayoutParams toLP = new LayoutParams(0, LinearLayout.LayoutParams
                .WRAP_CONTENT);
        toLP.weight = 1;
        mFromDate.setLayoutParams(fromLP);
        mToDate.setLayoutParams(toLP);

        int margin = context.getResources().getDimensionPixelSize(R.dimen.dp_8);
        ImageView imageView = new ImageView(context, attrs);
        LinearLayout.LayoutParams imageLP = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        imageLP.leftMargin = margin;
        imageLP.rightMargin = margin;
        imageView.setLayoutParams(imageLP);
        if (isThemeLight)
            imageView.setImageResource(R.drawable.ic_play_arrow_black_24dp);
        else
            imageView.setImageResource(R.drawable.ic_play_arrow_white_24dp);

        addView(mFromDate);
        addView(imageView);
        addView(mToDate);
        mToDate.setOnDateSetListener(new CustomDatePicker.CustomDateSetListener() {
            @Override
            public void onDateSet(long time) {
                mFromDate.setMaxDate(new Date(time));
                if (mFromDate.getDateInMilis() > time)
                    mFromDate.setTime(time);
            }
        });
    }

    public long getFromDate() {
        return mFromDate.getDateInMilis();
    }

    public long getToDate() {
        return mToDate.getDateInMilis();
    }

    public void setFromDate(Date fromDate) {
        mFromDate.setDateToCalendar(fromDate);
    }

    public void setToDate(Date toDate) {
        mToDate.setDateToCalendar(toDate);
    }

    public String getFromDateString() {
        return mFromDate.getStringDate();
    }

    public String getToDateString() {
        return mToDate.getStringDate();
    }

    public String getFromDateStringFormatDDMMYY() {
        return mFromDate.getStringFormatDDMMYY();
    }

    public String getToFromDateStringFormatDDMMYY() {
        return mToDate.getStringFormatDDMMYY();
    }
}
