package com.viettel.mbccs.data.source.local.datasource;

import com.google.gson.Gson;
import com.viettel.mbccs.data.source.local.ISellVasLocalDataSource;

/**
 * Created by eo_cuong on 5/10/17.
 */

public class SellVasLocalDataSource implements ISellVasLocalDataSource {
    private SharedPrefs sharedPrefs;
    private Gson gson;
    public static SellVasLocalDataSource instance;

    public SellVasLocalDataSource() {
        sharedPrefs = SharedPrefs.getInstance();
        gson = new Gson();
    }


    public static SellVasLocalDataSource getInstance() {
        if (instance == null) {
            instance = new SellVasLocalDataSource();
        }
        return instance;
    }
}
