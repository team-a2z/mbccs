package com.viettel.mbccs.data.model;

import com.google.gson.annotations.Expose;

import java.util.Date;

/**
 * Created by minhnx on 10/17/17.
 */

public class EventInfo implements Cloneable {

    public enum EventType {FESTIVAL, MARKET}

    private Long Id;
    @Expose
    private String areaCode;
    @Expose
    private String eventCode;
    @Expose
    private String eventName;
    @Expose
    private Date issueDate;
    @Expose
    private Date issueTime;
    @Expose
    private String note;
    @Expose
    private String latitude;
    @Expose
    private String longitude;
    @Expose
    private EventType eventType;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getEventCode() {
        return eventCode;
    }

    public void setEventCode(String eventCode) {
        this.eventCode = eventCode;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public Date getIssueTime() {
        return issueTime;
    }

    public void setIssueTime(Date issueTime) {
        this.issueTime = issueTime;
    }

    public EventInfo clone() throws CloneNotSupportedException {
        try {
            return (EventInfo) super.clone();
        } catch (CloneNotSupportedException ce) {
            return null;
        }
    }
}
