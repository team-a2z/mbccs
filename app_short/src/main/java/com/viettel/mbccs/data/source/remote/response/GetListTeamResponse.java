package com.viettel.mbccs.data.source.remote.response;

import com.google.gson.annotations.SerializedName;
import com.viettel.mbccs.data.model.Shop;

import java.util.List;

/**
 * Created by jacky on 6/23/17.
 */

public class GetListTeamResponse extends DataResponse {

    @SerializedName("listShop")
    private List<Shop> listShop;

    public List<Shop> getListShop() {
        return listShop;
    }

    public void setListShop(List<Shop> listShop) {
        this.listShop = listShop;
    }
}
