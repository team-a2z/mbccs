package com.viettel.mbccs.data.source.remote.request;

import com.google.gson.annotations.Expose;

/**
 * Created by eo_cuong on 5/19/17.
 */

public class GetDistributedChannelRequest extends BaseRequest {

    @Expose
    private String idNo;

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public GetDistributedChannelRequest() {
        super();
    }
}
