package com.viettel.mbccs.data.source.remote.request;

/**
 * Created by minhnx on 6/9/17.
 */

public class CreateDistributedChannelRequest extends BaseRequest {

    private Long shopId;
    private Long channelTypeId;
    private String tel;
    private String idType;
    private String idNo;
    private String idUssueDate;
    private String issuePlace;
    private String address;
    private String staffOwnType;
    private Long staffOwnerId;
    private Long btsId;
    private String longitude;
    private String latitude;
    private String idPathName;
    private String idImageName;
    private String idImageName1;
    private String idImageName2;
    private String ctPathName;
    private String ctImageName;
    private String username;
    private String isdn;

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getChannelTypeId() {
        return channelTypeId;
    }

    public void setChannelTypeId(Long channelTypeId) {
        this.channelTypeId = channelTypeId;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getIdUssueDate() {
        return idUssueDate;
    }

    public void setIdUssueDate(String idUssueDate) {
        this.idUssueDate = idUssueDate;
    }

    public String getIssuePlace() {
        return issuePlace;
    }

    public void setIssuePlace(String issuePlace) {
        this.issuePlace = issuePlace;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStaffOwnType() {
        return staffOwnType;
    }

    public void setStaffOwnType(String staffOwnType) {
        this.staffOwnType = staffOwnType;
    }

    public Long getStaffOwnerId() {
        return staffOwnerId;
    }

    public void setStaffOwnerId(Long staffOwnerId) {
        this.staffOwnerId = staffOwnerId;
    }

    public Long getBtsId() {
        return btsId;
    }

    public void setBtsId(Long btsId) {
        this.btsId = btsId;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getIdPathName() {
        return idPathName;
    }

    public void setIdPathName(String idPathName) {
        this.idPathName = idPathName;
    }

    public String getIdImageName() {
        return idImageName;
    }

    public void setIdImageName(String idImageName) {
        this.idImageName = idImageName;
    }

    public String getIdImageName1() {
        return idImageName1;
    }

    public void setIdImageName1(String idImageName1) {
        this.idImageName1 = idImageName1;
    }

    public String getIdImageName2() {
        return idImageName2;
    }

    public void setIdImageName2(String idImageName2) {
        this.idImageName2 = idImageName2;
    }

    public String getCtPathName() {
        return ctPathName;
    }

    public void setCtPathName(String ctPathName) {
        this.ctPathName = ctPathName;
    }

    public String getCtImageName() {
        return ctImageName;
    }

    public void setCtImageName(String ctImageName) {
        this.ctImageName = ctImageName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public CreateDistributedChannelRequest() {
        super();
    }
}
