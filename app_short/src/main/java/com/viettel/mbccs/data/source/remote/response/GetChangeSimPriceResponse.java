package com.viettel.mbccs.data.source.remote.response;

import com.google.gson.annotations.Expose;

/**
 * Created by minhnx on 6/7/17.
 */

public class GetChangeSimPriceResponse {
    @Expose
    private Double changeSimPrice;

    public Double getChangeSimPrice() {
        return changeSimPrice;
    }

    public void setChangeSimPrice(Double changeSimPrice) {
        this.changeSimPrice = changeSimPrice;
    }
}
