package com.viettel.mbccs.data.source.remote;

import com.viettel.mbccs.data.source.remote.request.CreateDistributedChannelRequest;
import com.viettel.mbccs.data.source.remote.request.DataRequest;
import com.viettel.mbccs.data.source.remote.request.GetDistributedChannelRequest;
import com.viettel.mbccs.data.source.remote.request.GetListBtsesRequest;
import com.viettel.mbccs.data.source.remote.request.GetListChannelTypeRequest;
import com.viettel.mbccs.data.source.remote.response.CreateDistributedChannelResponse;
import com.viettel.mbccs.data.source.remote.response.GetDistributedChannelResponse;
import com.viettel.mbccs.data.source.remote.response.GetListBtsesResponse;
import com.viettel.mbccs.data.source.remote.response.GetListChannelTypeResponse;

import rx.Observable;

/**
 * Created by eo_cuong on 5/11/17.
 */

public interface IBranchesRemoteDataSource {

    Observable<GetDistributedChannelResponse> getDistributedChannelInfo(DataRequest<GetDistributedChannelRequest> request);

    Observable<CreateDistributedChannelResponse> createDistributedChannel(DataRequest<CreateDistributedChannelRequest> request);

    Observable<GetListChannelTypeResponse> getListChannelType(DataRequest<GetListChannelTypeRequest> request);

    Observable<GetListBtsesResponse> getListBtses(DataRequest<GetListBtsesRequest> request);
}
