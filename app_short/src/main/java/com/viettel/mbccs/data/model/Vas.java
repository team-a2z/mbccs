package com.viettel.mbccs.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by minhnx on 7/24/17.
 */

public class Vas {
    @Expose
    @SerializedName("vasCode")
    private String code;
    @Expose
    @SerializedName("vasName")
    private String name;
    @Expose
    @SerializedName("note")
    private String description;
    @Expose
    @SerializedName("content")
    private String regSyntax;
    @Expose
    private boolean registered;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRegSyntax() {
        return regSyntax;
    }

    public void setRegSyntax(String regSyntax) {
        this.regSyntax = regSyntax;
    }

    public boolean isRegistered() {
        return registered;
    }

    public void setRegistered(boolean registered) {
        this.registered = registered;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Vas clone() {
        Vas newItem = new Vas();
        newItem.code = this.code;
        newItem.name = this.name;
        newItem.description = this.description;
        newItem.regSyntax = this.regSyntax;
        newItem.registered = this.registered;

        return newItem;
    }
}
