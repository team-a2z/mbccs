package com.viettel.mbccs.data.source.remote;

import com.viettel.mbccs.data.source.remote.request.DataRequest;
import com.viettel.mbccs.data.source.remote.request.GetAnypayAuthorizeRequest;
import com.viettel.mbccs.data.source.remote.request.GetListChannelByOwnerTypeIdRequest;
import com.viettel.mbccs.data.source.remote.request.GetListOwnerCodeRequest;
import com.viettel.mbccs.data.source.remote.request.GetListTTKDRequest;
import com.viettel.mbccs.data.source.remote.request.SellAnypayToChannelRequest;
import com.viettel.mbccs.data.source.remote.request.SellAnypayToCustomerRequest;
import com.viettel.mbccs.data.source.remote.response.GetAnypayAuthorizeResponse;
import com.viettel.mbccs.data.source.remote.response.GetListChannelByOwnerTypeIdResponse;
import com.viettel.mbccs.data.source.remote.response.GetListOwneCodeReponse;
import com.viettel.mbccs.data.source.remote.response.GetListTTKDResponse;
import com.viettel.mbccs.data.source.remote.response.SellAnypayToChannelResponse;
import com.viettel.mbccs.data.source.remote.response.SellAnypayToCustomerResponse;

import rx.Observable;

/**
 * Created by eo_cuong on 5/11/17.
 */

public interface ISellAnyPayRemoteDataSource {
    Observable<GetAnypayAuthorizeResponse> getAnypayAuthorize(DataRequest<GetAnypayAuthorizeRequest> request);

    Observable<SellAnypayToCustomerResponse> sellAnypayToCustomer(DataRequest<SellAnypayToCustomerRequest> request);

    Observable<SellAnypayToChannelResponse> sellAnypayToChannel(DataRequest<SellAnypayToChannelRequest> request);

//    Observable<FindShopByNameResponse> findShopByName(DataRequest<FindShopByNameRequest> request);
//
//    Observable<FindStaffByNameResponse> findStaffByName(DataRequest<FindStaffByNameRequest> request);
//
//    Observable<FindChannelByNameResponse> findChannelByName(DataRequest<FindChannelByNameRequest> request);

    Observable<GetListTTKDResponse> getListTTKD(DataRequest<GetListTTKDRequest> request);

    Observable<GetListOwneCodeReponse> getListOwnerCode(DataRequest<GetListOwnerCodeRequest> request);

    Observable<GetListChannelByOwnerTypeIdResponse> getListChannelByOwnerTypeId(DataRequest<GetListChannelByOwnerTypeIdRequest> request);
}
