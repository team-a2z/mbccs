package com.viettel.mbccs.data.source.remote.request;

import com.google.gson.annotations.Expose;

/**
 * Created by eo_cuong on 5/19/17.
 */

public class GetListSerialOfOrderRequest extends BaseRequest {

    @Expose
    private Long saleOrderDetailId;

    public Long getSaleOrderDetailId() {
        return saleOrderDetailId;
    }

    public void setSaleOrderDetailId(Long saleOrderDetailId) {
        this.saleOrderDetailId = saleOrderDetailId;
    }

    public GetListSerialOfOrderRequest() {
        super();
    }
}
