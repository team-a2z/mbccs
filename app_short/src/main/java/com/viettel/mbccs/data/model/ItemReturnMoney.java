package com.viettel.mbccs.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by HuyQuyet on 7/10/17.
 */

public class ItemReturnMoney implements Parcelable {

    @Expose
    private String invoiceNo;
    @Expose
    private Double amount;
    @Expose
    private String status;
    @Expose
    private String shop;
    @Expose
    private String statusText;
    @Expose
    private List<ItemReturnMoneyDetail> itemReturnMoneyDetails;

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getShop() {
        return shop;
    }

    public void setShop(String shop) {
        this.shop = shop;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public List<ItemReturnMoneyDetail> getItemReturnMoneyDetails() {
        return itemReturnMoneyDetails;
    }

    public void setItemReturnMoneyDetails(List<ItemReturnMoneyDetail> itemReturnMoneyDetails) {
        this.itemReturnMoneyDetails = itemReturnMoneyDetails;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(invoiceNo);
        parcel.writeDouble(amount);
        parcel.writeString(status);
        parcel.writeString(statusText);
        parcel.writeString(shop);
    }
}
