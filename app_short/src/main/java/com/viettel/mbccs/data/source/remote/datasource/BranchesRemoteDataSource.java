package com.viettel.mbccs.data.source.remote.datasource;

import com.viettel.mbccs.data.source.remote.IBranchesRemoteDataSource;
import com.viettel.mbccs.data.source.remote.request.CreateDistributedChannelRequest;
import com.viettel.mbccs.data.source.remote.request.DataRequest;
import com.viettel.mbccs.data.source.remote.request.GetDistributedChannelRequest;
import com.viettel.mbccs.data.source.remote.request.GetListBtsesRequest;
import com.viettel.mbccs.data.source.remote.request.GetListChannelTypeRequest;
import com.viettel.mbccs.data.source.remote.response.CreateDistributedChannelResponse;
import com.viettel.mbccs.data.source.remote.response.GetDistributedChannelResponse;
import com.viettel.mbccs.data.source.remote.response.GetListBtsesResponse;
import com.viettel.mbccs.data.source.remote.response.GetListChannelTypeResponse;
import com.viettel.mbccs.data.source.remote.service.RequestHelper;
import com.viettel.mbccs.utils.rx.SchedulerUtils;

import rx.Observable;

/**
 * Created by eo_cuong on 5/10/17.
 */

public class BranchesRemoteDataSource implements IBranchesRemoteDataSource {

    public volatile static BranchesRemoteDataSource instance;

    public BranchesRemoteDataSource() {

    }

    public static BranchesRemoteDataSource getInstance() {
        if (instance == null) {
            instance = new BranchesRemoteDataSource();
        }
        return instance;
    }

    @Override
    public Observable<GetDistributedChannelResponse> getDistributedChannelInfo(DataRequest<GetDistributedChannelRequest> request) {
        return RequestHelper.getRequest()
                .getDistributedChannelInfo(request)
                .flatMap(SchedulerUtils.<GetDistributedChannelResponse>convertDataFlatMap())
                .compose(SchedulerUtils.<GetDistributedChannelResponse>applyAsyncSchedulers());
    }

    @Override
    public Observable<CreateDistributedChannelResponse> createDistributedChannel(DataRequest<CreateDistributedChannelRequest> request) {
        return RequestHelper.getRequest()
                .createDistributedChannel(request)
                .flatMap(SchedulerUtils.<CreateDistributedChannelResponse>convertDataFlatMap())
                .compose(SchedulerUtils.<CreateDistributedChannelResponse>applyAsyncSchedulers());
    }

    @Override
    public Observable<GetListChannelTypeResponse> getListChannelType(DataRequest<GetListChannelTypeRequest> request) {
        return RequestHelper.getRequest()
                .getListChannelType(request)
                .flatMap(SchedulerUtils.<GetListChannelTypeResponse>convertDataFlatMap())
                .compose(SchedulerUtils.<GetListChannelTypeResponse>applyAsyncSchedulers());
    }

    @Override
    public Observable<GetListBtsesResponse> getListBtses(DataRequest<GetListBtsesRequest> request) {
        return RequestHelper.getRequest()
                .getListBtses(request)
                .flatMap(SchedulerUtils.<GetListBtsesResponse>convertDataFlatMap())
                .compose(SchedulerUtils.<GetListBtsesResponse>applyAsyncSchedulers());
    }
}
