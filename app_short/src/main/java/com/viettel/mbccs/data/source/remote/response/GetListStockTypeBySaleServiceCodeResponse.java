package com.viettel.mbccs.data.source.remote.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.viettel.mbccs.data.model.StockModel_1;
import java.util.List;

/**
 * Created by HuyQuyet on 7/20/17.
 */

public class GetListStockTypeBySaleServiceCodeResponse {

    @SerializedName("lstStockModel")
    @Expose
    private List<StockModel_1> stockModelList;

    public List<StockModel_1> getStockModelList() {
        return stockModelList;
    }

    public void setStockModelList(List<StockModel_1> stockModelList) {
        this.stockModelList = stockModelList;
    }
}
