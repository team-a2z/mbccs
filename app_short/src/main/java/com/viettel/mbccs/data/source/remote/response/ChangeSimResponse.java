package com.viettel.mbccs.data.source.remote.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by minhnx on 5/29/17.
 */

public class ChangeSimResponse extends DataResponse {

    @Expose
    private Long custId;
    @Expose
    @SerializedName("lstNameImage")
    private List<String> lstImageNames;

    public Long getCustId() {
        return custId;
    }

    public void setCustId(Long custId) {
        this.custId = custId;
    }

    public List<String> getLstImageNames() {
        return lstImageNames;
    }

    public void setLstImageNames(List<String> lstImageNames) {
        this.lstImageNames = lstImageNames;
    }
}
