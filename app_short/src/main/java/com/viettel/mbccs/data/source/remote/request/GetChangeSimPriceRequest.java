package com.viettel.mbccs.data.source.remote.request;

import com.google.gson.annotations.Expose;

/**
 * Created by eo_cuong on 5/19/17.
 */

public class GetChangeSimPriceRequest extends BaseRequest {

    @Expose
    private String subType;

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public GetChangeSimPriceRequest() {
        super();
    }
}
