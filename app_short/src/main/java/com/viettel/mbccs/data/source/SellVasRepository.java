package com.viettel.mbccs.data.source;

import com.viettel.mbccs.data.source.local.ISellVasLocalDataSource;
import com.viettel.mbccs.data.source.local.datasource.SellVasLocalDataSource;
import com.viettel.mbccs.data.source.remote.ISellVasRemoteDataSource;
import com.viettel.mbccs.data.source.remote.datasource.SellVasRemoteDataSource;
import com.viettel.mbccs.data.source.remote.request.DataRequest;
import com.viettel.mbccs.data.source.remote.request.GetReasonRequest;
import com.viettel.mbccs.data.source.remote.request.GetVasForIsdnRequest;
import com.viettel.mbccs.data.source.remote.request.GetVasInfoRequest;
import com.viettel.mbccs.data.source.remote.request.RegisterVasRequest;
import com.viettel.mbccs.data.source.remote.response.GetReasonResponse;
import com.viettel.mbccs.data.source.remote.response.GetVasForIsdnResponse;
import com.viettel.mbccs.data.source.remote.response.GetVasInfoResponse;
import com.viettel.mbccs.data.source.remote.response.RegisterVasResponse;

import rx.Observable;

/**
 * Created by eo_cuong on 5/10/17.
 */

public class SellVasRepository implements ISellVasLocalDataSource, ISellVasRemoteDataSource {

    private volatile static SellVasRepository instance;
    private SellVasLocalDataSource localDataSource;
    private SellVasRemoteDataSource remoteDataSource;

    public SellVasRepository(SellVasLocalDataSource localDataSource,
                             SellVasRemoteDataSource remoteDataSource) {
        this.localDataSource = localDataSource;
        this.remoteDataSource = remoteDataSource;
    }

    public static SellVasRepository getInstance() {
        if (instance == null) {
            instance = new SellVasRepository(SellVasLocalDataSource.getInstance(),
                    SellVasRemoteDataSource.getInstance());
        }
        return instance;
    }


    @Override
    public Observable<GetVasForIsdnResponse> getVasForIsdn(DataRequest<GetVasForIsdnRequest> request) {
        return remoteDataSource.getVasForIsdn(request);
    }

    @Override
    public Observable<GetVasInfoResponse> getVasInfo(DataRequest<GetVasInfoRequest> request) {
        return remoteDataSource.getVasInfo(request);
    }

    @Override
    public Observable<RegisterVasResponse> registerVas(DataRequest<RegisterVasRequest> request) {
        return remoteDataSource.registerVas(request);
    }

    @Override
    public Observable<GetReasonResponse> getReasons(DataRequest<GetReasonRequest> request) {
        return remoteDataSource.getReasons(request);
    }
}
