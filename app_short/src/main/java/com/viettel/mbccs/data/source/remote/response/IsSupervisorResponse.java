package com.viettel.mbccs.data.source.remote.response;

import com.google.gson.annotations.Expose;

/**
 * Created by minhnx on 5/29/17.
 */

public class IsSupervisorResponse extends DataResponse {
    @Expose
    private boolean manager;

    public boolean isManager() {
        return manager;
    }

    public void setManager(boolean manager) {
        this.manager = manager;
    }
}
