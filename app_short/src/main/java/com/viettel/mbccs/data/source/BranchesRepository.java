package com.viettel.mbccs.data.source;

import com.viettel.mbccs.data.source.local.IBranchesLocalDataSource;
import com.viettel.mbccs.data.source.local.datasource.BranchesLocalDataSource;
import com.viettel.mbccs.data.source.remote.IBranchesRemoteDataSource;
import com.viettel.mbccs.data.source.remote.datasource.BranchesRemoteDataSource;
import com.viettel.mbccs.data.source.remote.request.CreateDistributedChannelRequest;
import com.viettel.mbccs.data.source.remote.request.DataRequest;
import com.viettel.mbccs.data.source.remote.request.GetDistributedChannelRequest;
import com.viettel.mbccs.data.source.remote.request.GetListBtsesRequest;
import com.viettel.mbccs.data.source.remote.request.GetListChannelTypeRequest;
import com.viettel.mbccs.data.source.remote.response.CreateDistributedChannelResponse;
import com.viettel.mbccs.data.source.remote.response.GetDistributedChannelResponse;
import com.viettel.mbccs.data.source.remote.response.GetListBtsesResponse;
import com.viettel.mbccs.data.source.remote.response.GetListChannelTypeResponse;

import rx.Observable;

/**
 * Created by eo_cuong on 5/10/17.
 */

public class BranchesRepository implements IBranchesLocalDataSource, IBranchesRemoteDataSource {

    private volatile static BranchesRepository instance;
    private BranchesLocalDataSource localDataSource;
    private BranchesRemoteDataSource remoteDataSource;

    public BranchesRepository(BranchesLocalDataSource localDataSource,
                              BranchesRemoteDataSource remoteDataSource) {
        this.localDataSource = localDataSource;
        this.remoteDataSource = remoteDataSource;
    }

    public static BranchesRepository getInstance() {
        if (instance == null) {
            instance = new BranchesRepository(BranchesLocalDataSource.getInstance(),
                    BranchesRemoteDataSource.getInstance());
        }
        return instance;
    }

    @Override
    public Observable<GetDistributedChannelResponse> getDistributedChannelInfo(DataRequest<GetDistributedChannelRequest> request) {
        return remoteDataSource.getDistributedChannelInfo(request);
    }

    @Override
    public Observable<CreateDistributedChannelResponse> createDistributedChannel(DataRequest<CreateDistributedChannelRequest> request) {
        return remoteDataSource.createDistributedChannel(request);
    }

    @Override
    public Observable<GetListChannelTypeResponse> getListChannelType(DataRequest<GetListChannelTypeRequest> request) {
        return remoteDataSource.getListChannelType(request);
    }

    @Override
    public Observable<GetListBtsesResponse> getListBtses(DataRequest<GetListBtsesRequest> request) {
        return remoteDataSource.getListBtses(request);
    }
}
