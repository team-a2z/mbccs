package com.viettel.mbccs.data.source.remote.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.viettel.mbccs.data.model.Bts;

import java.util.List;

/**
 * Created by minhnx on 6/7/17.
 */

public class GetListBtsesResponse extends DataResponse {
    @Expose
    @SerializedName("lstBts")
    private List<Bts> listBtses;

    public List<Bts> getListBtses() {
        return listBtses;
    }

    public void setListBts(List<Bts> listBts) {
        this.listBtses = listBtses;
    }
}
