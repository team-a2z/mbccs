
package com.viettel.mbccs.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Bts implements Parcelable, Cloneable {

    @SerializedName("numOfSub")
    @Expose
    private long numOfSub;

    @SerializedName("note")
    @Expose
    private String note;

    @SerializedName("btsName")
    @Expose
    private String btsName;

    @SerializedName("updateDate")
    @Expose
    private String updateDate;

    @SerializedName("lats")
    @Expose
    private String lats;

    @SerializedName("btsId")
    @Expose
    private long btsId;

    @SerializedName("coverType")
    @Expose
    private String coverType;

    @SerializedName("longs")
    @Expose
    private String longs;

    @SerializedName("cell01")
    @Expose
    private String cell01;

    @SerializedName("high")
    @Expose
    private String high;

    @SerializedName("streetName")
    @Expose
    private String streetName;

    @SerializedName("cell03")
    @Expose
    private String cell03;

    @SerializedName("province")
    @Expose
    private String province;

    @SerializedName("cell02")
    @Expose
    private String cell02;

    @SerializedName("company")
    @Expose
    private String company;

    @SerializedName("shopId")
    @Expose
    private long shopId;

    @SerializedName("createDate")
    @Expose
    private String createDate;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("btsCode")
    @Expose
    private String btsCode;

    @SerializedName("toDate")
    @Expose
    private String toDate;

    @SerializedName("updateStaffId")
    @Expose
    private long updateStaffId;

    @SerializedName("population")
    @Expose
    private long population;

    @SerializedName("fromDate")
    @Expose
    private String fromDate;

    @SerializedName("precinct")
    @Expose
    private String precinct;

    @SerializedName("areaCode")
    @Expose
    private String areaCode;

    @SerializedName("site")
    @Expose
    private String site;

    @SerializedName("createStaffId")
    @Expose
    private long createStaffId;

    @SerializedName("administrative")
    @Expose
    private String administrative;

    @SerializedName("district")
    @Expose
    private String district;

    @SerializedName("config")
    @Expose
    private String config;

    @SerializedName("staffId")
    @Expose
    private long staffId;

    @SerializedName("status")
    @Expose
    private long status;

    public Bts() {

    }

    protected Bts(Parcel in) {

    }

    public long getNumOfSub() {
        return numOfSub;
    }

    public void setNumOfSub(long numOfSub) {
        this.numOfSub = numOfSub;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getBtsName() {
        return btsName;
    }

    public void setBtsName(String btsName) {
        this.btsName = btsName;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getLats() {
        return lats;
    }

    public void setLats(String lats) {
        this.lats = lats;
    }

    public long getBtsId() {
        return btsId;
    }

    public void setBtsId(long btsId) {
        this.btsId = btsId;
    }

    public String getCoverType() {
        return coverType;
    }

    public void setCoverType(String coverType) {
        this.coverType = coverType;
    }

    public String getLongs() {
        return longs;
    }

    public void setLongs(String longs) {
        this.longs = longs;
    }

    public String getCell01() {
        return cell01;
    }

    public void setCell01(String cell01) {
        this.cell01 = cell01;
    }

    public String getHigh() {
        return high;
    }

    public void setHigh(String high) {
        this.high = high;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getCell03() {
        return cell03;
    }

    public void setCell03(String cell03) {
        this.cell03 = cell03;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCell02() {
        return cell02;
    }

    public void setCell02(String cell02) {
        this.cell02 = cell02;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public long getShopId() {
        return shopId;
    }

    public void setShopId(long shopId) {
        this.shopId = shopId;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBtsCode() {
        return btsCode;
    }

    public void setBtsCode(String btsCode) {
        this.btsCode = btsCode;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public long getUpdateStaffId() {
        return updateStaffId;
    }

    public void setUpdateStaffId(long updateStaffId) {
        this.updateStaffId = updateStaffId;
    }

    public long getPopulation() {
        return population;
    }

    public void setPopulation(long population) {
        this.population = population;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getPrecinct() {
        return precinct;
    }

    public void setPrecinct(String precinct) {
        this.precinct = precinct;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public long getCreateStaffId() {
        return createStaffId;
    }

    public void setCreateStaffId(long createStaffId) {
        this.createStaffId = createStaffId;
    }

    public String getAdministrative() {
        return administrative;
    }

    public void setAdministrative(String administrative) {
        this.administrative = administrative;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getConfig() {
        return config;
    }

    public void setConfig(String config) {
        this.config = config;
    }

    public long getStaffId() {
        return staffId;
    }

    public void setStaffId(long staffId) {
        this.staffId = staffId;
    }

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return getBtsName();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }

    public String toGson() {
        return new Gson().toJson(this);
    }

    public Bts clone() throws CloneNotSupportedException {
        try {
            return (Bts) super.clone();
        } catch (CloneNotSupportedException ce) {
            return null;
        }
    }
}
