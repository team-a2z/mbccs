package com.viettel.mbccs.data.source.remote.request;

import com.google.gson.annotations.Expose;
import com.viettel.mbccs.data.model.RegisterVasInfo;

import java.util.List;

/**
 * Created by eo_cuong on 5/19/17.
 */

public class RegisterVasRequest extends BaseRequest {

    @Expose
    private String isdn;
    @Expose
    private List<RegisterVasInfo> lstVasCode;

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public List<RegisterVasInfo> getLstVasCode() {
        return lstVasCode;
    }

    public void setLstVasCode(List<RegisterVasInfo> lstVasCode) {
        this.lstVasCode = lstVasCode;
    }

    public RegisterVasRequest() {
        super();
    }
}
