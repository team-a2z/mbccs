package com.viettel.mbccs.data.model;

import com.google.gson.annotations.Expose;

/**
 * Created by minhnx on 7/26/17.
 */

public class ChannelType {
    @Expose
    private String prefixObjectCode;
    @Expose
    private Double debitDefault;
    @Expose
    private String name;
    @Expose
    private Long channelTypeId;
    @Expose
    private Long rateDebit;
    @Expose
    private Integer isPrivate;
    @Expose
    private String discountPolicyDefault;
    @Expose
    private String checkComm;
    @Expose
    private String isVtUnit;
    @Expose
    private String objectType;
    @Expose
    private Integer status;

    public String getPrefixObjectCode() {
        return prefixObjectCode;
    }

    public void setPrefixObjectCode(String prefixObjectCode) {
        this.prefixObjectCode = prefixObjectCode;
    }

    public Double getDebitDefault() {
        return debitDefault;
    }

    public void setDebitDefault(Double debitDefault) {
        this.debitDefault = debitDefault;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getChannelTypeId() {
        return channelTypeId;
    }

    public void setChannelTypeId(Long channelTypeId) {
        this.channelTypeId = channelTypeId;
    }

    public Long getRateDebit() {
        return rateDebit;
    }

    public void setRateDebit(Long rateDebit) {
        this.rateDebit = rateDebit;
    }

    public Integer getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(Integer isPrivate) {
        this.isPrivate = isPrivate;
    }

    public String getDiscountPolicyDefault() {
        return discountPolicyDefault;
    }

    public void setDiscountPolicyDefault(String discountPolicyDefault) {
        this.discountPolicyDefault = discountPolicyDefault;
    }

    public String getCheckComm() {
        return checkComm;
    }

    public void setCheckComm(String checkComm) {
        this.checkComm = checkComm;
    }

    public String getIsVtUnit() {
        return isVtUnit;
    }

    public void setIsVtUnit(String isVtUnit) {
        this.isVtUnit = isVtUnit;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
