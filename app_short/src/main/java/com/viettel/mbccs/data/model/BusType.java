package com.viettel.mbccs.data.model;

import android.content.Context;
import android.support.annotation.IntDef;
import android.support.annotation.StringDef;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.viettel.mbccs.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HuyQuyet on 7/4/17.
 */

public class BusType {

    @StringDef({LoaiKhachHang.CA_NHAN, LoaiKhachHang.DOANH_NGHIEP})
    public @interface LoaiKhachHang {
        String CA_NHAN = "IND";
        String DOANH_NGHIEP = "BC";
    }

    @IntDef({IdLoaiKhachHang.CA_NHAN, IdLoaiKhachHang.DOANH_NGHIEP})
    public @interface IdLoaiKhachHang {
        int CA_NHAN = 0;
        int DOANH_NGHIEP = 1;
    }

    @SerializedName("tet")
    @Expose
    private int tet;

    @SerializedName("tct")
    @Expose
    private int tct;

    @SerializedName("tax")
    @Expose
    private int tax;

    @SerializedName("tinRequired")
    @Expose
    private String tinRequired;

    @SerializedName("expireRequired")
    @Expose
    private String expireRequired;

    @SerializedName("changeDatetime")
    @Expose
    private String changeDatetime;

    @SerializedName("createDatetime")
    @Expose
    private String createDatetime;

    @SerializedName("popRequired")
    @Expose
    private String popRequired;

    @SerializedName("custType")
    @Expose
    private String custType;

    @SerializedName("permitRequired")
    @Expose
    private String permitRequired;

    @SerializedName("idRequired")
    @Expose
    private String idRequired;

    @SerializedName("busType")
    @Expose
    private String busType;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("status")
    @Expose
    private int status;

    public int getTet() {
        return tet;
    }

    public void setTet(int tet) {
        this.tet = tet;
    }

    public int getTct() {
        return tct;
    }

    public void setTct(int tct) {
        this.tct = tct;
    }

    public int getTax() {
        return tax;
    }

    public void setTax(int tax) {
        this.tax = tax;
    }

    public String getTinRequired() {
        return tinRequired;
    }

    public void setTinRequired(String tinRequired) {
        this.tinRequired = tinRequired;
    }

    public String getExpireRequired() {
        return expireRequired;
    }

    public void setExpireRequired(String expireRequired) {
        this.expireRequired = expireRequired;
    }

    public String getChangeDatetime() {
        return changeDatetime;
    }

    public void setChangeDatetime(String changeDatetime) {
        this.changeDatetime = changeDatetime;
    }

    public String getCreateDatetime() {
        return createDatetime;
    }

    public void setCreateDatetime(String createDatetime) {
        this.createDatetime = createDatetime;
    }

    public String getPopRequired() {
        return popRequired;
    }

    public void setPopRequired(String popRequired) {
        this.popRequired = popRequired;
    }

    public String getCustType() {
        return custType;
    }

    public void setCustType(String custType) {
        this.custType = custType;
    }

    public String getPermitRequired() {
        return permitRequired;
    }

    public void setPermitRequired(String permitRequired) {
        this.permitRequired = permitRequired;
    }

    public String getIdRequired() {
        return idRequired;
    }

    public void setIdRequired(String idRequired) {
        this.idRequired = idRequired;
    }

    public String getBusType() {
        return busType;
    }

    public void setBusType(String busType) {
        this.busType = busType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return getName();
    }


    public static List<BusType> getListLoaiKhachHang(Context context) {
        BusType busTypeCN = new BusType();
        busTypeCN.setBusType(LoaiKhachHang.CA_NHAN);
        busTypeCN.setName(context.getString(
                R.string.create_new_connector_information_postpaid_khach_hang_ca_nhan));
        BusType busTypeDN = new BusType();
        busTypeDN.setBusType(LoaiKhachHang.DOANH_NGHIEP);
        busTypeDN.setName(context.getString(
                R.string.create_new_connector_information_postpaid_khach_hang_doanh_nghiep));

        List<BusType> listLoaiKhachHang = new ArrayList<>();
        listLoaiKhachHang.add(busTypeCN);
        listLoaiKhachHang.add(busTypeDN);
        return listLoaiKhachHang;
    }
}
