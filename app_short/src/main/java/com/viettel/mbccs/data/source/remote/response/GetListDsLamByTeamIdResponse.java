package com.viettel.mbccs.data.source.remote.response;

import com.google.gson.annotations.SerializedName;
import com.viettel.mbccs.data.model.LamModel;

import java.util.List;

/**
 * Created by jacky on 6/23/17.
 */

public class GetListDsLamByTeamIdResponse  extends DataResponse {

    @SerializedName("lstdsLam")
    private List<LamModel> listDSLam;

    public List<LamModel> getListDSLam() {
        return listDSLam;
    }

    public void setListDSLam(List<LamModel> listDSLam) {
        this.listDSLam = listDSLam;
    }
}
