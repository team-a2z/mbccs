package com.viettel.mbccs.data.source.remote.request;

import com.google.gson.annotations.Expose;

/**
 * Created by buidinhviet on 8/29/17.
 */

public class UserPassRequest {
    @Expose
    private String username;
    @Expose
    private String passold;
    @Expose
    private String passnew;
    @Expose
    private String otp;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassold() {
        return passold;
    }

    public void setPassold(String passold) {
        this.passold = passold;
    }

    public String getPassnew() {
        return passnew;
    }

    public void setPassnew(String passnew) {
        this.passnew = passnew;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
}
