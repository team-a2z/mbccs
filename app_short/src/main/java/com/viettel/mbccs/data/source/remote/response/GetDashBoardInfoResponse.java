package com.viettel.mbccs.data.source.remote.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.viettel.mbccs.data.model.Dashboard;

import java.util.List;

/**
 * Created by Da Qiao on 8/18/2017.
 */

public class GetDashBoardInfoResponse {
    @SerializedName("lstDashBoard")
    @Expose
    private List<Dashboard> lstDashBoard = null;

    public List<Dashboard> getLstDashBoard() {
        return lstDashBoard;
    }

    public void setLstDashBoard(List<Dashboard> lstDashBoard) {
        this.lstDashBoard = lstDashBoard;
    }
}
