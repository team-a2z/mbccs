package com.viettel.mbccs.data.source.remote.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by eo_cuong on 5/19/17.
 */

public class IsSupervisorRequest extends BaseRequest {

    @Expose
    @SerializedName("lstRole")
    private List<String> lstRoles;

    public List<String> getLstRoles() {
        return lstRoles;
    }

    public void setLstRoles(List<String> lstRoles) {
        this.lstRoles = lstRoles;
    }

    public IsSupervisorRequest() {
        super();
    }
}
