package com.viettel.mbccs.data.source.remote.request;

import com.google.gson.annotations.Expose;

/**
 * Created by eo_cuong on 5/19/17.
 */

public class GetVasForIsdnRequest extends BaseRequest {

    @Expose
    private String vasType;
    @Expose
    private String isdn;

    public String getVasType() {
        return vasType;
    }

    public void setVasType(String vasType) {
        this.vasType = vasType;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public GetVasForIsdnRequest() {
        super();
    }
}
