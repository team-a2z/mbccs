package com.viettel.mbccs.data.source.remote.request;

import android.support.annotation.StringDef;
import com.google.gson.annotations.SerializedName;

/**
 * Created by HuyQuyet on 7/20/17.
 */

public class GetListStockTypeBySaleServiceCodeRequest extends BaseRequest {

    @StringDef({ SystemCode.TRA_SAU, SystemCode.TRA_TRUOC })
    public @interface SystemCode {
        String TRA_TRUOC = "CM_PRE";
        String TRA_SAU = "CM_POS";
    }

    @SystemCode
    @SerializedName("systemCode")
    private String systemCode;

    @SerializedName("reasonId")
    private String reasonId;

    @SerializedName("telServiceId")
    private String telServiceId;

    @SerializedName("productCode")
    private String productCode;

    public String getSystemCode() {
        return systemCode;
    }

    public void setSystemCode(String systemCode) {
        this.systemCode = systemCode;
    }

    public String getReasonId() {
        return reasonId;
    }

    public void setReasonId(String reasonId) {
        this.reasonId = reasonId;
    }

    public String getTelServiceId() {
        return telServiceId;
    }

    public void setTelServiceId(String telServiceId) {
        this.telServiceId = telServiceId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
}
