package com.viettel.mbccs.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

/**
 * Created by Anh Vu Viet on 7/2/2017.
 */

public class TaskStaffManagement implements Parcelable {

    private Long taskMngtId;
    @Expose
    private Long taskStaffMngtId;
    @Expose
    private String jobDescription;
    @Expose
    private String jobName;
    @Expose
    private Integer status;
    @Expose
    private Integer statusTaskStaff;
    @Expose
    private String createDate;
    @Expose
    private String endDate;

    protected TaskStaffManagement(Parcel in) {
        taskMngtId = in.readLong();
        taskStaffMngtId = in.readLong();
        jobDescription = in.readString();
        jobName = in.readString();
        status = in.readInt();
        statusTaskStaff = in.readInt();
        createDate = in.readString();
        endDate = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(taskMngtId);
        dest.writeLong(taskStaffMngtId);
        dest.writeString(jobDescription);
        dest.writeString(jobName);
        dest.writeInt(status);
        dest.writeInt(statusTaskStaff);
        dest.writeString(createDate);
        dest.writeString(endDate);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TaskStaffManagement> CREATOR = new Creator<TaskStaffManagement>() {
        @Override
        public TaskStaffManagement createFromParcel(Parcel in) {
            return new TaskStaffManagement(in);
        }

        @Override
        public TaskStaffManagement[] newArray(int size) {
            return new TaskStaffManagement[size];
        }
    };

    public Long getTaskMngtId() {
        return taskMngtId;
    }

    public void setTaskMngtId(Long taskMngtId) {
        this.taskMngtId = taskMngtId;
    }

    public Long getTaskStaffMngtId() {
        return taskStaffMngtId;
    }

    public void setTaskStaffMngtId(Long taskStaffMngtId) {
        this.taskStaffMngtId = taskStaffMngtId;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getStatusTaskStaff() {
        return statusTaskStaff;
    }

    public void setStatusTaskStaff(Integer statusTaskStaff) {
        this.statusTaskStaff = statusTaskStaff;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
