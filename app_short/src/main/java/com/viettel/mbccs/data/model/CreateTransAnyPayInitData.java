package com.viettel.mbccs.data.model;

import com.viettel.mbccs.data.source.remote.response.GetApDomainByTypeResponse;
import com.viettel.mbccs.data.source.remote.response.GetListTTKDResponse;

/**
 * Created by minhnx on 7/25/17.
 */

public class CreateTransAnyPayInitData {
    GetApDomainByTypeResponse defaultAmountResponse;
    GetListTTKDResponse shopListResponse;

    public CreateTransAnyPayInitData(GetApDomainByTypeResponse defaultAmountResponse, GetListTTKDResponse shopListResponse) {
        this.defaultAmountResponse = defaultAmountResponse;
        this.shopListResponse = shopListResponse;
    }

    public GetApDomainByTypeResponse getDefaultAmountResponse() {
        return defaultAmountResponse;
    }

    public void setDefaultAmountResponse(GetApDomainByTypeResponse defaultAmountResponse) {
        this.defaultAmountResponse = defaultAmountResponse;
    }

    public GetListTTKDResponse getShopListResponse() {
        return shopListResponse;
    }

    public void setShopListResponse(GetListTTKDResponse shopListResponse) {
        this.shopListResponse = shopListResponse;
    }
}
