package com.viettel.mbccs.data.source.remote.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.viettel.mbccs.data.model.Vas;

import java.util.List;

/**
 * Created by minhnx on 6/7/17.
 */

public class GetVasForIsdnResponse {
    @Expose
    @SerializedName("lstVasUsed")
    private List<Vas> listUsedVases;
    @Expose
    @SerializedName("lstVasInfo")
    private List<Vas> listAvailableVases;

    public List<Vas> getListUsedVases() {
        return listUsedVases;
    }

    public void setListUsedVases(List<Vas> listVas) {
        this.listUsedVases = listVas;
    }

    public List<Vas> getListAvailableVases() {
        return listAvailableVases;
    }

    public void setListAvailableVases(List<Vas> listAvailableVases) {
        this.listAvailableVases = listAvailableVases;
    }
}
