package com.viettel.mbccs.data.source.remote.response;

import com.google.gson.annotations.Expose;
import com.viettel.mbccs.data.model.Shop;

import java.util.List;

/**
 * Created by minhnx on 6/7/17.
 */

public class FindShopByNameResponse {
    @Expose
    private List<Shop> listShop;

    public List<Shop> getListShop() {
        return listShop;
    }

    public void setListShop(List<Shop> listShop) {
        this.listShop = listShop;
    }
}
