package com.viettel.mbccs.data.model;

import com.viettel.mbccs.data.source.remote.response.GetApDomainByTypeResponse;
import com.viettel.mbccs.data.source.remote.response.GetListBtsesResponse;
import com.viettel.mbccs.data.source.remote.response.GetListChannelTypeResponse;

/**
 * Created by minbhnx on 7/25/17.
 */

public class AddBranchInitData {
    GetListChannelTypeResponse channelTypesResponse;
    GetApDomainByTypeResponse documentTypesResponse;
    GetListBtsesResponse listBtsesResponse;

    public AddBranchInitData(GetListChannelTypeResponse channelTypesResponse, GetApDomainByTypeResponse documentTypesResponse, GetListBtsesResponse btsesResponse) {
        this.channelTypesResponse = channelTypesResponse;
        this.documentTypesResponse = documentTypesResponse;
        this.listBtsesResponse = btsesResponse;
    }

    public GetListChannelTypeResponse getChannelTypesResponse() {
        return channelTypesResponse;
    }

    public void setChannelTypesResponse(GetListChannelTypeResponse channelTypesResponse) {
        this.channelTypesResponse = channelTypesResponse;
    }

    public GetApDomainByTypeResponse getDocumentTypesResponse() {
        return documentTypesResponse;
    }

    public void setDocumentTypesResponse(GetApDomainByTypeResponse documentTypesResponse) {
        this.documentTypesResponse = documentTypesResponse;
    }

    public GetListBtsesResponse getListBtsesResponse() {
        return listBtsesResponse;
    }

    public void setListBtsesResponse(GetListBtsesResponse listBtsesResponse) {
        this.listBtsesResponse = listBtsesResponse;
    }
}
