package com.viettel.mbccs.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by HuyQuyet on 7/21/17.
 */

public class SubStockModelRel implements Parcelable {

    @SerializedName("subStockModelRelId")
    @Expose
    private long subStockModelRelId;

    @SerializedName("subId")
    @Expose
    private long subId;

    @SerializedName("saleServiceId")
    @Expose
    private long saleServiceId;

    @SerializedName("stockModelId")
    @Expose
    private long stockModelId;

    @SerializedName("serial")
    @Expose
    private String serial;

    @SerializedName("newSerial")
    @Expose
    private String newSerial;

    @SerializedName("createdDate")
    @Expose
    private String createdDate;

    @SerializedName("saleTransId")
    @Expose
    private long saleTransId;

    @SerializedName("partnerId")
    @Expose
    private long partnerId;

    @SerializedName("partnerName")
    @Expose
    private String partnerName;

    @SerializedName("stockTypeId")
    @Expose
    private long stockTypeId;

    @SerializedName("stockTypeName")
    @Expose
    private String stockTypeName;

    @SerializedName("quantity")
    @Expose
    private long quantity;

    @SerializedName("sourceId")
    @Expose
    private long sourceId;

    @SerializedName("status")
    @Expose
    private long status;

    public SubStockModelRel() {
    }

    protected SubStockModelRel(Parcel in) {
        subStockModelRelId = in.readLong();
        subId = in.readLong();
        saleServiceId = in.readLong();
        stockModelId = in.readLong();
        serial = in.readString();
        newSerial = in.readString();
        createdDate = in.readString();
        saleTransId = in.readLong();
        partnerId = in.readLong();
        partnerName = in.readString();
        stockTypeId = in.readLong();
        stockTypeName = in.readString();
        quantity = in.readLong();
        sourceId = in.readLong();
        status = in.readLong();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(subStockModelRelId);
        dest.writeLong(subId);
        dest.writeLong(saleServiceId);
        dest.writeLong(stockModelId);
        dest.writeString(serial);
        dest.writeString(newSerial);
        dest.writeString(createdDate);
        dest.writeLong(saleTransId);
        dest.writeLong(partnerId);
        dest.writeString(partnerName);
        dest.writeLong(stockTypeId);
        dest.writeString(stockTypeName);
        dest.writeLong(quantity);
        dest.writeLong(sourceId);
        dest.writeLong(status);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SubStockModelRel> CREATOR = new Creator<SubStockModelRel>() {
        @Override
        public SubStockModelRel createFromParcel(Parcel in) {
            return new SubStockModelRel(in);
        }

        @Override
        public SubStockModelRel[] newArray(int size) {
            return new SubStockModelRel[size];
        }
    };

    public long getSubStockModelRelId() {
        return subStockModelRelId;
    }

    public void setSubStockModelRelId(long subStockModelRelId) {
        this.subStockModelRelId = subStockModelRelId;
    }

    public long getSubId() {
        return subId;
    }

    public void setSubId(long subId) {
        this.subId = subId;
    }

    public long getSaleServiceId() {
        return saleServiceId;
    }

    public void setSaleServiceId(long saleServiceId) {
        this.saleServiceId = saleServiceId;
    }

    public long getStockModelId() {
        return stockModelId;
    }

    public void setStockModelId(long stockModelId) {
        this.stockModelId = stockModelId;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getNewSerial() {
        return newSerial;
    }

    public void setNewSerial(String newSerial) {
        this.newSerial = newSerial;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public long getSaleTransId() {
        return saleTransId;
    }

    public void setSaleTransId(long saleTransId) {
        this.saleTransId = saleTransId;
    }

    public long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(long partnerId) {
        this.partnerId = partnerId;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public long getStockTypeId() {
        return stockTypeId;
    }

    public void setStockTypeId(long stockTypeId) {
        this.stockTypeId = stockTypeId;
    }

    public String getStockTypeName() {
        return stockTypeName;
    }

    public void setStockTypeName(String stockTypeName) {
        this.stockTypeName = stockTypeName;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public long getSourceId() {
        return sourceId;
    }

    public void setSourceId(long sourceId) {
        this.sourceId = sourceId;
    }

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }
}
