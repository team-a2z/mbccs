package com.viettel.mbccs.data.source.remote.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by minhnx on 5/29/17.
 */

public class CreateDistributedChannelResponse extends DataResponse {

    @Expose
    private Long staffId;
    @Expose
    private String channelType;
    @Expose
    @SerializedName("lstNameImage")
    private List<String> lstImageNames;

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public List<String> getLstImageNames() {
        return lstImageNames;
    }

    public void setLstImageNames(List<String> lstImageNames) {
        this.lstImageNames = lstImageNames;
    }

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }
}
