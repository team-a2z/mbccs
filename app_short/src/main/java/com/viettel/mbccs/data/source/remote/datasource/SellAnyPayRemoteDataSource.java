package com.viettel.mbccs.data.source.remote.datasource;

import com.viettel.mbccs.data.source.remote.ISellAnyPayRemoteDataSource;
import com.viettel.mbccs.data.source.remote.request.DataRequest;
import com.viettel.mbccs.data.source.remote.request.GetAnypayAuthorizeRequest;
import com.viettel.mbccs.data.source.remote.request.GetListChannelByOwnerTypeIdRequest;
import com.viettel.mbccs.data.source.remote.request.GetListOwnerCodeRequest;
import com.viettel.mbccs.data.source.remote.request.GetListTTKDRequest;
import com.viettel.mbccs.data.source.remote.request.SellAnypayToChannelRequest;
import com.viettel.mbccs.data.source.remote.request.SellAnypayToCustomerRequest;
import com.viettel.mbccs.data.source.remote.response.GetAnypayAuthorizeResponse;
import com.viettel.mbccs.data.source.remote.response.GetListChannelByOwnerTypeIdResponse;
import com.viettel.mbccs.data.source.remote.response.GetListOwneCodeReponse;
import com.viettel.mbccs.data.source.remote.response.GetListTTKDResponse;
import com.viettel.mbccs.data.source.remote.response.SellAnypayToChannelResponse;
import com.viettel.mbccs.data.source.remote.response.SellAnypayToCustomerResponse;
import com.viettel.mbccs.data.source.remote.service.RequestHelper;
import com.viettel.mbccs.utils.rx.SchedulerUtils;

import rx.Observable;

/**
 * Created by eo_cuong on 5/10/17.
 */

public class SellAnyPayRemoteDataSource implements ISellAnyPayRemoteDataSource {

    public volatile static SellAnyPayRemoteDataSource instance;

    public SellAnyPayRemoteDataSource() {

    }

    @Override
    public Observable<GetAnypayAuthorizeResponse> getAnypayAuthorize(DataRequest<GetAnypayAuthorizeRequest> request) {
        return RequestHelper.getRequest()
                .getAnypayAuthorize(request)
                .flatMap(SchedulerUtils.<GetAnypayAuthorizeResponse>convertDataFlatMap())
                .compose(SchedulerUtils.<GetAnypayAuthorizeResponse>applyAsyncSchedulers());
    }

    @Override
    public Observable<SellAnypayToCustomerResponse> sellAnypayToCustomer(DataRequest<SellAnypayToCustomerRequest> request) {
        return RequestHelper.getRequest()
                .saleAnypayToCustomer(request)
                .flatMap(SchedulerUtils.<SellAnypayToCustomerResponse>convertDataFlatMap())
                .compose(SchedulerUtils.<SellAnypayToCustomerResponse>applyAsyncSchedulers());
    }

    @Override
    public Observable<SellAnypayToChannelResponse> sellAnypayToChannel(DataRequest<SellAnypayToChannelRequest> request) {
        return RequestHelper.getRequest()
                .saleAnypayToChannel(request)
                .flatMap(SchedulerUtils.<SellAnypayToChannelResponse>convertDataFlatMap())
                .compose(SchedulerUtils.<SellAnypayToChannelResponse>applyAsyncSchedulers());
    }

//    @Override
//    public Observable<FindShopByNameResponse> findShopByName(DataRequest<FindShopByNameRequest> request) {
//        return RequestHelper.getRequest()
//                .findShopByName(request)
//                .flatMap(SchedulerUtils.<FindShopByNameResponse>convertDataFlatMap())
//                .compose(SchedulerUtils.<FindShopByNameResponse>applyAsyncSchedulers());
//    }
//
//    @Override
//    public Observable<FindStaffByNameResponse> findStaffByName(DataRequest<FindStaffByNameRequest> request) {
//        return RequestHelper.getRequest()
//                .findStaffByName(request)
//                .flatMap(SchedulerUtils.<FindStaffByNameResponse>convertDataFlatMap())
//                .compose(SchedulerUtils.<FindStaffByNameResponse>applyAsyncSchedulers());
//    }
//
//    @Override
//    public Observable<FindChannelByNameResponse> findChannelByName(DataRequest<FindChannelByNameRequest> request) {
//        return RequestHelper.getRequest()
//                .findChannelByName(request)
//                .flatMap(SchedulerUtils.<FindChannelByNameResponse>convertDataFlatMap())
//                .compose(SchedulerUtils.<FindChannelByNameResponse>applyAsyncSchedulers());
//    }


    @Override
    public Observable<GetListTTKDResponse> getListTTKD(DataRequest<GetListTTKDRequest> request) {
        return RequestHelper.getRequest()
                .getListTTKD(request)
                .flatMap(SchedulerUtils.<GetListTTKDResponse>convertDataFlatMap())
                .compose(SchedulerUtils.<GetListTTKDResponse>applyAsyncSchedulers());
    }

    @Override
    public Observable<GetListOwneCodeReponse> getListOwnerCode(DataRequest<GetListOwnerCodeRequest> request) {
        return RequestHelper.getRequest()
                .getListOwnerCode(request)
                .flatMap(SchedulerUtils.<GetListOwneCodeReponse>convertDataFlatMap())
                .compose(SchedulerUtils.<GetListOwneCodeReponse>applyAsyncSchedulers());
    }

    @Override
    public Observable<GetListChannelByOwnerTypeIdResponse> getListChannelByOwnerTypeId(DataRequest<GetListChannelByOwnerTypeIdRequest> request) {
        return RequestHelper.getRequest()
                .getListChannelByOwnerTypeId(request)
                .flatMap(SchedulerUtils.<GetListChannelByOwnerTypeIdResponse>convertDataFlatMap())
                .compose(SchedulerUtils.<GetListChannelByOwnerTypeIdResponse>applyAsyncSchedulers());
    }

    public static SellAnyPayRemoteDataSource getInstance() {
        if (instance == null) {
            instance = new SellAnyPayRemoteDataSource();
        }
        return instance;
    }
}
