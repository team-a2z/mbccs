package com.viettel.mbccs.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FRAMGIA\vu.viet.anh on 17/07/2017.
 */

public class InfoTaskExtend implements Parcelable {

    @SerializedName("jobName")
    @Expose
    private String jobName;
    @SerializedName("statusTaskStaff")
    @Expose
    private int statusTaskStaff;
    @SerializedName("taskStaffMngtId")
    @Expose
    private int taskStaffMngtId;
    @SerializedName("endDate")
    @Expose
    private String endDate;
    @SerializedName("taskMngtId")
    @Expose
    private int taskMngtId;
    @SerializedName("jobDescription")
    @Expose
    private String jobDescription;
    @SerializedName("createDate")
    @Expose
    private String createDate;
    @SerializedName("status")
    @Expose
    private int status;

    protected InfoTaskExtend(Parcel in) {
        jobName = in.readString();
        statusTaskStaff = in.readInt();
        taskStaffMngtId = in.readInt();
        endDate = in.readString();
        taskMngtId = in.readInt();
        jobDescription = in.readString();
        createDate = in.readString();
        status = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(jobName);
        dest.writeInt(statusTaskStaff);
        dest.writeInt(taskStaffMngtId);
        dest.writeString(endDate);
        dest.writeInt(taskMngtId);
        dest.writeString(jobDescription);
        dest.writeString(createDate);
        dest.writeInt(status);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<InfoTaskExtend> CREATOR = new Creator<InfoTaskExtend>() {
        @Override
        public InfoTaskExtend createFromParcel(Parcel in) {
            return new InfoTaskExtend(in);
        }

        @Override
        public InfoTaskExtend[] newArray(int size) {
            return new InfoTaskExtend[size];
        }
    };

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public int getStatusTaskStaff() {
        return statusTaskStaff;
    }

    public void setStatusTaskStaff(int statusTaskStaff) {
        this.statusTaskStaff = statusTaskStaff;
    }

    public int getTaskStaffMngtId() {
        return taskStaffMngtId;
    }

    public void setTaskStaffMngtId(int taskStaffMngtId) {
        this.taskStaffMngtId = taskStaffMngtId;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getTaskMngtId() {
        return taskMngtId;
    }

    public void setTaskMngtId(int taskMngtId) {
        this.taskMngtId = taskMngtId;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
