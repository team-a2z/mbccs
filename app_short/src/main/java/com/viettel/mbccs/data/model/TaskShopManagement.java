package com.viettel.mbccs.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.IntDef;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Anh Vu Viet on 5/20/2017.
 */

public class TaskShopManagement implements Parcelable {

    @SerializedName("taskShopMngtId")
    @Expose
    private int taskShopMngtId;
    @SerializedName("reasonId")
    @Expose
    private int reasonId;
    @SerializedName("taskMngtId")
    @Expose
    private int taskMngtId;
    @SerializedName("jobName")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("progress")
    @Expose
    private String progress;
    @SerializedName("shopId")
    @Expose
    private int shopId;
    @SerializedName("createDate")
    @Expose
    private String createDate;
    @SerializedName("stageId")
    @Expose
    private int stageId;
    @SerializedName("status")
    @Expose
    private int status;

    @IntDef({TaskType.TYPE_SU_CO_CC, TaskType.TYPE_KHAO_SAT, TaskType.TYPE_TRIEN_KHAI_MOI,
            TaskType.TYPE_THAY_DOI_LAP_DAT, TaskType.TYPE_KIEM_TRA_CAP, TaskType.TYPE_KIEM_TRA_TRAM,
            TaskType.TYPE_BAO_DUONG_TRAM, TaskType.TYPE_PHAT_SINH, TaskType.TYPE_CSKPP
    })
    public @interface TaskType {
        int TYPE_SU_CO_CC = 1;
        int TYPE_KHAO_SAT = 2;
        int TYPE_TRIEN_KHAI_MOI = 3;
        int TYPE_THAY_DOI_LAP_DAT = 4;
        int TYPE_KIEM_TRA_CAP = 5;
        int TYPE_KIEM_TRA_TRAM = 6;
        int TYPE_BAO_DUONG_TRAM = 7;
        int TYPE_PHAT_SINH = 8;
        int TYPE_CSKPP = 9;
    }

    @IntDef({TaskStatus.STATUS_NEW, TaskStatus.STATUS_ASSIGNED, TaskStatus.STATUS_FINISHED, TaskStatus.STATUS_REJECTED
    })
    public @interface TaskStatus {
        int STATUS_NEW = 0;
        int STATUS_ASSIGNED = 1;
        int STATUS_FINISHED = 2;
        int STATUS_REJECTED = 3;
    }

    protected TaskShopManagement(Parcel in) {
        taskShopMngtId = in.readInt();
        reasonId = in.readInt();
        taskMngtId = in.readInt();
        name = in.readString();
        description = in.readString();
        progress = in.readString();
        shopId = in.readInt();
        createDate = in.readString();
        stageId = in.readInt();
        status = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(taskShopMngtId);
        dest.writeInt(reasonId);
        dest.writeInt(taskMngtId);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(progress);
        dest.writeInt(shopId);
        dest.writeString(createDate);
        dest.writeInt(stageId);
        dest.writeInt(status);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TaskShopManagement> CREATOR = new Creator<TaskShopManagement>() {
        @Override
        public TaskShopManagement createFromParcel(Parcel in) {
            return new TaskShopManagement(in);
        }

        @Override
        public TaskShopManagement[] newArray(int size) {
            return new TaskShopManagement[size];
        }
    };

    public int getTaskShopMngtId() {
        return taskShopMngtId;
    }

    public void setTaskShopMngtId(int taskShopMngtId) {
        this.taskShopMngtId = taskShopMngtId;
    }

    public int getReasonId() {
        return reasonId;
    }

    public void setReasonId(int reasonId) {
        this.reasonId = reasonId;
    }

    public int getTaskMngtId() {
        return taskMngtId;
    }

    public void setTaskMngtId(int taskMngtId) {
        this.taskMngtId = taskMngtId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public int getStageId() {
        return stageId;
    }

    public void setStageId(int stageId) {
        this.stageId = stageId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
