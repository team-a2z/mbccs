package com.viettel.mbccs.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by HuyQuyet on 7/20/17.
 */

public class StockModel_1 implements Parcelable {
    @SerializedName("stockModelId")
    @Expose
    private long stockModelId;

    @SerializedName("stockModelCode")
    @Expose
    private String stockModelCode;

    @SerializedName("stockModelName")
    @Expose
    private String stockModelName;

    @SerializedName("stateId")
    @Expose
    private long stateId;

    @SerializedName("serial")
    @Expose
    private String serial;

    @SerializedName("quantityIssue")
    @Expose
    private long quantity;

    public StockModel_1() {

    }

    protected StockModel_1(Parcel in) {
        stockModelId = in.readLong();
        stockModelCode = in.readString();
        stockModelName = in.readString();
        stateId = in.readLong();
        serial = in.readString();
        quantity = in.readLong();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(stockModelId);
        dest.writeString(stockModelCode);
        dest.writeString(stockModelName);
        dest.writeLong(stateId);
        dest.writeString(serial);
        dest.writeLong(quantity);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<StockModel_1> CREATOR = new Creator<StockModel_1>() {
        @Override
        public StockModel_1 createFromParcel(Parcel in) {
            return new StockModel_1(in);
        }

        @Override
        public StockModel_1[] newArray(int size) {
            return new StockModel_1[size];
        }
    };

    public long getStockModelId() {
        return stockModelId;
    }

    public void setStockModelId(long stockModelId) {
        this.stockModelId = stockModelId;
    }

    public String getStockModelCode() {
        return stockModelCode;
    }

    public void setStockModelCode(String stockModelCode) {
        this.stockModelCode = stockModelCode;
    }

    public String getStockModelName() {
        return stockModelName;
    }

    public void setStockModelName(String stockModelName) {
        this.stockModelName = stockModelName;
    }

    public long getStateId() {
        return stateId;
    }

    public void setStateId(long stateId) {
        this.stateId = stateId;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }
}
