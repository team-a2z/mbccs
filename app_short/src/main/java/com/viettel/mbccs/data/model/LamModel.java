package com.viettel.mbccs.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


/**
 * Created by jacky on 6/23/17.
 */

public class LamModel implements Serializable{

    @SerializedName("reservedPort")
    @Expose
    private int reservedPort;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("productId")
    @Expose
    private int productId;

    @SerializedName("province")
    @Expose
    private String province;

    @SerializedName("dslamId")
    @Expose
    private long dslamId;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("usedPort")
    @Expose
    private int usedPort;

    @SerializedName("maxPort")
    @Expose
    private int maxPort;

    @SerializedName("shopId")
    @Expose
    private int shopId;

    @SerializedName("dslamIp")
    @Expose
    private String dslamIp;

    @SerializedName("status")
    @Expose
    private int status;


    public int getReservedPort() {
        return reservedPort;
    }

    public void setReservedPort(int reservedPort) {
        this.reservedPort = reservedPort;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public long getDslamId() {
        return dslamId;
    }

    public void setDslamId(long dslamId) {
        this.dslamId = dslamId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getUsedPort() {
        return usedPort;
    }

    public void setUsedPort(int usedPort) {
        this.usedPort = usedPort;
    }

    public int getMaxPort() {
        return maxPort;
    }

    public void setMaxPort(int maxPort) {
        this.maxPort = maxPort;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public String getDslamIp() {
        return dslamIp;
    }

    public void setDslamIp(String dslamIp) {
        this.dslamIp = dslamIp;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
