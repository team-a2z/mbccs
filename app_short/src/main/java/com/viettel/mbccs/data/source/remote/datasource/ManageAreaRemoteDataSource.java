package com.viettel.mbccs.data.source.remote.datasource;

import com.viettel.mbccs.data.source.remote.IManageAreaRemoteDataSource;
import com.viettel.mbccs.data.source.remote.request.DataRequest;
import com.viettel.mbccs.data.source.remote.request.GetAreaInfoRequest;
import com.viettel.mbccs.data.source.remote.request.GetListBtsesByAreaCodeRequest;
import com.viettel.mbccs.data.source.remote.request.GetListEventsByAreaCodeRequest;
import com.viettel.mbccs.data.source.remote.request.GetListShopsByAreaCodeRequest;
import com.viettel.mbccs.data.source.remote.request.UpdateBtsGpsRequest;
import com.viettel.mbccs.data.source.remote.response.GetAreaInfoResponse;
import com.viettel.mbccs.data.source.remote.response.GetListBtsesByAreaCodeResponse;
import com.viettel.mbccs.data.source.remote.response.GetListEventsByAreaCodeResponse;
import com.viettel.mbccs.data.source.remote.response.GetListShopsByAreaCodeResponse;
import com.viettel.mbccs.data.source.remote.response.UpdateBtsGpsResponse;
import com.viettel.mbccs.data.source.remote.service.RequestHelper;
import com.viettel.mbccs.utils.rx.SchedulerUtils;

import rx.Observable;

/**
 * Created by eo_cuong on 5/10/17.
 */

public class ManageAreaRemoteDataSource implements IManageAreaRemoteDataSource {

    public volatile static ManageAreaRemoteDataSource instance;

    public ManageAreaRemoteDataSource() {

    }

    public static ManageAreaRemoteDataSource getInstance() {
        if (instance == null) {
            instance = new ManageAreaRemoteDataSource();
        }
        return instance;
    }

    @Override
    public Observable<GetAreaInfoResponse> getAreaInfo(DataRequest<GetAreaInfoRequest> request) {
        return RequestHelper.getRequest()
                .getAreaInfo(request)
                .flatMap(SchedulerUtils.<GetAreaInfoResponse>convertDataFlatMap())
                .compose(SchedulerUtils.<GetAreaInfoResponse>applyAsyncSchedulers());
    }

    @Override
    public Observable<GetListBtsesByAreaCodeResponse> getListBtsesByAreaCode(DataRequest<GetListBtsesByAreaCodeRequest> request) {
        return RequestHelper.getRequest()
                .getListBtsesByAreaCode(request)
                .flatMap(SchedulerUtils.<GetListBtsesByAreaCodeResponse>convertDataFlatMap())
                .compose(SchedulerUtils.<GetListBtsesByAreaCodeResponse>applyAsyncSchedulers());
    }

    @Override
    public Observable<GetListShopsByAreaCodeResponse> getListShopsByAreaCode(DataRequest<GetListShopsByAreaCodeRequest> request) {
        return RequestHelper.getRequest()
                .getListShopsByAreaCode(request)
                .flatMap(SchedulerUtils.<GetListShopsByAreaCodeResponse>convertDataFlatMap())
                .compose(SchedulerUtils.<GetListShopsByAreaCodeResponse>applyAsyncSchedulers());
    }

    @Override
    public Observable<GetListEventsByAreaCodeResponse> getListEventsByAreaCode(DataRequest<GetListEventsByAreaCodeRequest> request) {
        return RequestHelper.getRequest()
                .getListEventsByAreaCode(request)
                .flatMap(SchedulerUtils.<GetListEventsByAreaCodeResponse>convertDataFlatMap())
                .compose(SchedulerUtils.<GetListEventsByAreaCodeResponse>applyAsyncSchedulers());
    }

    @Override
    public Observable<UpdateBtsGpsResponse> updateBtsGps(DataRequest<UpdateBtsGpsRequest> request) {
        return RequestHelper.getRequest()
                .updateBtsGps(request)
                .flatMap(SchedulerUtils.<UpdateBtsGpsResponse>convertDataFlatMap())
                .compose(SchedulerUtils.<UpdateBtsGpsResponse>applyAsyncSchedulers());
    }
}
