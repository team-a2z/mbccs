package com.viettel.mbccs.data.source.remote.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.viettel.mbccs.data.model.StaffInfo;
import com.viettel.mbccs.data.model.TaskStaffManagement;

import java.util.List;

/**
 * Created by Anh Vu Viet on 6/23/2017.
 */

public class GetTaskPrepareAssignStaffResponse extends DataResponse {

    @SerializedName("lstInfoTaskExtend")
    @Expose
    private List<TaskStaffManagement> lstInfoTaskExtend = null;

    @SerializedName("lstStaff")
    @Expose
    private List<StaffInfo> lstStaff = null;

    public List<TaskStaffManagement> getLstInfoTaskExtend() {
        return lstInfoTaskExtend;
    }

    public void setLstInfoTaskExtend(List<TaskStaffManagement> lstInfoTaskExtend) {
        this.lstInfoTaskExtend = lstInfoTaskExtend;
    }

    public List<StaffInfo> getLstStaff() {
        return lstStaff;
    }

    public void setLstStaff(List<StaffInfo> lstStaff) {
        this.lstStaff = lstStaff;
    }
}
