package com.viettel.mbccs.data.source.local.datasource;

import com.google.gson.Gson;
import com.viettel.mbccs.data.source.local.IBranchesLocalDataSource;

/**
 * Created by eo_cuong on 5/10/17.
 */

public class BranchesLocalDataSource implements IBranchesLocalDataSource {
    private SharedPrefs sharedPrefs;
    private Gson gson;
    public static BranchesLocalDataSource instance;

    public BranchesLocalDataSource() {
        sharedPrefs = SharedPrefs.getInstance();
        gson = new Gson();
    }

    public static BranchesLocalDataSource getInstance() {
        if (instance == null) {
            instance = new BranchesLocalDataSource();
        }
        return instance;
    }


}
