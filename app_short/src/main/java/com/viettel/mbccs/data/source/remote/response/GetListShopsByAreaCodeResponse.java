package com.viettel.mbccs.data.source.remote.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.viettel.mbccs.data.model.Shop;

import java.util.List;

/**
 * Created by minhnx on 5/29/17.
 */

public class GetListShopsByAreaCodeResponse extends DataResponse {
    @Expose
    @SerializedName("listShop")
    private List<Shop> lstShop;

    public List<Shop> getLstShop() {
        return lstShop;
    }

    public void setLstShop(List<Shop> lstShop) {
        this.lstShop = lstShop;
    }
}
