package com.viettel.mbccs.data.source.remote.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Da Qiao on 8/18/2017.
 */

public class GetDashBoardInfoRequest {

    @SerializedName("shopId")
    @Expose
    private String shopId;

    @SerializedName("staffId")
    @Expose
    private String staffId;

    @SerializedName("lstFunction")
    private List<String> lstFunction;

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public List<String> getLstFunction() {
        return lstFunction;
    }

    public void setLstFunction(List<String> lstFunction) {
        this.lstFunction = lstFunction;
    }
}
