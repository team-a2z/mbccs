package com.viettel.mbccs.data.source.local.datasource;

import com.google.gson.Gson;
import com.viettel.mbccs.data.source.local.IChangeSimLocalDataSource;

/**
 * Created by eo_cuong on 5/10/17.
 */

public class ChangeSimLocalDataSource implements IChangeSimLocalDataSource {
    private SharedPrefs sharedPrefs;
    private Gson gson;
    public static ChangeSimLocalDataSource instance;

    public ChangeSimLocalDataSource() {
        sharedPrefs = SharedPrefs.getInstance();
        gson = new Gson();
    }

    public static ChangeSimLocalDataSource getInstance() {
        if (instance == null) {
            instance = new ChangeSimLocalDataSource();
        }
        return instance;
    }
}
