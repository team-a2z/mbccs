package com.viettel.mbccs.data.source.local.datasource;

import com.google.gson.Gson;
import com.viettel.mbccs.data.source.local.IManageAreaLocalDataSource;

/**
 * Created by eo_cuong on 5/10/17.
 */

public class ManageAreaLocalDataSource implements IManageAreaLocalDataSource {
    private SharedPrefs sharedPrefs;
    private Gson gson;
    public static ManageAreaLocalDataSource instance;

    public ManageAreaLocalDataSource() {
        sharedPrefs = SharedPrefs.getInstance();
        gson = new Gson();
    }

    public static ManageAreaLocalDataSource getInstance() {
        if (instance == null) {
            instance = new ManageAreaLocalDataSource();
        }
        return instance;
    }


}
