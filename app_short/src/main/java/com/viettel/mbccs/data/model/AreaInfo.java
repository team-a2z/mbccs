package com.viettel.mbccs.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by minhnx on 10/12/17.
 */

public class AreaInfo implements Serializable {
    @Expose
    private String climate;
    @Expose
    private String name;
    @Expose
    @SerializedName("numOf2GBTS")
    private Long numOf2GBTS;
    @Expose
    @SerializedName("numOf3GBTS")
    private Long numOf3GBTS;
    @Expose
    @SerializedName("numOf4GBTS")
    private Long numOf4GBTS;
    @Expose
    private Long numOfChannel;
    @Expose
    @SerializedName("numOfCollage")
    private Long numOfCollege;
    @Expose
    private Long numOfHighSchool;
    @Expose
    private Long numOfHospital;
    @Expose
    private Long numOfIndustrialPark;
    @Expose
    private Long numOfPrimarySchool;
    @Expose
    private Long numOfSecondarySchool;
    @Expose
    private Long numOfUniversity;
    @Expose
    private Long population;
    @Expose
    private Long surfaceArea;

    public String getClimate() {
        return climate;
    }

    public void setClimate(String climate) {
        this.climate = climate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getNumOf2GBTS() {
        return numOf2GBTS;
    }

    public void setNumOf2GBTS(Long numOf2GBTS) {
        this.numOf2GBTS = numOf2GBTS;
    }

    public Long getNumOf3GBTS() {
        return numOf3GBTS;
    }

    public void setNumOf3GBTS(Long numOf3GBTS) {
        this.numOf3GBTS = numOf3GBTS;
    }

    public Long getNumOf4GBTS() {
        return numOf4GBTS;
    }

    public void setNumOf4GBTS(Long numOf4GBTS) {
        this.numOf4GBTS = numOf4GBTS;
    }

    public Long getNumOfChannel() {
        return numOfChannel;
    }

    public void setNumOfChannel(Long numOfChannel) {
        this.numOfChannel = numOfChannel;
    }

    public Long getNumOfCollege() {
        return numOfCollege;
    }

    public void setNumOfCollege(Long numOfCollege) {
        this.numOfCollege = numOfCollege;
    }

    public Long getNumOfHighSchool() {
        return numOfHighSchool;
    }

    public void setNumOfHighSchool(Long numOfHighSchool) {
        this.numOfHighSchool = numOfHighSchool;
    }

    public Long getNumOfHospital() {
        return numOfHospital;
    }

    public void setNumOfHospital(Long numOfHospital) {
        this.numOfHospital = numOfHospital;
    }

    public Long getNumOfIndustrialPark() {
        return numOfIndustrialPark;
    }

    public void setNumOfIndustrialPark(Long numOfIndustrialPark) {
        this.numOfIndustrialPark = numOfIndustrialPark;
    }

    public Long getNumOfPrimarySchool() {
        return numOfPrimarySchool;
    }

    public void setNumOfPrimarySchool(Long numOfPrimarySchool) {
        this.numOfPrimarySchool = numOfPrimarySchool;
    }

    public Long getNumOfSecondarySchool() {
        return numOfSecondarySchool;
    }

    public void setNumOfSecondarySchool(Long numOfSecondarySchool) {
        this.numOfSecondarySchool = numOfSecondarySchool;
    }

    public Long getNumOfUniversity() {
        return numOfUniversity;
    }

    public void setNumOfUniversity(Long numOfUniversity) {
        this.numOfUniversity = numOfUniversity;
    }

    public Long getPopulation() {
        return population;
    }

    public void setPopulation(Long population) {
        this.population = population;
    }

    public Long getSurfaceArea() {
        return surfaceArea;
    }

    public void setSurfaceArea(Long surfaceArea) {
        this.surfaceArea = surfaceArea;
    }
}
