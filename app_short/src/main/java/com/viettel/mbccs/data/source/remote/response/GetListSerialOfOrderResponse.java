package com.viettel.mbccs.data.source.remote.response;

import com.viettel.mbccs.data.model.SerialBO;

import java.util.List;

/**
 * Created by HuyQuyet on 5/31/17.
 */

public class GetListSerialOfOrderResponse extends DataResponse {

    private List<SerialBO> lstSaleTransSerial;

    public List<SerialBO> getLstSaleTransSerial() {
        return lstSaleTransSerial;
    }

    public void setLstSaleTransSerial(List<SerialBO> lstSaleTransSerial) {
        this.lstSaleTransSerial = lstSaleTransSerial;
    }
}
