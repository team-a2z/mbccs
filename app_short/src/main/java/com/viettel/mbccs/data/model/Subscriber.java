package com.viettel.mbccs.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Created by HuyQuyet on 5/31/17.
 */

public class Subscriber implements Parcelable {

    @SerializedName("subId")
    @Expose
    private long subId;

    @SerializedName("custReqId")
    @Expose
    private long custReqId;

    @SerializedName("isSatisfy")
    @Expose
    private long isSatisfy;

    @SerializedName("contractId")
    @Expose
    private long contractId;

    @SerializedName("productCode")
    @Expose
    private String productCode;

    @SerializedName("actStatus")
    @Expose
    private String actStatus;

    @SerializedName("staDatetime")
    @Expose
    private String staDatetime;

    @SerializedName("endDatetime")
    @Expose
    private String endDatetime;

    @SerializedName("status")
    @Expose
    private long status;

    @SerializedName("statusName")
    @Expose
    private String statusName;

    @SerializedName("finishReasonId")
    @Expose
    private long finishReasonId;

    @SerializedName("regReasonId")
    @Expose
    private long regReasonId;

    @SerializedName("createDate")
    @Expose
    private String createDate;

    @SerializedName("userCreated")
    @Expose
    private String userCreated;

    @SerializedName("shopCode")
    @Expose
    private String shopCode;

    @SerializedName("shopName")
    @Expose
    private String shopName;

    @SerializedName("subType")
    @Expose
    private String subType;

    @SerializedName("quota")
    @Expose
    private long quota;

    @SerializedName("promotionCode")
    @Expose
    private String promotionCode;

    @SerializedName("regType")
    @Expose
    private String regType;

    @SerializedName("reasonDepositId")
    @Expose
    private long reasonDepositId;

    @SerializedName("deposit")
    @Expose
    private String deposit;

    @SerializedName("ipStatic")
    @Expose
    private String ipStatic;

    @SerializedName("quotaOver")
    @Expose
    private long quotaOver;

    @SerializedName("userUsing")
    @Expose
    private String userUsing;

    @SerializedName("userTitle")
    @Expose
    private String userTitle;

    @SerializedName("staffId")
    @Expose
    private long staffId;

    @SerializedName("staffName")
    @Expose
    private String staffName;

    @SerializedName("password")
    @Expose
    private String password;

    @SerializedName("changeDatetime")
    @Expose
    private String changeDatetime;

    @SerializedName("offerName")
    @Expose
    private String offerName;

    @SerializedName("custId")
    @Expose
    private long custId;

    @SerializedName("custName")
    @Expose
    private String custName;

    @SerializedName("limitDate")
    @Expose
    private String limitDate;

    @SerializedName("province")
    @Expose
    private String province;

    @SerializedName("district")
    @Expose
    private String district;

    @SerializedName("precinct")
    @Expose
    private String precinct;

    @SerializedName("streetBlock")
    @Expose
    private String streetBlock;

    @SerializedName("teamId")
    @Expose
    private long teamId;

    @SerializedName("vip")
    @Expose
    private String vip;

    @SerializedName("debit")
    @Expose
    private String debit;

    @SerializedName("deployAreaCode")
    @Expose
    private String deployAreaCode;

    @SerializedName("deployAreaCodeEnd")
    @Expose
    private String deployAreaCodeEnd;

    @SerializedName("imsi")
    @Expose
    private String imsi;

    @SerializedName("serviceType")
    @Expose
    private String serviceType;

    @SerializedName("account")
    @Expose
    private String account;

    @SerializedName("data")
    @Expose
    private String data;

    @SerializedName("serial")
    @Expose
    private String serial;

    @SerializedName("isInfoCompleted")
    @Expose
    private long isInfoCompleted;

    @SerializedName("addressCode")
    @Expose
    private String addressCode;

    @SerializedName("passwordATM")
    @Expose
    private String passwordATM;

    @SerializedName("birthDate")
    @Expose
    private String birthDate;

    @SerializedName("nationality")
    @Expose
    private String nationality;

    @SerializedName("susexbId")
    @Expose
    private String sex;

    @SerializedName("street")
    @Expose
    private String street;

    @SerializedName("streetName")
    @Expose
    private String streetName;

    @SerializedName("streetBlockName")
    @Expose
    private String streetBlockName;

    @SerializedName("home")
    @Expose
    private String home;

    @SerializedName("telecomServiceID")
    @Expose
    private long telecomServiceID;

    @SerializedName("paymentOrder")
    @Expose
    private long paymentOrder;

    @SerializedName("locationName")
    @Expose
    private String locationName;

    @SerializedName("locationCode")
    @Expose
    private String locationCode;

    @SerializedName("chanelTypeId")
    @Expose
    private long chanelTypeId;

    @SerializedName("adslIsdn")
    @Expose
    private String adslIsdn;

    @SerializedName("firstConnect")
    @Expose
    private String firstConnect;

    @SerializedName("deployAddressEnd")
    @Expose
    private String deployAddressEnd;

    @SerializedName("deployAddress")
    @Expose
    private String deployAddress;

    @SerializedName("recordWorkId")
    @Expose
    public long recordWorkId;

    @SerializedName("type")
    @Expose
    public String type;

    @SerializedName("gponGroupAccountId")
    @Expose
    private long gponGroupAccountId;

    @SerializedName("serviceName")
    @Expose
    private String serviceName;

    @SerializedName("contractNo")
    @Expose
    private String contractNo;

    @SerializedName("subidNoId")
    @Expose
    private String idNo;

    @SerializedName("contact")
    @Expose
    private String contact;

    @SerializedName("lineType")
    @Expose
    private String lineType;

    @SerializedName("pricePlan")
    @Expose
    private long pricePlan;

    @SerializedName("localPricePlan")
    @Expose
    private long localPricePlan;

    @SerializedName("descStatus")
    @Expose
    private String descStatus;

    @SerializedName("descActStatus")
    @Expose
    private String descActStatus;

    @SerializedName("telFax")
    @Expose
    private String telFax;

    @SerializedName("telMobile")
    @Expose
    private String telMobile;

    @SerializedName("technology")
    @Expose
    private String technology;

    @SerializedName("vlan")
    @Expose
    private String vlan;

    @SerializedName("isUplink")
    @Expose
    private long isUplink;

    @SerializedName("numOfComputer")
    @Expose
    private long numOfComputer;

    @SerializedName("adslAccount")
    @Expose
    private String adslAccount;

    @SerializedName("ipLan")
    @Expose
    private String ipLan;

    @SerializedName("ipWan")
    @Expose
    private String ipWan;

    @SerializedName("ipGateway")
    @Expose
    private String ipGateway;

    @SerializedName("ipRouter")
    @Expose
    private String ipRouter;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("speed")
    @Expose
    private long speed;

    @SerializedName("localSpeed")
    @Expose
    private long localSpeed;

    @SerializedName("sourceId")
    @Expose
    private long sourceId;

    @SerializedName("parentSubId")
    @Expose
    private long parentSubId;

    @SerializedName("upLink")
    @Expose
    private long upLink;

    @SerializedName("groupAccountId")
    @Expose
    private long groupAccountId;

    @SerializedName("numResetZone")
    @Expose
    private long numResetZone;

    @SerializedName("isdn")
    @Expose
    private String isdn;

    @SerializedName("dslamId")
    @Expose
    private long dslamId;

    @SerializedName("prepaidValue")
    @Expose
    private String prepaidValue;

    @SerializedName("prepaidBilling")
    @Expose
    private long prepaidBilling;

    @SerializedName("payType")
    @Expose
    private String payType;

    @SerializedName("portNo")
    @Expose
    private String portNo;

    @SerializedName("userID")
    @Expose
    private long userID;

    @SerializedName("customer")
    @Expose
    private Customer customer;


    @SerializedName("listSubStockModelRel")
    @Expose
    private List<SubStockModelRel> listSubStockModelRel;

    public Subscriber() {

    }

    protected Subscriber(Parcel in) {
        subId = in.readLong();
        custReqId = in.readLong();
        isSatisfy = in.readLong();
        contractId = in.readLong();
        productCode = in.readString();
        actStatus = in.readString();
        staDatetime = in.readString();
        endDatetime = in.readString();
        status = in.readLong();
        statusName = in.readString();
        finishReasonId = in.readLong();
        regReasonId = in.readLong();
        createDate = in.readString();
        userCreated = in.readString();
        shopCode = in.readString();
        shopName = in.readString();
        subType = in.readString();
        quota = in.readLong();
        promotionCode = in.readString();
        regType = in.readString();
        reasonDepositId = in.readLong();
        deposit = in.readString();
        ipStatic = in.readString();
        quotaOver = in.readLong();
        userUsing = in.readString();
        userTitle = in.readString();
        staffId = in.readLong();
        staffName = in.readString();
        password = in.readString();
        changeDatetime = in.readString();
        offerName = in.readString();
        custId = in.readLong();
        custName = in.readString();
        limitDate = in.readString();
        province = in.readString();
        district = in.readString();
        precinct = in.readString();
        streetBlock = in.readString();
        teamId = in.readLong();
        vip = in.readString();
        debit = in.readString();
        deployAreaCode = in.readString();
        deployAreaCodeEnd = in.readString();
        imsi = in.readString();
        serviceType = in.readString();
        account = in.readString();
        data = in.readString();
        serial = in.readString();
        isInfoCompleted = in.readLong();
        addressCode = in.readString();
        passwordATM = in.readString();
        birthDate = in.readString();
        nationality = in.readString();
        sex = in.readString();
        street = in.readString();
        streetName = in.readString();
        streetBlockName = in.readString();
        home = in.readString();
        telecomServiceID = in.readLong();
        paymentOrder = in.readLong();
        locationName = in.readString();
        locationCode = in.readString();
        chanelTypeId = in.readLong();
        adslIsdn = in.readString();
        firstConnect = in.readString();
        deployAddressEnd = in.readString();
        deployAddress = in.readString();
        recordWorkId = in.readLong();
        type = in.readString();
        gponGroupAccountId = in.readLong();
        serviceName = in.readString();
        contractNo = in.readString();
        idNo = in.readString();
        contact = in.readString();
        lineType = in.readString();
        pricePlan = in.readLong();
        localPricePlan = in.readLong();
        descStatus = in.readString();
        descActStatus = in.readString();
        telFax = in.readString();
        telMobile = in.readString();
        technology = in.readString();
        vlan = in.readString();
        isUplink = in.readLong();
        numOfComputer = in.readLong();
        adslAccount = in.readString();
        ipLan = in.readString();
        ipWan = in.readString();
        ipGateway = in.readString();
        ipRouter = in.readString();
        email = in.readString();
        speed = in.readLong();
        localSpeed = in.readLong();
        sourceId = in.readLong();
        parentSubId = in.readLong();
        upLink = in.readLong();
        groupAccountId = in.readLong();
        numResetZone = in.readLong();
        isdn = in.readString();
        dslamId = in.readLong();
        prepaidValue = in.readString();
        prepaidBilling = in.readLong();
        payType = in.readString();
        portNo = in.readString();
        userID = in.readLong();
        customer = in.readParcelable(Customer.class.getClassLoader());
        listSubStockModelRel = in.createTypedArrayList(SubStockModelRel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(subId);
        dest.writeLong(custReqId);
        dest.writeLong(isSatisfy);
        dest.writeLong(contractId);
        dest.writeString(productCode);
        dest.writeString(actStatus);
        dest.writeString(staDatetime);
        dest.writeString(endDatetime);
        dest.writeLong(status);
        dest.writeString(statusName);
        dest.writeLong(finishReasonId);
        dest.writeLong(regReasonId);
        dest.writeString(createDate);
        dest.writeString(userCreated);
        dest.writeString(shopCode);
        dest.writeString(shopName);
        dest.writeString(subType);
        dest.writeLong(quota);
        dest.writeString(promotionCode);
        dest.writeString(regType);
        dest.writeLong(reasonDepositId);
        dest.writeString(deposit);
        dest.writeString(ipStatic);
        dest.writeLong(quotaOver);
        dest.writeString(userUsing);
        dest.writeString(userTitle);
        dest.writeLong(staffId);
        dest.writeString(staffName);
        dest.writeString(password);
        dest.writeString(changeDatetime);
        dest.writeString(offerName);
        dest.writeLong(custId);
        dest.writeString(custName);
        dest.writeString(limitDate);
        dest.writeString(province);
        dest.writeString(district);
        dest.writeString(precinct);
        dest.writeString(streetBlock);
        dest.writeLong(teamId);
        dest.writeString(vip);
        dest.writeString(debit);
        dest.writeString(deployAreaCode);
        dest.writeString(deployAreaCodeEnd);
        dest.writeString(imsi);
        dest.writeString(serviceType);
        dest.writeString(account);
        dest.writeString(data);
        dest.writeString(serial);
        dest.writeLong(isInfoCompleted);
        dest.writeString(addressCode);
        dest.writeString(passwordATM);
        dest.writeString(birthDate);
        dest.writeString(nationality);
        dest.writeString(sex);
        dest.writeString(street);
        dest.writeString(streetName);
        dest.writeString(streetBlockName);
        dest.writeString(home);
        dest.writeLong(telecomServiceID);
        dest.writeLong(paymentOrder);
        dest.writeString(locationName);
        dest.writeString(locationCode);
        dest.writeLong(chanelTypeId);
        dest.writeString(adslIsdn);
        dest.writeString(firstConnect);
        dest.writeString(deployAddressEnd);
        dest.writeString(deployAddress);
        dest.writeLong(recordWorkId);
        dest.writeString(type);
        dest.writeLong(gponGroupAccountId);
        dest.writeString(serviceName);
        dest.writeString(contractNo);
        dest.writeString(idNo);
        dest.writeString(contact);
        dest.writeString(lineType);
        dest.writeLong(pricePlan);
        dest.writeLong(localPricePlan);
        dest.writeString(descStatus);
        dest.writeString(descActStatus);
        dest.writeString(telFax);
        dest.writeString(telMobile);
        dest.writeString(technology);
        dest.writeString(vlan);
        dest.writeLong(isUplink);
        dest.writeLong(numOfComputer);
        dest.writeString(adslAccount);
        dest.writeString(ipLan);
        dest.writeString(ipWan);
        dest.writeString(ipGateway);
        dest.writeString(ipRouter);
        dest.writeString(email);
        dest.writeLong(speed);
        dest.writeLong(localSpeed);
        dest.writeLong(sourceId);
        dest.writeLong(parentSubId);
        dest.writeLong(upLink);
        dest.writeLong(groupAccountId);
        dest.writeLong(numResetZone);
        dest.writeString(isdn);
        dest.writeLong(dslamId);
        dest.writeString(prepaidValue);
        dest.writeLong(prepaidBilling);
        dest.writeString(payType);
        dest.writeString(portNo);
        dest.writeLong(userID);
        dest.writeParcelable(customer, flags);
        dest.writeTypedList(listSubStockModelRel);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Subscriber> CREATOR = new Creator<Subscriber>() {
        @Override
        public Subscriber createFromParcel(Parcel in) {
            return new Subscriber(in);
        }

        @Override
        public Subscriber[] newArray(int size) {
            return new Subscriber[size];
        }
    };

    public long getSubId() {
        return subId;
    }

    public void setSubId(long subId) {
        this.subId = subId;
    }

    public long getCustReqId() {
        return custReqId;
    }

    public void setCustReqId(long custReqId) {
        this.custReqId = custReqId;
    }

    public long getIsSatisfy() {
        return isSatisfy;
    }

    public void setIsSatisfy(long isSatisfy) {
        this.isSatisfy = isSatisfy;
    }

    public long getContractId() {
        return contractId;
    }

    public void setContractId(long contractId) {
        this.contractId = contractId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getActStatus() {
        return actStatus;
    }

    public void setActStatus(String actStatus) {
        this.actStatus = actStatus;
    }

    public String getStaDatetime() {
        return staDatetime;
    }

    public void setStaDatetime(String staDatetime) {
        this.staDatetime = staDatetime;
    }

    public String getEndDatetime() {
        return endDatetime;
    }

    public void setEndDatetime(String endDatetime) {
        this.endDatetime = endDatetime;
    }

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public long getFinishReasonId() {
        return finishReasonId;
    }

    public void setFinishReasonId(long finishReasonId) {
        this.finishReasonId = finishReasonId;
    }

    public long getRegReasonId() {
        return regReasonId;
    }

    public void setRegReasonId(long regReasonId) {
        this.regReasonId = regReasonId;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public long getQuota() {
        return quota;
    }

    public void setQuota(long quota) {
        this.quota = quota;
    }

    public String getPromotionCode() {
        return promotionCode;
    }

    public void setPromotionCode(String promotionCode) {
        this.promotionCode = promotionCode;
    }

    public String getRegType() {
        return regType;
    }

    public void setRegType(String regType) {
        this.regType = regType;
    }

    public long getReasonDepositId() {
        return reasonDepositId;
    }

    public void setReasonDepositId(long reasonDepositId) {
        this.reasonDepositId = reasonDepositId;
    }

    public String getDeposit() {
        return deposit;
    }

    public void setDeposit(String deposit) {
        this.deposit = deposit;
    }

    public String getIpStatic() {
        return ipStatic;
    }

    public void setIpStatic(String ipStatic) {
        this.ipStatic = ipStatic;
    }

    public long getQuotaOver() {
        return quotaOver;
    }

    public void setQuotaOver(long quotaOver) {
        this.quotaOver = quotaOver;
    }

    public String getUserUsing() {
        return userUsing;
    }

    public void setUserUsing(String userUsing) {
        this.userUsing = userUsing;
    }

    public String getUserTitle() {
        return userTitle;
    }

    public void setUserTitle(String userTitle) {
        this.userTitle = userTitle;
    }

    public long getStaffId() {
        return staffId;
    }

    public void setStaffId(long staffId) {
        this.staffId = staffId;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getChangeDatetime() {
        return changeDatetime;
    }

    public void setChangeDatetime(String changeDatetime) {
        this.changeDatetime = changeDatetime;
    }

    public String getOfferName() {
        return offerName;
    }

    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }

    public long getCustId() {
        return custId;
    }

    public void setCustId(long custId) {
        this.custId = custId;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getLimitDate() {
        return limitDate;
    }

    public void setLimitDate(String limitDate) {
        this.limitDate = limitDate;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getPrecinct() {
        return precinct;
    }

    public void setPrecinct(String precinct) {
        this.precinct = precinct;
    }

    public String getStreetBlock() {
        return streetBlock;
    }

    public void setStreetBlock(String streetBlock) {
        this.streetBlock = streetBlock;
    }

    public long getTeamId() {
        return teamId;
    }

    public void setTeamId(long teamId) {
        this.teamId = teamId;
    }

    public String getVip() {
        return vip;
    }

    public void setVip(String vip) {
        this.vip = vip;
    }

    public String getDebit() {
        return debit;
    }

    public void setDebit(String debit) {
        this.debit = debit;
    }

    public String getDeployAreaCode() {
        return deployAreaCode;
    }

    public void setDeployAreaCode(String deployAreaCode) {
        this.deployAreaCode = deployAreaCode;
    }

    public String getDeployAreaCodeEnd() {
        return deployAreaCodeEnd;
    }

    public void setDeployAreaCodeEnd(String deployAreaCodeEnd) {
        this.deployAreaCodeEnd = deployAreaCodeEnd;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public long getIsInfoCompleted() {
        return isInfoCompleted;
    }

    public void setIsInfoCompleted(long isInfoCompleted) {
        this.isInfoCompleted = isInfoCompleted;
    }

    public String getAddressCode() {
        return addressCode;
    }

    public void setAddressCode(String addressCode) {
        this.addressCode = addressCode;
    }

    public String getPasswordATM() {
        return passwordATM;
    }

    public void setPasswordATM(String passwordATM) {
        this.passwordATM = passwordATM;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getStreetBlockName() {
        return streetBlockName;
    }

    public void setStreetBlockName(String streetBlockName) {
        this.streetBlockName = streetBlockName;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public long getTelecomServiceID() {
        return telecomServiceID;
    }

    public void setTelecomServiceID(long telecomServiceID) {
        this.telecomServiceID = telecomServiceID;
    }

    public long getPaymentOrder() {
        return paymentOrder;
    }

    public void setPaymentOrder(long paymentOrder) {
        this.paymentOrder = paymentOrder;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public long getChanelTypeId() {
        return chanelTypeId;
    }

    public void setChanelTypeId(long chanelTypeId) {
        this.chanelTypeId = chanelTypeId;
    }

    public String getAdslIsdn() {
        return adslIsdn;
    }

    public void setAdslIsdn(String adslIsdn) {
        this.adslIsdn = adslIsdn;
    }

    public String getFirstConnect() {
        return firstConnect;
    }

    public void setFirstConnect(String firstConnect) {
        this.firstConnect = firstConnect;
    }

    public String getDeployAddressEnd() {
        return deployAddressEnd;
    }

    public void setDeployAddressEnd(String deployAddressEnd) {
        this.deployAddressEnd = deployAddressEnd;
    }

    public String getDeployAddress() {
        return deployAddress;
    }

    public void setDeployAddress(String deployAddress) {
        this.deployAddress = deployAddress;
    }

    public long getRecordWorkId() {
        return recordWorkId;
    }

    public void setRecordWorkId(long recordWorkId) {
        this.recordWorkId = recordWorkId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getGponGroupAccountId() {
        return gponGroupAccountId;
    }

    public void setGponGroupAccountId(long gponGroupAccountId) {
        this.gponGroupAccountId = gponGroupAccountId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getLineType() {
        return lineType;
    }

    public void setLineType(String lineType) {
        this.lineType = lineType;
    }

    public long getPricePlan() {
        return pricePlan;
    }

    public void setPricePlan(long pricePlan) {
        this.pricePlan = pricePlan;
    }

    public long getLocalPricePlan() {
        return localPricePlan;
    }

    public void setLocalPricePlan(long localPricePlan) {
        this.localPricePlan = localPricePlan;
    }

    public String getDescStatus() {
        return descStatus;
    }

    public void setDescStatus(String descStatus) {
        this.descStatus = descStatus;
    }

    public String getDescActStatus() {
        return descActStatus;
    }

    public void setDescActStatus(String descActStatus) {
        this.descActStatus = descActStatus;
    }

    public String getTelFax() {
        return telFax;
    }

    public void setTelFax(String telFax) {
        this.telFax = telFax;
    }

    public String getTelMobile() {
        return telMobile;
    }

    public void setTelMobile(String telMobile) {
        this.telMobile = telMobile;
    }

    public String getTechnology() {
        return technology;
    }

    public void setTechnology(String technology) {
        this.technology = technology;
    }

    public String getVlan() {
        return vlan;
    }

    public void setVlan(String vlan) {
        this.vlan = vlan;
    }

    public long getIsUplink() {
        return isUplink;
    }

    public void setIsUplink(long isUplink) {
        this.isUplink = isUplink;
    }

    public long getNumOfComputer() {
        return numOfComputer;
    }

    public void setNumOfComputer(long numOfComputer) {
        this.numOfComputer = numOfComputer;
    }

    public String getAdslAccount() {
        return adslAccount;
    }

    public void setAdslAccount(String adslAccount) {
        this.adslAccount = adslAccount;
    }

    public String getIpLan() {
        return ipLan;
    }

    public void setIpLan(String ipLan) {
        this.ipLan = ipLan;
    }

    public String getIpWan() {
        return ipWan;
    }

    public void setIpWan(String ipWan) {
        this.ipWan = ipWan;
    }

    public String getIpGateway() {
        return ipGateway;
    }

    public void setIpGateway(String ipGateway) {
        this.ipGateway = ipGateway;
    }

    public String getIpRouter() {
        return ipRouter;
    }

    public void setIpRouter(String ipRouter) {
        this.ipRouter = ipRouter;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getSpeed() {
        return speed;
    }

    public void setSpeed(long speed) {
        this.speed = speed;
    }

    public long getLocalSpeed() {
        return localSpeed;
    }

    public void setLocalSpeed(long localSpeed) {
        this.localSpeed = localSpeed;
    }

    public long getSourceId() {
        return sourceId;
    }

    public void setSourceId(long sourceId) {
        this.sourceId = sourceId;
    }

    public long getParentSubId() {
        return parentSubId;
    }

    public void setParentSubId(long parentSubId) {
        this.parentSubId = parentSubId;
    }

    public long getUpLink() {
        return upLink;
    }

    public void setUpLink(long upLink) {
        this.upLink = upLink;
    }

    public long getGroupAccountId() {
        return groupAccountId;
    }

    public void setGroupAccountId(long groupAccountId) {
        this.groupAccountId = groupAccountId;
    }

    public long getNumResetZone() {
        return numResetZone;
    }

    public void setNumResetZone(long numResetZone) {
        this.numResetZone = numResetZone;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public long getDslamId() {
        return dslamId;
    }

    public void setDslamId(long dslamId) {
        this.dslamId = dslamId;
    }

    public String getPrepaidValue() {
        return prepaidValue;
    }

    public void setPrepaidValue(String prepaidValue) {
        this.prepaidValue = prepaidValue;
    }

    public long getPrepaidBilling() {
        return prepaidBilling;
    }

    public void setPrepaidBilling(long prepaidBilling) {
        this.prepaidBilling = prepaidBilling;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getPortNo() {
        return portNo;
    }

    public void setPortNo(String portNo) {
        this.portNo = portNo;
    }

    public long getUserID() {
        return userID;
    }

    public void setUserID(long userID) {
        this.userID = userID;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<SubStockModelRel> getListSubStockModelRel() {
        return listSubStockModelRel;
    }

    public void setListSubStockModelRel(List<SubStockModelRel> listSubStockModelRel) {
        this.listSubStockModelRel = listSubStockModelRel;
    }
}
