package com.viettel.mbccs.data.source.remote.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.viettel.mbccs.data.model.Customer;
import com.viettel.mbccs.data.model.StockSerial;
import java.util.List;

/**
 * Created by eo_cuong on 5/28/17.
 */

public class CreateSaleTransChannelRequest extends BaseRequest {

    @SerializedName("shopId")
    @Expose
    private Long shopId;

    @SerializedName("staffId")
    @Expose
    private Long staffId;

    @SerializedName("lstSerialSale")
    @Expose
    private List<StockSerial> lstSerialSale;

    @SerializedName("customer")
    @Expose
    private Customer mCustomer;

    @SerializedName("payMethod")
    @Expose
    private String paymentMethod;

    @SerializedName("isdnPay")
    @Expose
    private String IsdnPay;

    @SerializedName("telecomserviceId")
    @Expose
    private Long telecomserviceId;

    @SerializedName("saleProgrameCode")
    @Expose
    private String saleProgrameCode;

    @SerializedName("saleTransType")
    @Expose
    private String saleTransType;

    @SerializedName("channelId")
    @Expose
    private Long chanelId;

    @SerializedName("channelType")
    @Expose
    private Long channelType;

    @SerializedName("priceType")
    @Expose
    private String priceType;

    @SerializedName("pricePolicy")
    @Expose
    private String pricePolicy;

    @SerializedName("discountPolicy")
    @Expose
    private String discountPolicy;

    @SerializedName("couponCode")
    @Expose
    private String couponCode;

    @SerializedName("orderId")
    @Expose
    private Long orderId;

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public List<StockSerial> getLstSerialSale() {
        return lstSerialSale;
    }

    public void setLstSerialSale(List<StockSerial> lstSerialSale) {
        this.lstSerialSale = lstSerialSale;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getIsdnPay() {
        return IsdnPay;
    }

    public void setIsdnPay(String isdnPay) {
        IsdnPay = isdnPay;
    }

    public Long getTelecomserviceId() {
        return telecomserviceId;
    }

    public void setTelecomserviceId(Long telecomserviceId) {
        this.telecomserviceId = telecomserviceId;
    }

    public String getSaleProgrameCode() {
        return saleProgrameCode;
    }

    public void setSaleProgrameCode(String saleProgrameCode) {
        this.saleProgrameCode = saleProgrameCode;
    }

    public String getSaleTransType() {
        return saleTransType;
    }

    public void setSaleTransType(String saleTransType) {
        this.saleTransType = saleTransType;
    }

    public Long getChanelId() {
        return chanelId;
    }

    public void setChanelId(Long chanelId) {
        this.chanelId = chanelId;
    }

    public Long getChannelType() {
        return channelType;
    }

    public void setChannelType(Long channelType) {
        this.channelType = channelType;
    }

    public String getPriceType() {
        return priceType;
    }

    public void setPriceType(String priceType) {
        this.priceType = priceType;
    }

    public String getPricePolicy() {
        return pricePolicy;
    }

    public void setPricePolicy(String pricePolicy) {
        this.pricePolicy = pricePolicy;
    }

    public String getDiscountPolicy() {
        return discountPolicy;
    }

    public void setDiscountPolicy(String discountPolicy) {
        this.discountPolicy = discountPolicy;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public Customer getCustomer() {
        return mCustomer;
    }

    public void setCustomer(Customer customer) {
        mCustomer = customer;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public CreateSaleTransChannelRequest() {
        super();
    }
}
