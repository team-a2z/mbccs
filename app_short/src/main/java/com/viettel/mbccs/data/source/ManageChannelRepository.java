package com.viettel.mbccs.data.source;

import com.viettel.mbccs.data.source.local.IManageChannelLocalDataSource;
import com.viettel.mbccs.data.source.local.datasource.ManageChannelLocalDataSource;
import com.viettel.mbccs.data.source.remote.IManageChannelRemoteDataSource;
import com.viettel.mbccs.data.source.remote.datasource.ManageChannelRemoteDataSource;
import com.viettel.mbccs.data.source.remote.request.DataRequest;
import com.viettel.mbccs.data.source.remote.request.GetChannelByLocationRequest;
import com.viettel.mbccs.data.source.remote.request.GetChannelDetailRequest;
import com.viettel.mbccs.data.source.remote.request.UpdateChannelInfoRequest;
import com.viettel.mbccs.data.source.remote.response.GetChannelByLocationResponse;
import com.viettel.mbccs.data.source.remote.response.GetChannelDetailResponse;
import com.viettel.mbccs.data.source.remote.response.UpdateChannelInfoResponse;

import rx.Observable;

/**
 * Created by eo_cuong on 5/10/17.
 */

public class ManageChannelRepository implements IManageChannelLocalDataSource, IManageChannelRemoteDataSource {

    private volatile static ManageChannelRepository instance;
    private IManageChannelLocalDataSource localDataSource;
    private IManageChannelRemoteDataSource remoteDataSource;

    public ManageChannelRepository(IManageChannelLocalDataSource localDataSource,
                                   IManageChannelRemoteDataSource remoteDataSource) {
        this.localDataSource = localDataSource;
        this.remoteDataSource = remoteDataSource;
    }

    public static ManageChannelRepository getInstance() {
        if (instance == null) {
            instance = new ManageChannelRepository(ManageChannelLocalDataSource.getInstance(),
                    ManageChannelRemoteDataSource.getInstance());
        }
        return instance;
    }

    @Override
    public Observable<GetChannelByLocationResponse> getChannelByLocation(DataRequest<GetChannelByLocationRequest> request) {
        return remoteDataSource.getChannelByLocation(request);
    }

    @Override
    public Observable<GetChannelDetailResponse> getChannelDetail(DataRequest<GetChannelDetailRequest> request) {
        return remoteDataSource.getChannelDetail(request);
    }

    @Override
    public Observable<UpdateChannelInfoResponse> updateChannelInfo(DataRequest<UpdateChannelInfoRequest> request) {
        return remoteDataSource.updateChannelInfo(request);
    }
}
