package com.viettel.mbccs.data.model.database;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.IntDef;
import android.support.annotation.StringDef;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.viettel.mbccs.constance.UploadImageField;

/**
 * Created by HuyQuyet on 6/8/17.
 */

@Table(name = "UploadImage")
public class UploadImage extends Model implements Parcelable {

    @IntDef({ImageType.FRONT, ImageType.BACKSIDE, ImageType.PORTRAIT})
    public @interface ImageType {
        int FRONT = 1;
        int BACKSIDE = 2;
        int PORTRAIT = 3;
    }

    @StringDef({StatusUpload.WAITING, StatusUpload.IN_PROGRESS, StatusUpload.SUCCESS, StatusUpload.ERROR})
    public @interface StatusUpload {
        String WAITING = "Waiting";
        String IN_PROGRESS = "Inprogress";
        String SUCCESS = "Success";
        String ERROR = "Error";
    }


    @Column(name = UploadImageField.IMAGE_NAME, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private String imageName;

    @Column(name = UploadImageField.CONTENT)
    private String content;

    @Column(name = UploadImageField.IMAGE_TYPE)
    private String imageType;

    @Column(name = UploadImageField.CUST_ID)
    private String custId;

    @StatusUpload
    @Column(name = UploadImageField.STATUS)
    private String status;

    public UploadImage() {
        super();
    }


    protected UploadImage(Parcel in) {
        imageName = in.readString();
        content = in.readString();
        imageType = in.readString();
        custId = in.readString();
        status = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(imageName);
        dest.writeString(content);
        dest.writeString(imageType);
        dest.writeString(custId);
        dest.writeString(status);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UploadImage> CREATOR = new Creator<UploadImage>() {
        @Override
        public UploadImage createFromParcel(Parcel in) {
            return new UploadImage(in);
        }

        @Override
        public UploadImage[] newArray(int size) {
            return new UploadImage[size];
        }
    };

    public String getImageName() {
        return imageName;
    }

    public void setIdImage(String imageName) {
        this.imageName = imageName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(@StatusUpload String status) {
        this.status = status;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }
}
