package com.viettel.mbccs.data.source.remote.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by HuyQuyet on 6/8/17.
 */

public class UploadImageRequest extends BaseRequest {

    @SerializedName("lstUploadImageTran")
    private List<ImageRequest> lstUploadImageTran;

    @Expose
    private String channelType;

    public List<ImageRequest> getLstUploadImageTran() {
        return lstUploadImageTran;
    }

    public void setLstUploadImageTran(List<ImageRequest> lstUploadImageTran) {
        this.lstUploadImageTran = lstUploadImageTran;
    }

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public UploadImageRequest() {
        super();
    }

    public static class ImageRequest {
        String fileName;
        String content;
        String custId;
        String imageType;

        public String getFileName() {
            return fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getCustId() {
            return custId;
        }

        public void setCustId(String custId) {
            this.custId = custId;
        }

        public String getImageType() {
            return imageType;
        }

        public void setImageType(String imageType) {
            this.imageType = imageType;
        }
    }
}
