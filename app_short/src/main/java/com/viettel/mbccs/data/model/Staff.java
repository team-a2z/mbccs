package com.viettel.mbccs.data.model;

import com.google.gson.annotations.Expose;

/**
 * Created by minhnx on 7/20/17.
 */

public class Staff {
    @Expose
    private String name;
    @Expose
    private String isdn;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }
}
