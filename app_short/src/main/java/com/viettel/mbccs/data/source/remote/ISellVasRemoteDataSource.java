package com.viettel.mbccs.data.source.remote;

import com.viettel.mbccs.data.source.remote.request.DataRequest;
import com.viettel.mbccs.data.source.remote.request.GetReasonRequest;
import com.viettel.mbccs.data.source.remote.request.GetVasForIsdnRequest;
import com.viettel.mbccs.data.source.remote.request.GetVasInfoRequest;
import com.viettel.mbccs.data.source.remote.request.RegisterVasRequest;
import com.viettel.mbccs.data.source.remote.response.GetReasonResponse;
import com.viettel.mbccs.data.source.remote.response.GetVasForIsdnResponse;
import com.viettel.mbccs.data.source.remote.response.GetVasInfoResponse;
import com.viettel.mbccs.data.source.remote.response.RegisterVasResponse;

import rx.Observable;

/**
 * Created by eo_cuong on 5/11/17.
 */

public interface ISellVasRemoteDataSource {
    Observable<GetVasForIsdnResponse> getVasForIsdn(DataRequest<GetVasForIsdnRequest> request);

    Observable<GetVasInfoResponse> getVasInfo(DataRequest<GetVasInfoRequest> request);

    Observable<RegisterVasResponse> registerVas(DataRequest<RegisterVasRequest> request);

    Observable<GetReasonResponse> getReasons(DataRequest<GetReasonRequest> request);
}
