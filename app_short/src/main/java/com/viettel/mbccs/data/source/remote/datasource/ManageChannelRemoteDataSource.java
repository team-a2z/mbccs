package com.viettel.mbccs.data.source.remote.datasource;

import com.viettel.mbccs.data.source.remote.IManageChannelRemoteDataSource;
import com.viettel.mbccs.data.source.remote.request.DataRequest;
import com.viettel.mbccs.data.source.remote.request.GetChannelByLocationRequest;
import com.viettel.mbccs.data.source.remote.request.GetChannelDetailRequest;
import com.viettel.mbccs.data.source.remote.request.UpdateChannelInfoRequest;
import com.viettel.mbccs.data.source.remote.response.GetChannelByLocationResponse;
import com.viettel.mbccs.data.source.remote.response.GetChannelDetailResponse;
import com.viettel.mbccs.data.source.remote.response.UpdateChannelInfoResponse;
import com.viettel.mbccs.data.source.remote.service.RequestHelper;
import com.viettel.mbccs.utils.rx.SchedulerUtils;

import rx.Observable;

/**
 * Created by eo_cuong on 5/10/17.
 */

public class ManageChannelRemoteDataSource implements IManageChannelRemoteDataSource {

    public volatile static ManageChannelRemoteDataSource instance;

    public ManageChannelRemoteDataSource() {

    }

    public static ManageChannelRemoteDataSource getInstance() {
        if (instance == null) {
            instance = new ManageChannelRemoteDataSource();
        }
        return instance;
    }

    @Override
    public Observable<GetChannelByLocationResponse> getChannelByLocation(DataRequest<GetChannelByLocationRequest> request) {
        return RequestHelper.getRequest()
                .getChannelByLocation(request)
                .flatMap(SchedulerUtils.<GetChannelByLocationResponse>convertDataFlatMap())
                .compose(SchedulerUtils.<GetChannelByLocationResponse>applyAsyncSchedulers());
    }

    @Override
    public Observable<GetChannelDetailResponse> getChannelDetail(DataRequest<GetChannelDetailRequest> request) {
        return RequestHelper.getRequest()
                .getChannelDetail(request)
                .flatMap(SchedulerUtils.<GetChannelDetailResponse>convertDataFlatMap())
                .compose(SchedulerUtils.<GetChannelDetailResponse>applyAsyncSchedulers());
    }

    @Override
    public Observable<UpdateChannelInfoResponse> updateChannelInfo(DataRequest<UpdateChannelInfoRequest> request) {
        return RequestHelper.getRequest()
                .updateChannelInfo(request)
                .flatMap(SchedulerUtils.<UpdateChannelInfoResponse>convertDataFlatMap())
                .compose(SchedulerUtils.<UpdateChannelInfoResponse>applyAsyncSchedulers());
    }
}
