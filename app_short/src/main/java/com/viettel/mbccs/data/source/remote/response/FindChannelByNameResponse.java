package com.viettel.mbccs.data.source.remote.response;

import com.google.gson.annotations.Expose;
import com.viettel.mbccs.data.model.Staff;

import java.util.List;

/**
 * Created by minhnx on 6/7/17.
 */

public class FindChannelByNameResponse {
    @Expose
    private List<Staff> listStaff;

    public List<Staff> getListStaff() {
        return listStaff;
    }

    public void setListStaff(List<Staff> listStaff) {
        this.listStaff = listStaff;
    }
}
