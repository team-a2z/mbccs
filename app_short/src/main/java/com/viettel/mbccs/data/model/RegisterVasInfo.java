package com.viettel.mbccs.data.model;

import com.google.gson.annotations.Expose;

/**
 * Created by minhnx on 8/10/17.
 */

public class RegisterVasInfo {
    @Expose
    private String vasCode;
    @Expose
    private boolean used;

    public String getVasCode() {
        return vasCode;
    }

    public void setVasCode(String vasCode) {
        this.vasCode = vasCode;
    }

    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    public RegisterVasInfo(String vasCode, boolean used) {
        this.vasCode = vasCode;
        this.used = used;
    }

    public RegisterVasInfo() {
    }
}