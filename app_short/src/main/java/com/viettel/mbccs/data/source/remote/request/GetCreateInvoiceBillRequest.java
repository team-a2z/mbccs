package com.viettel.mbccs.data.source.remote.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jacky on 7/4/17.
 */

public class GetCreateInvoiceBillRequest extends BaseRequest {
    @Expose
    @SerializedName("shopId")
    public int mShopId;

    @Expose
    @SerializedName("staffId")
    public long mStaffId;

    @Expose
    @SerializedName("lstSaleTransId")
    public List<Long> mListSaleTrans;

    public List<Long> getListSaleTrans() {
        return mListSaleTrans;
    }

    public void setListSaleTrans(List<Long> listSaleTrans) {
        mListSaleTrans = listSaleTrans;
    }
}
