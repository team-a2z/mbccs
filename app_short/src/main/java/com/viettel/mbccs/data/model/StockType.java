package com.viettel.mbccs.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by HuyQuyet on 7/20/17.
 */

public class StockType {

    @SerializedName("bankAccountGroupId")
    @Expose
    private long bankAccountGroupId;

    @SerializedName("checkExp")
    @Expose
    private long checkExp;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("notes")
    @Expose
    private String notes;

    @SerializedName("status")
    @Expose
    private long status;

    @SerializedName("stockTypeId")
    @Expose
    private long stockTypeId;

    public long getBankAccountGroupId() {
        return bankAccountGroupId;
    }

    public void setBankAccountGroupId(long bankAccountGroupId) {
        this.bankAccountGroupId = bankAccountGroupId;
    }

    public long getCheckExp() {
        return checkExp;
    }

    public void setCheckExp(long checkExp) {
        this.checkExp = checkExp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    public long getStockTypeId() {
        return stockTypeId;
    }

    public void setStockTypeId(long stockTypeId) {
        this.stockTypeId = stockTypeId;
    }

    @Override
    public String toString() {
        return getName();
    }
}
