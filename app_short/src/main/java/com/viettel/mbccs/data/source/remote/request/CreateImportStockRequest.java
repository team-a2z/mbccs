package com.viettel.mbccs.data.source.remote.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FRAMGIA\hoang.van.cuong on 04/07/2017.
 */

public class CreateImportStockRequest extends BaseRequest {

    @SerializedName("stockTransId")
    @Expose
    private Long stockTransId;
    @SerializedName("staffId")
    @Expose
    private Long staffId;
    @Expose
    private Long reasonId;

    public Long getStockTransId() {
        return stockTransId;
    }

    public void setStockTransId(Long stockTransId) {
        this.stockTransId = stockTransId;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public Long getReasonId() {
        return reasonId;
    }

    public void setReasonId(Long reasonId) {
        this.reasonId = reasonId;
    }

    public CreateImportStockRequest() {
        super();
    }
}
