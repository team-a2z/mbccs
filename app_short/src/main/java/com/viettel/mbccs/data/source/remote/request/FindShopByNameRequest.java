package com.viettel.mbccs.data.source.remote.request;

import com.google.gson.annotations.Expose;

/**
 * Created by eo_cuong on 5/19/17.
 */

public class FindShopByNameRequest extends BaseRequest {

    @Expose
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FindShopByNameRequest() {
        super();
    }
}
