package com.viettel.mbccs.data.model;

import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by HuyQuyet on 7/10/17.
 */

public class ItemReturnMoneyDetail {

    @Expose
    private String transCode;
    @Expose
    private Double amount;
    @Expose
    private String creator;
    @Expose
    private String status;
    @Expose
    private String createDate;
    @Expose
    private List<String> items;

    public String getTransCode() {
        return transCode;
    }

    public void setTransCode(String transCode) {
        this.transCode = transCode;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public List<String> getItems() {
        return items;
    }

    public void setItems(List<String> items) {
        this.items = items;
    }
}
