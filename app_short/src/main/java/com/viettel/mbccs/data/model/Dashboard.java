package com.viettel.mbccs.data.model;

import android.support.annotation.StringRes;
import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.viettel.mbccs.R;

import java.util.List;

/**
 * Created by Da Qiao on 8/18/2017.
 */

public class Dashboard {

    @SerializedName("lstValue")
    @Expose
    private List<Value> mValue = null;
    @SerializedName("functionCode")
    @Expose
    private String functionCode;
    @SerializedName("dashBoardType")
    @Expose
    private String dashBoardType;
    @SerializedName("dashBoardCode")
    @Expose
    private String dashBoardCode;
    @SerializedName("dashBoardName")
    @Expose
    private String dashBoardName;

    public List<Value> getValue() {
        return mValue;
    }

    public void setValue(List<Value> value) {
        this.mValue = value;
    }

    public String getFunctionCode() {
        return functionCode;
    }

    public void setFunctionCode(String functionCode) {
        this.functionCode = functionCode;
    }

    public String getDashBoardType() {
        return dashBoardType;
    }

    public void setDashBoardType(String dashBoardType) {
        this.dashBoardType = dashBoardType;
    }

    public String getDashBoardCode() {
        return dashBoardCode;
    }

    public void setDashBoardCode(String dashBoardCode) {
        this.dashBoardCode = dashBoardCode;
    }

    public String getDashBoardName() {
        if (TextUtils.isEmpty(dashBoardName))
            return "";
        return dashBoardName;
    }

    public void setDashBoardName(String dashBoardName) {
        this.dashBoardName = dashBoardName;
    }

    @StringRes
    public int getTitle() {
//        if (dashBoardCode != null)
//            switch (dashBoardCode) {
//                case DashboardCode.DONDH:
//                    return R.string.dash_board_don_dat_hang;
//                case DashboardCode.GDCLH:
//                    return R.string.dash_board_giao_dich_chua_lap_hoa_don;
//                case DashboardCode.GDTCH:
//                    return R.string.dash_board_giao_dich_treo_cua_hang;
//                case DashboardCode.GDTNV:
//                    return R.string.dash_board_giao_dich_treo_nhan_vien;
//                case DashboardCode.GNTCP:
//                    return R.string.dash_board_tao_giay_nop_tien_can_phe;
//                case DashboardCode.TGNTI:
//                    return R.string.dash_board_tao_giay_nop_tien;
//                case DashboardCode.TRASP:
//                    return R.string.dash_board_tra_cuu_san_pham;
//            }
        return R.string.menu_dashboard;
    }

    public static class Value {

        @SerializedName("value")
        @Expose
        private String value;
        @SerializedName("key")
        @Expose
        private String key;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }
    }

//    @StringDef({DashboardCode.GDTCH, DashboardCode.GDTNV, DashboardCode.DONDH, DashboardCode.GDCLH,
//            DashboardCode.TGNTI, DashboardCode.GNTCP, DashboardCode.TRASP
//    })
//    public @interface DashboardCode {
//        String DB_CHITIEUKD = "DB_CHITIEUKD";
//        String DB_GIAOVIEC = "DB_GIAOVIEC";
//        String DB_GDCHUALAPHOADON = "DB_GDCHUALAPHOADON";
//        String DB_TAO_GNT = "DB_TAO_GNT";
//        String DB_VIECCHUAXONG = "DB_VIECCHUAXONG";
//        String DB_KENH_FEEDBACK = "DB_KENH_FEEDBACK";
//        String DB_GDTCH = "DB_GDTCH";
//        String DB_THUCUOC = "DB_THUCUOC";
//        String DB_DONDATHANG = "DB_DONDATHANG";
//        String DB_DUYET_GNT = "DB_DUYET_GNT";
//        String DB_DS_CONGNO = "DB_DS_CONGNO";
//        String DB_XACMINH = "DB_XACMINH";
//        String DB_SURVEY = "DB_SURVEY";
//        String DB_GDTNV = "DB_GDTNV";

//        String GDTCH = "GDTCH";
//        String GDTNV = "GDTNV";
//        String DONDH = "DONDH";
//        String GDCLH = "GDCLH";
//        String TGNTI = "TGNTI";
//        String GNTCP = "GNTCP";
//        String TRASP = "TRASP";
//    }
}
