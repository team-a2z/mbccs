package com.viettel.mbccs.data.source.remote.datasource;

import com.viettel.mbccs.data.source.remote.ISellVasRemoteDataSource;
import com.viettel.mbccs.data.source.remote.request.DataRequest;
import com.viettel.mbccs.data.source.remote.request.GetReasonRequest;
import com.viettel.mbccs.data.source.remote.request.GetVasForIsdnRequest;
import com.viettel.mbccs.data.source.remote.request.GetVasInfoRequest;
import com.viettel.mbccs.data.source.remote.request.RegisterVasRequest;
import com.viettel.mbccs.data.source.remote.response.GetReasonResponse;
import com.viettel.mbccs.data.source.remote.response.GetVasForIsdnResponse;
import com.viettel.mbccs.data.source.remote.response.GetVasInfoResponse;
import com.viettel.mbccs.data.source.remote.response.RegisterVasResponse;
import com.viettel.mbccs.data.source.remote.service.RequestHelper;
import com.viettel.mbccs.utils.rx.SchedulerUtils;

import rx.Observable;

/**
 * Created by eo_cuong on 5/10/17.
 */

public class SellVasRemoteDataSource implements ISellVasRemoteDataSource {

    public volatile static SellVasRemoteDataSource instance;

    public SellVasRemoteDataSource() {

    }

    @Override
    public Observable<GetVasForIsdnResponse> getVasForIsdn(DataRequest<GetVasForIsdnRequest> request) {
        return RequestHelper.getRequest()
                .getVasForIsdn(request)
                .flatMap(SchedulerUtils.<GetVasForIsdnResponse>convertDataFlatMap())
                .compose(SchedulerUtils.<GetVasForIsdnResponse>applyAsyncSchedulers());
    }

    @Override
    public Observable<GetVasInfoResponse> getVasInfo(DataRequest<GetVasInfoRequest> request) {
        return RequestHelper.getRequest()
                .getVasInfo(request)
                .flatMap(SchedulerUtils.<GetVasInfoResponse>convertDataFlatMap())
                .compose(SchedulerUtils.<GetVasInfoResponse>applyAsyncSchedulers());
    }

    @Override
    public Observable<RegisterVasResponse> registerVas(DataRequest<RegisterVasRequest> request) {
        return RequestHelper.getRequest()
                .registerVas(request)
                .flatMap(SchedulerUtils.<RegisterVasResponse>convertDataFlatMap())
                .compose(SchedulerUtils.<RegisterVasResponse>applyAsyncSchedulers());
    }

    @Override
    public Observable<GetReasonResponse> getReasons(DataRequest<GetReasonRequest> request) {
        return RequestHelper.getRequest()
                .getReason(request)
                .flatMap(SchedulerUtils.<GetReasonResponse>convertDataFlatMap())
                .compose(SchedulerUtils.<GetReasonResponse>applyAsyncSchedulers());
    }

    public static SellVasRemoteDataSource getInstance() {
        if (instance == null) {
            instance = new SellVasRemoteDataSource();
        }
        return instance;
    }
}
