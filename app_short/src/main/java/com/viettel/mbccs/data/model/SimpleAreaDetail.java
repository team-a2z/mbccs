package com.viettel.mbccs.data.model;

import java.util.List;

/**
 * Created by minhnx on 10/25/17.
 */

public class SimpleAreaDetail {
    private String title;

    private List<KeyValue> details;

    public List<KeyValue> getDetails() {
        return details;
    }

    public void setDetails(List<KeyValue> details) {
        this.details = details;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
