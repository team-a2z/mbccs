package com.viettel.mbccs.data.source.remote.request;


import com.google.gson.annotations.Expose;

public class GetListShopsByAreaCodeRequest extends BaseRequest {

    @Expose
    private String areaCode;

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public GetListShopsByAreaCodeRequest() {
        super();
    }
}
