package com.viettel.mbccs.data.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by minhnx on 10/12/17.
 */

public class FestivalInfo implements Serializable {
    @Expose
    private Long id;
    @Expose
    private String areaCode;
    @Expose
    private String festivalCode;
    @Expose
    private String name;
    @Expose
    private Date fromIssueDate;
    @Expose
    private Date toIssueDate;
    @Expose
    private String scope;
    @Expose
    private String note;
    @Expose
    private String latitude;
    @Expose
    private String longitude;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getFestivalCode() {
        return festivalCode;
    }

    public void setFestivalCode(String festivalCode) {
        this.festivalCode = festivalCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getFromIssueDate() {
        return fromIssueDate;
    }

    public void setFromIssueDate(Date fromIssueDate) {
        this.fromIssueDate = fromIssueDate;
    }

    public Date getToIssueDate() {
        return toIssueDate;
    }

    public void setToIssueDate(Date toIssueDate) {
        this.toIssueDate = toIssueDate;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
