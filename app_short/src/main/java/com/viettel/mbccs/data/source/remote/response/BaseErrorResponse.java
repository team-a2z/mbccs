package com.viettel.mbccs.data.source.remote.response;

import java.util.ArrayList;
import java.util.List;

public class BaseErrorResponse {

    List<Error> errors;

    public String getErrorCode() {
        return errors.get(0).code;
    }

    public String getErrorMessage() {
        return errors.get(0).message;
    }

    public void setError(String code, String message) {
        errors=new ArrayList<>();
        Error error = new Error();
        error.code = code;
        error.message = message;
        errors.add(error);
    }

    public boolean isError() {
        return errors != null;
    }

    private static final class Error {

        String code;

        String message;
    }
}
