package com.viettel.mbccs.data.source.remote.response;

import com.google.gson.annotations.Expose;
import com.viettel.mbccs.data.model.ChannelType;

import java.util.List;

/**
 * Created by minhnx on 5/29/17.
 */

public class GetListChannelTypeResponse extends DataResponse {
    @Expose
    private List<com.viettel.mbccs.data.model.ChannelType> lstChannelType;

    public List<ChannelType> getLstChannelType() {
        return lstChannelType;
    }

    public void setLstChannelType(List<ChannelType> lstChannelType) {
        this.lstChannelType = lstChannelType;
    }
}
