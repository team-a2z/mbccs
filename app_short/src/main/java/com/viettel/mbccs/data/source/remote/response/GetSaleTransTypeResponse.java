package com.viettel.mbccs.data.source.remote.response;

import com.google.gson.annotations.SerializedName;
import com.viettel.mbccs.data.model.SaleTransType;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by FRAMGIA\bui.dinh.viet on 11/09/2017.
 */

public class GetSaleTransTypeResponse extends DataResponse {
    @SerializedName("lstSaleTransType")
    private List<SaleTransType> mSaleTransTypes;
    private List<String> mCodeList;

    public List<SaleTransType> getSaleTransTypes() {
        return mSaleTransTypes;
    }

    public void setSaleTransTypes(List<SaleTransType> saleTransTypes) {
        mSaleTransTypes = saleTransTypes;
    }

    public List<String> getCodeList() {
        if (mCodeList == null) {
            mCodeList = new ArrayList<>();
            for (SaleTransType saleTransType : mSaleTransTypes) {
                mCodeList.add(saleTransType.description);
            }
        }
        return mCodeList;
    }
}
