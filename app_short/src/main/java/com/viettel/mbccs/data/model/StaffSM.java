package com.viettel.mbccs.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


/**
 * Created by minhnx on 7/28/17.
 */

public class StaffSM {
    @Expose
    private Long staffId;
    @Expose
    private String staffCode;
    @Expose
    private String name;
    @Expose
    private Short status;
    @Expose
    private String birthday;
    @Expose
    private String idNo;
    @Expose
    private String idIssuePlace;
    @Expose
    private String idIssueDate;
    @Expose
    private String tel;
    @Expose
    private Long type;
    @Expose
    private String serial;
    @Expose
    private String isdn;
    @Expose
    private String pin;
    @Expose
    private String address;
    @Expose
    private String province;
    @Expose
    private String staffOwnType;
    @Expose
    private Long staffOwnerId;
    @Expose
    private String staffOwnerCode;
    @Expose
    private Long channelTypeId;
    @Expose
    private String pricePolicy;
    @Expose
    private String discountPolicy;
    @Expose
    private String email;
    @Expose
    private Short agentType;
    @Expose
    private Long shopId;
    @Expose
    private String ShopCode;
    @Expose
    private String ShopName;
    @Expose
    private Long lockStatus;
    @Expose
    private String registryDate;
    @Expose
    private String longitude;
    @Expose
    private String latitude;
    @Expose
    private Long btsId;
    @Expose
    private String btsName;
    @Expose
    private String channelTypeName;
    @Expose
    private String idTypeName;
    @Expose
    private String staffName;
    @Expose
    private Long idType;

    @SerializedName("lstImageNames")
    @Expose
    private List<ImageSelect> imageSelectList;

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public String getStaffCode() {
        return staffCode;
    }

    public void setStaffCode(String staffCode) {
        this.staffCode = staffCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getIdIssuePlace() {
        return idIssuePlace;
    }

    public void setIdIssuePlace(String idIssuePlace) {
        this.idIssuePlace = idIssuePlace;
    }

    public String getIdIssueDate() {
        return idIssueDate;
    }

    public void setIdIssueDate(String idIssueDate) {
        this.idIssueDate = idIssueDate;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getStaffOwnType() {
        return staffOwnType;
    }

    public void setStaffOwnType(String staffOwnType) {
        this.staffOwnType = staffOwnType;
    }

    public Long getStaffOwnerId() {
        return staffOwnerId;
    }

    public void setStaffOwnerId(Long staffOwnerId) {
        this.staffOwnerId = staffOwnerId;
    }

    public Long getChannelTypeId() {
        return channelTypeId;
    }

    public void setChannelTypeId(Long channelTypeId) {
        this.channelTypeId = channelTypeId;
    }

    public String getPricePolicy() {
        return pricePolicy;
    }

    public void setPricePolicy(String pricePolicy) {
        this.pricePolicy = pricePolicy;
    }

    public String getDiscountPolicy() {
        return discountPolicy;
    }

    public void setDiscountPolicy(String discountPolicy) {
        this.discountPolicy = discountPolicy;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Short getAgentType() {
        return agentType;
    }

    public void setAgentType(Short agentType) {
        this.agentType = agentType;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public String getShopCode() {
        return ShopCode;
    }

    public void setShopCode(String shopCode) {
        ShopCode = shopCode;
    }

    public String getShopName() {
        return ShopName;
    }

    public void setShopName(String shopName) {
        ShopName = shopName;
    }

    public Long getLockStatus() {
        return lockStatus;
    }

    public void setLockStatus(Long lockStatus) {
        this.lockStatus = lockStatus;
    }

    public String getRegistryDate() {
        return registryDate;
    }

    public void setRegistryDate(String registryDate) {
        this.registryDate = registryDate;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public Long getBtsId() {
        return btsId;
    }

    public void setBtsId(Long btsId) {
        this.btsId = btsId;
    }

    public String getBtsName() {
        return btsName;
    }

    public void setBtsName(String btsName) {
        this.btsName = btsName;
    }

    public String getChannelTypeName() {
        return channelTypeName;
    }

    public void setChannelTypeName(String channelTypeName) {
        this.channelTypeName = channelTypeName;
    }

    public String getIdTypeName() {
        return idTypeName;
    }

    public void setIdTypeName(String idTypeName) {
        this.idTypeName = idTypeName;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public Long getIdType() {
        return idType;
    }

    public void setIdType(Long idType) {
        this.idType = idType;
    }

    public String getStaffOwnerCode() {
        return staffOwnerCode;
    }

    public void setStaffOwnerCode(String staffOwnerCode) {
        this.staffOwnerCode = staffOwnerCode;
    }

    public List<ImageSelect> getImageSelectList() {
        return imageSelectList;
    }

    public void setImageSelectList(List<ImageSelect> imageSelectList) {
        this.imageSelectList = imageSelectList;
    }
}
