package com.viettel.mbccs.data.source.remote;

import com.viettel.mbccs.data.source.remote.request.DataRequest;
import com.viettel.mbccs.data.source.remote.request.GetChannelByLocationRequest;
import com.viettel.mbccs.data.source.remote.request.GetChannelDetailRequest;
import com.viettel.mbccs.data.source.remote.request.UpdateChannelInfoRequest;
import com.viettel.mbccs.data.source.remote.response.GetChannelByLocationResponse;
import com.viettel.mbccs.data.source.remote.response.GetChannelDetailResponse;
import com.viettel.mbccs.data.source.remote.response.UpdateChannelInfoResponse;

import rx.Observable;

/**
 * Created by eo_cuong on 5/11/17.
 */

public interface IManageChannelRemoteDataSource {

    Observable<GetChannelByLocationResponse> getChannelByLocation(DataRequest<GetChannelByLocationRequest> request);

    Observable<GetChannelDetailResponse> getChannelDetail(DataRequest<GetChannelDetailRequest> request);

    Observable<UpdateChannelInfoResponse> updateChannelInfo(DataRequest<UpdateChannelInfoRequest> request);
}
