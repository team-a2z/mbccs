package com.viettel.mbccs.data.source.remote.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by jacky on 7/3/17.
 */

public class GetListSearchTransRequest  extends BaseRequest  {

    @Expose
    @SerializedName("shopId")
    public Long mShopId;

    @SerializedName("startDate")
    @Expose
    public String mFromDate;

    @Expose
    @SerializedName("endDate")
    public String mToDate;

    public Long staffId;

    public int saleTransStatus;
    @Expose
    public Integer saleTransType;

    public GetListSearchTransRequest() {
        super();
    }

    public Long getShopId() {
        return mShopId;
    }

    public void setShopId(Long shopId) {
        mShopId = shopId;
    }

    public String getFromDate() {
        return mFromDate;
    }

    public void setFromDate(String fromDate) {
        mFromDate = fromDate;
    }

    public String getToDate() {
        return mToDate;
    }

    public void setToDate(String toDate) {
        mToDate = toDate;
    }

}
