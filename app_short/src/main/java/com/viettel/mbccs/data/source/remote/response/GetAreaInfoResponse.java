package com.viettel.mbccs.data.source.remote.response;

import com.google.gson.annotations.Expose;
import com.viettel.mbccs.data.model.AreaInfo;

import java.util.List;

/**
 * Created by minhnx on 5/29/17.
 */

public class GetAreaInfoResponse extends DataResponse {
    @Expose
    private List<AreaInfo> lstAreaInfo;

    public List<AreaInfo> getLstAreaInfo() {
        return lstAreaInfo;
    }

    public void setLstAreaInfo(List<AreaInfo> lstAreaInfo) {
        this.lstAreaInfo = lstAreaInfo;
    }
}
