package com.viettel.mbccs.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by FRAMGIA\bui.dinh.viet on 11/09/2017.
 */

public class SaleTransType implements Parcelable {
    public long saleTransType;
    public String description;

    protected SaleTransType(Parcel in) {
        saleTransType = in.readLong();
        description = in.readString();
    }

    public static final Creator<SaleTransType> CREATOR = new Creator<SaleTransType>() {
        @Override
        public SaleTransType createFromParcel(Parcel in) {
            return new SaleTransType(in);
        }

        @Override
        public SaleTransType[] newArray(int size) {
            return new SaleTransType[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(saleTransType);
        parcel.writeString(description);
    }
}
