package com.viettel.mbccs.data.source.remote;

import com.viettel.mbccs.data.source.remote.request.DataRequest;
import com.viettel.mbccs.data.source.remote.request.GetAreaInfoRequest;
import com.viettel.mbccs.data.source.remote.request.GetListBtsesByAreaCodeRequest;
import com.viettel.mbccs.data.source.remote.request.GetListEventsByAreaCodeRequest;
import com.viettel.mbccs.data.source.remote.request.GetListShopsByAreaCodeRequest;
import com.viettel.mbccs.data.source.remote.request.UpdateBtsGpsRequest;
import com.viettel.mbccs.data.source.remote.response.GetAreaInfoResponse;
import com.viettel.mbccs.data.source.remote.response.GetListBtsesByAreaCodeResponse;
import com.viettel.mbccs.data.source.remote.response.GetListEventsByAreaCodeResponse;
import com.viettel.mbccs.data.source.remote.response.GetListShopsByAreaCodeResponse;
import com.viettel.mbccs.data.source.remote.response.UpdateBtsGpsResponse;

import rx.Observable;

/**
 * Created by eo_cuong on 5/11/17.
 */

public interface IManageAreaRemoteDataSource {

    Observable<GetAreaInfoResponse> getAreaInfo(DataRequest<GetAreaInfoRequest> request);

    Observable<GetListBtsesByAreaCodeResponse> getListBtsesByAreaCode(DataRequest<GetListBtsesByAreaCodeRequest> request);

    Observable<GetListShopsByAreaCodeResponse> getListShopsByAreaCode(DataRequest<GetListShopsByAreaCodeRequest> request);

    Observable<GetListEventsByAreaCodeResponse> getListEventsByAreaCode(DataRequest<GetListEventsByAreaCodeRequest> request);

    Observable<UpdateBtsGpsResponse> updateBtsGps(DataRequest<UpdateBtsGpsRequest> request);
}
