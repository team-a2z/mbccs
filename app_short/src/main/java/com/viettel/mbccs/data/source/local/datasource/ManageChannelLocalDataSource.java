package com.viettel.mbccs.data.source.local.datasource;

import com.google.gson.Gson;
import com.viettel.mbccs.data.source.local.IManageChannelLocalDataSource;

/**
 * Created by eo_cuong on 5/10/17.
 */

public class ManageChannelLocalDataSource implements IManageChannelLocalDataSource {
    private SharedPrefs sharedPrefs;
    private Gson gson;
    public static ManageChannelLocalDataSource instance;

    public ManageChannelLocalDataSource() {
        sharedPrefs = SharedPrefs.getInstance();
        gson = new Gson();
    }

    public static ManageChannelLocalDataSource getInstance() {
        if (instance == null) {
            instance = new ManageChannelLocalDataSource();
        }
        return instance;
    }


}
