package com.viettel.mbccs.data.source;

import com.viettel.mbccs.data.source.local.IManageAreaLocalDataSource;
import com.viettel.mbccs.data.source.local.datasource.ManageAreaLocalDataSource;
import com.viettel.mbccs.data.source.remote.IManageAreaRemoteDataSource;
import com.viettel.mbccs.data.source.remote.datasource.ManageAreaRemoteDataSource;
import com.viettel.mbccs.data.source.remote.request.DataRequest;
import com.viettel.mbccs.data.source.remote.request.GetAreaInfoRequest;
import com.viettel.mbccs.data.source.remote.request.GetListBtsesByAreaCodeRequest;
import com.viettel.mbccs.data.source.remote.request.GetListEventsByAreaCodeRequest;
import com.viettel.mbccs.data.source.remote.request.GetListShopsByAreaCodeRequest;
import com.viettel.mbccs.data.source.remote.request.UpdateBtsGpsRequest;
import com.viettel.mbccs.data.source.remote.response.GetAreaInfoResponse;
import com.viettel.mbccs.data.source.remote.response.GetListBtsesByAreaCodeResponse;
import com.viettel.mbccs.data.source.remote.response.GetListEventsByAreaCodeResponse;
import com.viettel.mbccs.data.source.remote.response.GetListShopsByAreaCodeResponse;
import com.viettel.mbccs.data.source.remote.response.UpdateBtsGpsResponse;

import rx.Observable;

/**
 * Created by eo_cuong on 5/10/17.
 */

public class ManageAreaRepository implements IManageAreaLocalDataSource, IManageAreaRemoteDataSource {

    private volatile static ManageAreaRepository instance;
    private IManageAreaLocalDataSource localDataSource;
    private IManageAreaRemoteDataSource remoteDataSource;

    public ManageAreaRepository(IManageAreaLocalDataSource localDataSource,
                                IManageAreaRemoteDataSource remoteDataSource) {
        this.localDataSource = localDataSource;
        this.remoteDataSource = remoteDataSource;
    }

    public static ManageAreaRepository getInstance() {
        if (instance == null) {
            instance = new ManageAreaRepository(ManageAreaLocalDataSource.getInstance(),
                    ManageAreaRemoteDataSource.getInstance());
        }
        return instance;
    }

    @Override
    public Observable<GetAreaInfoResponse> getAreaInfo(DataRequest<GetAreaInfoRequest> request) {
        return remoteDataSource.getAreaInfo(request);
    }

    @Override
    public Observable<GetListBtsesByAreaCodeResponse> getListBtsesByAreaCode(DataRequest<GetListBtsesByAreaCodeRequest> request) {
        return remoteDataSource.getListBtsesByAreaCode(request);
    }

    @Override
    public Observable<GetListShopsByAreaCodeResponse> getListShopsByAreaCode(DataRequest<GetListShopsByAreaCodeRequest> request) {
        return remoteDataSource.getListShopsByAreaCode(request);
    }

    @Override
    public Observable<GetListEventsByAreaCodeResponse> getListEventsByAreaCode(DataRequest<GetListEventsByAreaCodeRequest> request) {
        return remoteDataSource.getListEventsByAreaCode(request);
    }

    @Override
    public Observable<UpdateBtsGpsResponse> updateBtsGps(DataRequest<UpdateBtsGpsRequest> request) {
        return remoteDataSource.updateBtsGps(request);
    }
}
