package com.viettel.mbccs.data.source.remote.response;

import com.google.gson.annotations.Expose;
import com.viettel.mbccs.data.model.FestivalInfo;
import com.viettel.mbccs.data.model.MarketInfo;

import java.util.List;

/**
 * Created by minhnx on 5/29/17.
 */

public class GetListEventsByAreaCodeResponse extends DataResponse {
    @Expose
    private List<MarketInfo> lstMarket;
    @Expose
    private List<FestivalInfo> lstFestival;

    public List<MarketInfo> getLstMarket() {
        return lstMarket;
    }

    public void setLstMarket(List<MarketInfo> lstMarket) {
        this.lstMarket = lstMarket;
    }

    public List<FestivalInfo> getLstFestival() {
        return lstFestival;
    }

    public void setLstFestival(List<FestivalInfo> lstFestival) {
        this.lstFestival = lstFestival;
    }
}
