package com.viettel.mbccs.data.source.remote.response;

import com.google.gson.annotations.Expose;
import com.viettel.mbccs.data.model.Bts;

import java.util.List;

/**
 * Created by minhnx on 5/29/17.
 */

public class GetListBtsesByAreaCodeResponse extends DataResponse {
    @Expose
    private List<Bts> lstBts;

    public List<Bts> getLstBts() {
        return lstBts;
    }

    public void setLstBts(List<Bts> lstBts) {
        this.lstBts = lstBts;
    }
}
