package com.viettel.mbccs.data.source.remote.response;

import com.google.gson.annotations.Expose;
import com.viettel.mbccs.data.model.ImageSelect;
import com.viettel.mbccs.data.model.StaffSM;

import java.util.List;

/**
 * Created by minhnx on 5/29/17.
 */

public class GetDistributedChannelResponse extends DataResponse {

    @Expose
    private List<ImageSelect> lstImageBO;

    @Expose
    private List<StaffSM> listStaff;

    public List<StaffSM> getListStaff() {
        return listStaff;
    }

    public void setListStaff(List<StaffSM> listStaff) {
        this.listStaff = listStaff;
    }

    public List<ImageSelect> getLstImageBO() {
        return lstImageBO;
    }

    public void setLstImageBO(List<ImageSelect> lstImageBO) {
        this.lstImageBO = lstImageBO;
    }
}
