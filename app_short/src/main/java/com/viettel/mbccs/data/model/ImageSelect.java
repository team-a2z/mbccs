package com.viettel.mbccs.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nguyenhuyquyet on 9/21/17.
 */

public class ImageSelect implements Parcelable {

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("content")
    @Expose
    private String content;

    @SerializedName("imageTitle")
    @Expose
    private String title;
    @Expose
    private boolean flag;


    public ImageSelect() {

    }

    public ImageSelect(String name, String content, String title) {
        this.name = name;
        this.content = content;
        this.title = title;
    }

    protected ImageSelect(Parcel in) {
        name = in.readString();
        content = in.readString();
        title = in.readString();
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(content);
        dest.writeString(title);
    }

    public static final Creator<ImageSelect> CREATOR = new Creator<ImageSelect>() {
        @Override
        public ImageSelect createFromParcel(Parcel in) {
            return new ImageSelect(in);
        }

        @Override
        public ImageSelect[] newArray(int size) {
            return new ImageSelect[size];
        }
    };

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
