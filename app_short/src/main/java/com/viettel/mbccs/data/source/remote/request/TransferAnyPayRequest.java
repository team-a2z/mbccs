package com.viettel.mbccs.data.source.remote.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by minhnx on 6/7/17.
 */

public class TransferAnyPayRequest extends BaseRequest {
    @Expose
    @SerializedName("fromISDN")
    private String fromIsdn;
    @Expose
    @SerializedName("toISDN")
    private String toIsdn;
    @Expose
    @SerializedName("amount")
    private Long amount;
    @Expose
    @SerializedName("mPIN")
    private String mPIN;
    @Expose
    @SerializedName("iccid")
    private String iccid;

    public String getFromIsdn() {
        return fromIsdn;
    }

    public void setFromIsdn(String fromIsdn) {
        this.fromIsdn = fromIsdn;
    }

    public String getToIsdn() {
        return toIsdn;
    }

    public void setToIsdn(String toIsdn) {
        this.toIsdn = toIsdn;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public TransferAnyPayRequest() {
        super();
    }

    public String getmPIN() {
        return mPIN;
    }

    public void setmPIN(String mPIN) {
        this.mPIN = mPIN;
    }

    public String getIccid() {
        return iccid;
    }

    public void setIccid(String iccid) {
        this.iccid = iccid;
    }
}
