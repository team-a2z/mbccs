package com.viettel.mbccs.dialog.languagepicker;

import android.support.annotation.IntDef;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Created by HuyQuyet on 5/25/17.
 */

/**
 * https://en.wikipedia.org/wiki/List_of_ISO_639-2_codes#E
 */

public class Language {

    @IntDef({LANGUAGES_ISO.ISO_639_1, LANGUAGES_ISO.ISO_639_2, LANGUAGES_ISO.ISO_639_3})
    public @interface LANGUAGES_ISO {
        int ISO_639_1 = 1;
        int ISO_639_2 = 2;
        int ISO_639_3 = 3;
    }

    public static final Language[] LANGUAGES = {
            new Language("uie", "vie", "vi", "Vietnamese"),
            new Language("eng", "eng", "en", "English"),
            new Language("fra", "fra", "fr", "French"),
            new Language("aym", "aym", "ay", "Aymara"),
            new Language("bur", "mya", "my", "Burmese (Myanma)"),
            new Language("hat", "hat", "ht", "Haitian, Haitian Creole"),
            new Language("ind", "ind", "id", "Indonesian"),
            new Language("khm", "khm", "km", "Central Khmer (Cambodia)"),
            new Language("lao", "lao", "lo", "Lao"),
            new Language("por", "por", "pt", "Portuguese"),
            new Language("que", "que", "qu", "Quechua"),
            new Language("run", "run", "rn", "Rundi"),
            new Language("spa", "spa", "es", "Spanish, Castilian"),
            new Language("swa", "swa", "sw", "Swahili"),
    };


    public Language() {

    }

    public Language(String codeLanguage, String nameLanguage) {
        this.codeLanguage_639_3 = codeLanguage;
        this.nameLanguage = nameLanguage;
    }

    public Language(String codeLanguage_639_2, String codeLanguage_693_3, String codeLanguage_639_1, String nameLanguage) {
        this.codeLanguage_639_3 = codeLanguage_693_3;
        this.codeLanguage_639_2 = codeLanguage_639_2;
        this.codeLanguage_639_1 = codeLanguage_639_1;
        this.nameLanguage = nameLanguage;
    }

    /**
     * 639 - 3
     */
    private String codeLanguage_639_3;
    /**
     * 639 - 2
     */
    private String codeLanguage_639_2;
    /**
     * 639 - 1
     */
    private String codeLanguage_639_1;
    private String nameLanguage;

    public String getCodeLanguage_639_3() {
        return codeLanguage_639_3;
    }

    public void setCodeLanguage_639_3(String codeLanguage_639_3) {
        this.codeLanguage_639_3 = codeLanguage_639_3;
    }

    public String getNameLanguage() {
        return nameLanguage;
    }

    public void setNameLanguage(String nameLanguage) {
        this.nameLanguage = nameLanguage;
    }

    public String getCodeLanguage_639_2() {
        return codeLanguage_639_2;
    }

    public void setCodeLanguage_639_2(String codeLanguage_639_2) {
        this.codeLanguage_639_2 = codeLanguage_639_2;
    }

    public String getCodeLanguage_639_1() {
        return codeLanguage_639_1;
    }

    public void setCodeLanguage_639_1(String codeLanguage_639_1) {
        this.codeLanguage_639_1 = codeLanguage_639_1;
    }

    public static Language getLanguageByCode(String languageCode) {
        languageCode = languageCode.toLowerCase();

        Language l = new Language();
        l.setCodeLanguage_639_3(languageCode);

        for (Language LANGUAGE : LANGUAGES) {
            if (LANGUAGE.codeLanguage_639_3.equals(languageCode)) {
                return LANGUAGE;
            }
        }
        return null;
    }

    public static Language getLanguageByCode(String languageCode, @LANGUAGES_ISO int iso) {
        languageCode = languageCode.toLowerCase();

        switch (iso) {
            case LANGUAGES_ISO.ISO_639_1:
                for (Language LANGUAGE : LANGUAGES) {
                    if (LANGUAGE.codeLanguage_639_1.equals(languageCode)) {
                        return LANGUAGE;
                    }
                }
                return null;

            case LANGUAGES_ISO.ISO_639_2:
                for (Language LANGUAGE : LANGUAGES) {
                    if (LANGUAGE.codeLanguage_639_2.equals(languageCode)) {
                        return LANGUAGE;
                    }
                }
                return null;

            case LANGUAGES_ISO.ISO_639_3:
                for (Language LANGUAGE : LANGUAGES) {
                    if (LANGUAGE.codeLanguage_639_3.equals(languageCode)) {
                        return LANGUAGE;
                    }
                }
                return null;
        }
        return null;
    }

    public static Language getLanguageByName(String languageName) {

        Language l = new Language();
        l.setNameLanguage(languageName);

        int i = Arrays.binarySearch(LANGUAGES, l, new ISONameComparator());

        if (i < 0) {
            return null;
        } else {
            return LANGUAGES[i];
        }
    }

    public static class ISOCodeComparator_639_3 implements Comparator<Language> {
        @Override
        public int compare(Language l, Language l1) {
            return l.codeLanguage_639_3.compareTo(l1.codeLanguage_639_3);
        }
    }

    public static class ISOCodeComparator_639_2 implements Comparator<Language> {
        @Override
        public int compare(Language l, Language l1) {
            return l.codeLanguage_639_2.compareTo(l1.codeLanguage_639_2);
        }
    }

    public static class ISOCodeComparator_639_1 implements Comparator<Language> {
        @Override
        public int compare(Language l, Language l1) {
            return l.codeLanguage_639_1.compareTo(l1.codeLanguage_639_1);
        }
    }

    public static class ISONameComparator implements Comparator<Language> {
        @Override
        public int compare(Language l, Language l1) {
            return l.nameLanguage.compareTo(l1.nameLanguage);
        }
    }
}
