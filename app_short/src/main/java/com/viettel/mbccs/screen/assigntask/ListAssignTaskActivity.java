package com.viettel.mbccs.screen.assigntask;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;

import com.viettel.mbccs.data.model.TaskShopManagement;
import com.viettel.mbccs.data.model.TaskStaffManagement;
import com.viettel.mbccs.screen.assigntask.arising.create.CreateArisingTaskActivity;
import com.viettel.mbccs.screen.assigntask.arising.detail.ArisingTaskDetailActivity;
import com.viettel.mbccs.screen.assigntask.cskpp.create.CreateCSKPPTaskActivity;
import com.viettel.mbccs.variable.Constants;

/**
 * Created by Anh Vu Viet on 5/20/2017.
 */

public class ListAssignTaskActivity extends BaseListTaskActivity<ListAssignTaskPresenter> {

    public static final String EXTRA_TASK_TYPE = "EXTRA_TASK_TYPE";

    private int mDefaultType;

    @Override
    protected void initData() {
        mDefaultType = getIntent().getIntExtra(EXTRA_TASK_TYPE, TaskShopManagement.TaskType.TYPE_PHAT_SINH);
        mPresenter = new ListAssignTaskPresenter(this, this, mDefaultType);
        mBinding.setPresenter(mPresenter);
        mBinding.drawer.animateOpen();
        mBinding.swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.doSearch(true);
            }
        });
    }

    @Override
    public void onAddClick() {

        switch (mDefaultType) {
            case TaskShopManagement.TaskType.TYPE_CSKPP:
                startActivity(new Intent(ListAssignTaskActivity.this,
                        CreateCSKPPTaskActivity.class));
                break;
            case TaskShopManagement.TaskType.TYPE_PHAT_SINH:
                startActivity(new Intent(ListAssignTaskActivity.this,
                        CreateArisingTaskActivity.class));
                break;
        }

    }

    @Override
    public void onItemClicked(Object object) {
        try {
            TaskStaffManagement taskStaffManagement = (TaskStaffManagement) object;

            Intent intent = new Intent(this, ArisingTaskDetailActivity.class);

            if (taskStaffManagement.getStatusTaskStaff().equals(TaskShopManagement.TaskStatus.STATUS_NEW))
                intent.putExtra(Constants.BundleConstant.STOCK_VIEW_ONLY, false);
            else
                intent.putExtra(Constants.BundleConstant.STOCK_VIEW_ONLY, true);

            intent.putExtra(Constants.BundleConstant.ITEM_LIST, taskStaffManagement);
            startActivityForResult(intent, REQUEST_CODE);
            return;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
//DACUET
//        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout
//                .simple_spinner_item, getResources().getStringArray(R.array.task_type));
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        final LayoutPopupSpinnerBinding binding = LayoutPopupSpinnerBinding.inflate
//                (getLayoutInflater());
//        binding.setLabel(getString(R.string.task_type));
//        binding.spinner.setAdapter(adapter);
//        switch (mDefaultType) {
//            case TaskShopManagement.TaskType.TYPE_CSKPP:
//                binding.spinner.setSelectedPosition(0);
//                break;
//            case TaskShopManagement.TaskType.TYPE_PHAT_SINH:
//                binding.spinner.setSelectedPosition(1);
//                break;
//        }
//
//        new AlertDialog.Builder(this).setTitle(R.string.select_assign_task_type).setView(binding
//                .getRoot()).setPositiveButton(R.string.text_continue, new DialogInterface
//                .OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                switch (binding.spinner.getSpinner().getSelectedItemPosition()) {
//                    case 0:
//                        startActivity(new Intent(ListAssignTaskActivity.this,
//                                CreateCSKPPTaskActivity.class));
//                        break;
//                    case 1:
//                        startActivity(new Intent(ListAssignTaskActivity.this,
//                                CreateArisingTaskActivity.class));
//                        break;
//                    default:
//                        break;
//                }
//            }
//        }).setNegativeButton(R.string.common_label_close, null).setCancelable(false).create()
//                .show();

