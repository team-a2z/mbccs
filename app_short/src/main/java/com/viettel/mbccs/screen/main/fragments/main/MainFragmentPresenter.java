package com.viettel.mbccs.screen.main.fragments.main;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;

import com.viettel.mbccs.R;
import com.viettel.mbccs.constance.WsCode;
import com.viettel.mbccs.data.model.Dashboard;
import com.viettel.mbccs.data.source.UserRepository;
import com.viettel.mbccs.data.source.remote.request.DataRequest;
import com.viettel.mbccs.data.source.remote.request.GetDashBoardInfoRequest;
import com.viettel.mbccs.data.source.remote.response.BaseException;
import com.viettel.mbccs.data.source.remote.response.GetDashBoardInfoResponse;
import com.viettel.mbccs.utils.rx.MBCCSSubscribe;
import com.viettel.mbccs.widget.SpacesItemDecoration;

import java.util.ArrayList;
import java.util.List;

import static com.viettel.mbccs.screen.main.fragments.main.MainRecyclerAdapter.TYPE_TEXT_COMPLEX;

/**
 * Created by eo_cuong on 5/11/17.
 */

public class MainFragmentPresenter implements MainFragmentContract.Presenter {

    private Context mContext;

    private MainFragmentContract.ViewModel mViewModel;

    private MainRecyclerAdapter mRecyclerAdapter;

    private RecyclerView.LayoutManager mLayoutManager;

    private List<Dashboard> mDashboardList = new ArrayList<>();

    public MainFragmentPresenter(Context context, MainFragmentContract.ViewModel viewModel) {
        mContext = context;
        mViewModel = viewModel;
        mRecyclerAdapter = new MainRecyclerAdapter(mContext, mDashboardList);

        getDashboardInfo();
    }

    private void getDashboardInfo() {
        UserRepository repository = UserRepository.getInstance();
        GetDashBoardInfoRequest request = new GetDashBoardInfoRequest();
        request.setShopId(String.valueOf(repository.getUserInfo().getShop().getShopId()));
        request.setStaffId(String.valueOf(repository.getUserInfo().getStaffInfo().getStaffId()));
        request.setLstFunction(repository.getFunctionsCodes());

        DataRequest<GetDashBoardInfoRequest> dataRequest = new DataRequest<>();
        dataRequest.setWsRequest(request);
        dataRequest.setWsCode(WsCode.GetDashBoardInfo);

        repository.getDashBoardInfo(dataRequest).subscribe(new MBCCSSubscribe<GetDashBoardInfoResponse>() {
            @Override
            public void onSuccess(GetDashBoardInfoResponse object) {
                mDashboardList.addAll(object.getLstDashBoard());
                mRecyclerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onError(BaseException error) {
                // TODO: 8/21/2017 Show error
            }
        });
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {

    }

    public RecyclerView.Adapter getRecyclerAdapter() {
        return mRecyclerAdapter;
    }

    public synchronized RecyclerView.LayoutManager getLayoutManager() {
        if (mLayoutManager == null)
            mLayoutManager = new GridLayoutManager(mContext, 2);
        return mLayoutManager;
    }

    public GridLayoutManager.SpanSizeLookup getSpanSizeLookup() {
        return new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (mRecyclerAdapter.getItemViewType(position)) {
                    case TYPE_TEXT_COMPLEX:
                        return 2;
                    default:
                        return 1;
                }
            }
        };
    }

    public RecyclerView.ItemDecoration getRecyclerDecoration() {
        return new SpacesItemDecoration((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimension(R.dimen.dp_6), mContext.getResources().getDisplayMetrics()), getLayoutManager());
    }
}
