package com.viettel.mbccs.screen.assigntask;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.viettel.mbccs.R;
import com.viettel.mbccs.constance.WsCode;
import com.viettel.mbccs.data.model.InfoTaskExtend;
import com.viettel.mbccs.data.model.TaskShopManagement;
import com.viettel.mbccs.data.source.remote.request.DataRequest;
import com.viettel.mbccs.data.source.remote.request.GetInfoTaskForUpdateRequest;
import com.viettel.mbccs.data.source.remote.response.BaseException;
import com.viettel.mbccs.data.source.remote.response.GetInfoTaskForUpdateResponse;
import com.viettel.mbccs.screen.assigntask.adapters.UpdateTaskAdapter;
import com.viettel.mbccs.utils.DateUtils;
import com.viettel.mbccs.utils.DialogUtils;
import com.viettel.mbccs.utils.rx.MBCCSSubscribe;

import rx.Subscription;

/**
 * Created by Anh Vu Viet on 7/2/2017.
 */

public class ListUpdateTaskPresenter extends BaseListTaskPresenter<InfoTaskExtend>
        implements BaseListTaskContract.Presenter {

    public ListUpdateTaskPresenter(Context context, BaseListTaskContract.ViewModel viewModel) {
        super(context, viewModel, TaskShopManagement.TaskType.TYPE_PHAT_SINH);
    }

    @Override
    public void doSearch(boolean showProgress) {
        long fromDate = mViewModel.getFromDate();
        long toDate = mViewModel.getToDate();
        if (fromDate > toDate) {
            if (fromDate > toDate) {
                DialogUtils.showDialog(mContext,
                        mContext.getString(R.string.common_msg_error_greater_fields2, mContext.getString(R.string.common_label_to_date), mContext.getString(R.string.common_label_from_date)));
            }
            return;
        }

        if (showProgress)
            mViewModel.showLoading();

        listData.clear();

        GetInfoTaskForUpdateRequest request = new GetInfoTaskForUpdateRequest();
        request.setShopId(String.valueOf(mUserRepository.getUserInfo().getShop().getShopId()));
        request.setType(String.valueOf(taskType.get()));
        request.setFromDate(
                DateUtils.convertDateToString(fromDate, DateUtils.TIMEZONE_FORMAT_SERVER));
        request.setToDate(DateUtils.convertDateToString(toDate, DateUtils.TIMEZONE_FORMAT_SERVER));
        request.setStaffId(
                String.valueOf(mUserRepository.getUserInfo().getStaffInfo().getStaffId()));
        request.setStatus(String.valueOf(taskStatus.get()));

        DataRequest<GetInfoTaskForUpdateRequest> dataRequest = new DataRequest<>();
        dataRequest.setWsCode(WsCode.GetInfoTaskForUpdate);
        dataRequest.setWsRequest(request);

        Subscription subscription = mRepository.getInfoTaskForUpdate(dataRequest)
                .subscribe(new MBCCSSubscribe<GetInfoTaskForUpdateResponse>() {
                    @Override
                    public void onSuccess(GetInfoTaskForUpdateResponse object) {
                        onSearchSuccess();
                        if (object != null && object.getLstInfoTaskExtend() != null
                                && !object.getLstInfoTaskExtend().isEmpty())
                            listData.addAll(object.getLstInfoTaskExtend());

                        itemCount.set(listData.size());
                        isEmpty.set(listData.isEmpty());

                        adapter.get().notifyDataSetChanged();
                        onAdapterChangeSize(itemCount.get());
                    }

                    @Override
                    public void onError(BaseException error) {
                        mViewModel.hideLoading();
                        mViewModel.showErrorDialog(error);
                    }
                });
        mSubscription.add(subscription);
    }

    @Override
    protected RecyclerView.Adapter getListAdapter() {
        UpdateTaskAdapter adapter = new UpdateTaskAdapter(mContext, listData);
        adapter.setOnTaskClickListener(this);
        return adapter;
    }

    @Override
    public String[] getStatusArray() {
        return mContext.getResources().getStringArray(R.array.task_update_status);
    }
}
