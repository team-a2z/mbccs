package com.viettel.mbccs.screen.common.adapter;

import android.content.Context;

import com.viettel.mbccs.data.model.KeyValue;

/**
 * Created by eo_cuong on 5/15/17.
 */

public class ItemKeyValueAutoCompletePresenter {

    private KeyValue mItem;
    private Context mContext;

    public ItemKeyValueAutoCompletePresenter(Context context, KeyValue item) {
        mItem = item;
        mContext = context;
    }

    public KeyValue getItem() {
        return mItem;
    }

}
