package com.viettel.mbccs.screen.common.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.viettel.mbccs.R;
import com.viettel.mbccs.data.model.KeyValue;
import com.viettel.mbccs.databinding.ItemSimpleRecycleViewBinding;

import java.util.List;

/**
 * Created by minhnx on 5/19/17.
 */

public class SimpleRecycleViewAdapter extends RecyclerView.Adapter<SimpleRecycleViewAdapter.ViewHolder> {

    private Context mContext;
    private List<KeyValue> mItems;

    private OnItemClickListener listener;

    public SimpleRecycleViewAdapter(Context context, List<KeyValue> items) {
        mContext = context;
        mItems = items;
    }

    public Context getContext() {
        return mContext;
    }

    public void setContext(Context context) {
        mContext = context;
    }

    public List<KeyValue> getItems() {
        return mItems;
    }

    public void setItems(List<KeyValue> items) {
        mItems = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(
                (ItemSimpleRecycleViewBinding) DataBindingUtil.inflate(LayoutInflater.from(mContext),
                        R.layout.item_simple_recycle_view, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(mItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ItemSimpleRecycleViewBinding mBinding;
        KeyValue mItem;

        public ViewHolder(final ItemSimpleRecycleViewBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        public void bind(KeyValue item) {
            mItem = item;
            ItemSimpleRecycleViewPresenter itemChangeSimPresenter = new ItemSimpleRecycleViewPresenter(mContext, item);
            mBinding.setPresenter(itemChangeSimPresenter);

            mBinding.tvItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null)
                        listener.onClick(view, mItem);
                }
            });
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onClick(View view, KeyValue item);
    }
}