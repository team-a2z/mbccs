package com.viettel.mbccs.screen.config;

import android.content.Intent;
import android.os.Build;
import android.support.v4.app.ActivityCompat;

import com.google.gson.Gson;
import com.viettel.mbccs.R;
import com.viettel.mbccs.base.BaseDataBindActivity;
import com.viettel.mbccs.databinding.ActivityConfigBinding;
import com.viettel.mbccs.dialog.countrypicker.Country;
import com.viettel.mbccs.dialog.countrypicker.CountryPicker;
import com.viettel.mbccs.dialog.countrypicker.CountryPickerListener;
import com.viettel.mbccs.dialog.languagepicker.Language;
import com.viettel.mbccs.dialog.languagepicker.LanguagePicker;
import com.viettel.mbccs.dialog.languagepicker.LanguagePickerListener;
import com.viettel.mbccs.screen.resetpass.ResetPasswordActivity;
import com.viettel.mbccs.screen.splash.SplashActivity;
import com.viettel.mbccs.utils.LocaleUtils;
import com.viettel.mbccs.variable.Constants;

import java.util.List;

public class ConfigActivity extends BaseDataBindActivity<ActivityConfigBinding, ConfigPresenter>
        implements ConfigContract.View {
    private static final String COUNTRY_PICKER = "COUNTRY_PICKER";
    private static final String LANGUAGE_PICKER = "LANGUAGE_PICKER";

    private List<Country> countries;
    private List<Language> languageList;

    @Override
    public void onBackPressed() {
        onFinish();
    }

    @Override
    protected int getIdLayout() {
        return R.layout.activity_config;
    }

    @Override
    protected void initData() {
        mPresenter = new ConfigPresenter(this, this);
        mBinding.setPresenter(mPresenter);
        mPresenter.subscribe();
        mPresenter.setPositionSpinner(mPresenter.getPositionDefaultSpinnerSync());

        mBinding.spinnerTimeSyncBccs.setOnItemSelectedListener(mPresenter);
    }


    @Override
    public void showLoading() {
        showLoadingDialog();
    }

    @Override
    public void hideLoading() {
        hideLoadingDialog();
    }

    @Override
    public void onFinish() {
        finish();
    }

    @Override
    public void selectCountry() {
        countries = mPresenter.getListCountry();
        final CountryPicker picker =
                CountryPicker.newInstance(getString(R.string.config_select_country),
                        new Gson().toJson(countries));  // dialog title
        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(Country country) {
                mPresenter.setCounty(country);
                picker.dismiss();
            }
        });
        picker.show(getSupportFragmentManager(), COUNTRY_PICKER);
    }

    @Override
    public void selectLanguage() {
        languageList = mPresenter.getListLanguage();
        final LanguagePicker picker =
                LanguagePicker.newInstance(getString(R.string.config_select_language),
                        new Gson().toJson(languageList));  // dialog title
        picker.setListener(new LanguagePickerListener() {
            @Override
            public void onSelectLanguage(Language language) {
                mPresenter.setLanguage(language);
                picker.dismiss();
            }
        });
        picker.show(getSupportFragmentManager(), LANGUAGE_PICKER);
    }

    @Override
    public void changeLanguage(String language) {
        LocaleUtils.setLocale(this, language);

        Intent refresh = new Intent(this, SplashActivity.class);
        startActivity(refresh);
        if (Build.VERSION.SDK_INT >= 16) {
            finishAffinity();
        } else {
            ActivityCompat.finishAffinity(this);
        }
    }

    @Override
    public void openChangePassword() {
        Intent intent = new Intent(this, ResetPasswordActivity.class);
        intent.putExtra(Constants.BundleConstant.EXTRA_FORGOT_PW, false);
        startActivity(intent);
    }
}
