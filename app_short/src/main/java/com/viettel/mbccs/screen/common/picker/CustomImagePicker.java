package com.viettel.mbccs.screen.common.picker;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.databinding.BindingMethod;
import android.databinding.BindingMethods;
import android.databinding.ObservableField;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.viettel.mbccs.R;
import com.viettel.mbccs.databinding.LayoutSelectImageBinding;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by minhnx on 7/31/17.
 */

@BindingMethods({
        @BindingMethod(type = CustomImagePicker.class, attribute = "base64Image", method = "setBase64Image"),
        @BindingMethod(type = CustomImagePicker.class, attribute = "image", method = "setImage"),
        @BindingMethod(type = CustomImagePicker.class, attribute = "text", method = "setText"),
        @BindingMethod(type = CustomImagePicker.class, attribute = "viewOnly", method = "setViewOnly")
})
public class CustomImagePicker extends LinearLayout {

    @IntDef({TypeSelect.CAMERA, TypeSelect.GALLERY})
    public @interface TypeSelect {
        int CAMERA = 1;
        int GALLERY = 2;
    }

    private static final int IMAGE = 1;

    private Context context;
    private LayoutSelectImageBinding binding;
    private SelectImageCallback callback;
    private int imageSelect;

    public ObservableField<Bitmap> image;
    public ObservableField<String> title;

    private Uri imageFileUri;
    private String imageFilePath;
    private String imageName;
    private boolean isViewOnly = false;

    public CustomImagePicker(Context context) {
        this(context, null);
    }

    public CustomImagePicker(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomImagePicker(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initView();
    }

    private void initView() {
        binding = LayoutSelectImageBinding.inflate(LayoutInflater.from(context), this, true);
        binding.setPresenter(this);
        image = new ObservableField<>();
        title = new ObservableField<>();
    }

    public void onSelectImage(View v) {

        if (isViewOnly)
            return;

        imageSelect = IMAGE;

        final CharSequence[] items = {
                context.getString(R.string.picker_photo_take_photo),
                context.getString(R.string.picker_photo_chooose_library),
                context.getString(R.string.picker_photo_cancel)
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.picker_photo_add_photo));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(context.getString(R.string.picker_photo_take_photo))) {
                    cameraIntent();
                } else if (items[item].equals(
                        context.getString(R.string.picker_photo_chooose_library))) {
                    galleryIntent();
                } else if (items[item].equals(context.getString(R.string.picker_photo_cancel))) {
                    dialog.dismiss();
                }
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void cameraIntent() {
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (captureIntent.resolveActivity(context.getPackageManager()) != null) {
            try {
                // Create an image file name
                String timeStamp =
                        new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
                File storageDir =
                        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
                File image = File.createTempFile("JPEG_" + timeStamp, ".jpg", storageDir);
                imageName = "JPEG_" + timeStamp;

                imageFilePath = image.getAbsolutePath();
                imageFileUri = Uri.fromFile(image);
                captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageFileUri);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (callback != null) {
                callback.onSelectImage(captureIntent, TypeSelect.CAMERA, getId());
            }
        }
    }

    private void galleryIntent() {
        Intent intent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        if (callback != null) {
            callback.onSelectImage(intent, TypeSelect.GALLERY, getId());
        }
    }

    private void onSelectImageResult(Intent intent) {
        Uri selectedImage = null;
        Bitmap thumbnail;
        ExifInterface ei = null;
        String picturePath;

        if (intent == null || intent.getData() == null) {
            selectedImage = imageFileUri;
            picturePath = imageFilePath;
        } else {
            selectedImage = intent.getData();
            String[] filePath = {MediaStore.Images.Media.DATA};
            Cursor c =
                    context.getContentResolver().query(selectedImage, filePath, null, null, null);
            c.moveToFirst();
            int columnIndex = c.getColumnIndex(filePath[0]);
            picturePath = c.getString(columnIndex);
            c.close();
        }

        thumbnail = decodeBitmapFromPathFile(picturePath, 200, 200);
        try {
            ei = new ExifInterface(picturePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);

        switch (orientation) {

            case ExifInterface.ORIENTATION_ROTATE_90:
                setImageView(rotateImage(thumbnail, 90), selectedImage);
                break;

            case ExifInterface.ORIENTATION_ROTATE_180:
                setImageView(rotateImage(thumbnail, 180), selectedImage);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                setImageView(rotateImage(thumbnail, 270), selectedImage);
                break;

            case ExifInterface.ORIENTATION_NORMAL:

            default:
                setImageView(thumbnail, selectedImage);
                break;
        }
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        Bitmap result =
                Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix,
                        true);
        source.recycle();
        return result;
    }

    private String path(Uri selectedImage) {
        String picturePath;
        String[] filePath = {MediaStore.Images.Media.DATA};
        Cursor c = context.getContentResolver().query(selectedImage, filePath, null, null, null);
        c.moveToFirst();
        int columnIndex = c.getColumnIndex(filePath[0]);
        picturePath = c.getString(columnIndex);
        c.close();
        return picturePath;
    }

    private void setImageView(Bitmap thumbnail, Uri selectedImage) {
        switch (imageSelect) {
            case IMAGE:
                image.set(thumbnail);
                setImage(thumbnail);
                break;
        }
    }

    public void setBase64Image(String base64) {
        Glide.with(context)
                .load(base64 == null ? null : Base64.decode(base64, Base64.DEFAULT))
                .asBitmap()
                .placeholder(R.drawable.ic_camera_placeholder)
                .into(binding.ivPicker);
    }

    public void setImage(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

        Glide.with(context)
                .load(stream.toByteArray())
                .placeholder(R.drawable.ic_camera_placeholder)
                .dontAnimate()
                .into(binding.ivPicker);
    }

    public void setText(String text) {
        title.set(text);
    }

    public void setViewOnly(boolean viewOnly) {
        isViewOnly = true;
    }

    public Bitmap getBitmapImage() {
        return image.get();
    }


    public void recycleBitmap() {
        if (image.get() != null) {
            image.get().recycle();
        }

        image.set(null);
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public Bitmap decodeBitmapFromPathFile(String pathName, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(pathName, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(pathName, options);
    }

    public void setResultIntent(Intent intent, @TypeSelect int type) {
        onSelectImageResult(intent);
    }

    public interface SelectImageCallback {
        void onSelectImage(Intent intent, int type, int id);
    }

    public void setSelectImageCallback(SelectImageCallback callback) {
        this.callback = callback;
    }

}