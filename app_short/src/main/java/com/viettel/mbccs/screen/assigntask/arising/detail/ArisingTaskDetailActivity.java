package com.viettel.mbccs.screen.assigntask.arising.detail;

import android.content.DialogInterface;
import android.databinding.ObservableBoolean;
import android.graphics.Color;
import android.text.TextUtils;

import com.viettel.mbccs.R;
import com.viettel.mbccs.base.BaseDataBindActivity;
import com.viettel.mbccs.constance.WsCode;
import com.viettel.mbccs.data.model.InfoTaskExtend;
import com.viettel.mbccs.data.model.TaskShopManagement;
import com.viettel.mbccs.data.model.TaskStaffManagement;
import com.viettel.mbccs.data.source.CongViecRepository;
import com.viettel.mbccs.data.source.UserRepository;
import com.viettel.mbccs.data.source.remote.request.DataRequest;
import com.viettel.mbccs.data.source.remote.request.UpdateTaskRequest;
import com.viettel.mbccs.data.source.remote.response.BaseException;
import com.viettel.mbccs.data.source.remote.response.UpdateTaskResponse;
import com.viettel.mbccs.databinding.ActivityArisingTaskDetailBinding;
import com.viettel.mbccs.utils.DateUtils;
import com.viettel.mbccs.utils.DialogUtils;
import com.viettel.mbccs.utils.rx.MBCCSSubscribe;
import com.viettel.mbccs.variable.Constants;
import com.viettel.mbccs.widget.CustomDialog;

import java.util.Date;
import java.util.Locale;

/**
 * Created by Anh Vu Viet on 5/29/2017.
 */

public class ArisingTaskDetailActivity
        extends BaseDataBindActivity<ActivityArisingTaskDetailBinding, ArisingTaskDetailActivity> {

    public ObservableBoolean viewOnly = new ObservableBoolean();

    private InfoTaskExtend mUpdateTask;
    private TaskStaffManagement mTaskInfo;

    private CongViecRepository mCongViecRepository;

    @Override
    protected int getIdLayout() {
        return R.layout.activity_arising_task_detail;
    }

    @Override
    protected void initData() {
        try {
            mCongViecRepository = CongViecRepository.getInstance();
            viewOnly.set(getIntent().getBooleanExtra(Constants.BundleConstant.STOCK_VIEW_ONLY, true));
            mUpdateTask = getIntent().getParcelableExtra(Constants.BundleConstant.TASK_INFO);
            mTaskInfo = getIntent().getParcelableExtra(Constants.BundleConstant.ITEM_LIST);
            mPresenter = this;
            mBinding.setPresenter(mPresenter);

            if (!viewOnly.get())
                validateEndDate(mUpdateTask != null ? mUpdateTask.getEndDate() : mTaskInfo.getEndDate());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void validateEndDate(String strEndDate) {
        try {
            Date currentDate = new Date();
            Date endDate = DateUtils.stringToDate(strEndDate, DateUtils.TIMEZONE_FORMAT_SERVER, Locale.getDefault());

            if (endDate != null && endDate.before(currentDate)) {
                DialogUtils.showDialog(this, getString(R.string.common_msg_expired, getString(R.string.menu_task)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getStatusBg() {
        if (mUpdateTask != null) {
            switch (mUpdateTask.getStatusTaskStaff()) {
                case TaskShopManagement.TaskStatus.STATUS_NEW:
                    return Color.GRAY;
                case TaskShopManagement.TaskStatus.STATUS_ASSIGNED:
                    return Color.YELLOW;
                case TaskShopManagement.TaskStatus.STATUS_FINISHED:
                    return Color.GREEN;
                case TaskShopManagement.TaskStatus.STATUS_REJECTED:
                    return Color.RED;
            }
        } else if (mTaskInfo != null) {
            switch (mTaskInfo.getStatusTaskStaff()) {
                case TaskShopManagement.TaskStatus.STATUS_NEW:
                    return Color.GRAY;
                case TaskShopManagement.TaskStatus.STATUS_ASSIGNED:
                    return Color.YELLOW;
                case TaskShopManagement.TaskStatus.STATUS_FINISHED:
                    return Color.GREEN;
                case TaskShopManagement.TaskStatus.STATUS_REJECTED:
                    return Color.RED;
            }
        }
        return Color.WHITE;
    }

    public void onReject() {

        new CustomDialog(this, R.string.confirm,
                R.string.common_msg_confirm_reject, false, R.string.common_label_no,
                R.string.common_label_yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }, new CustomDialog.OnInputDialogListener() {
            @Override
            public void onClick(DialogInterface var1, int var2, String input) {
                updateTask(UpdateTaskRequest.TaskProgress.STATUS_TU_CHOI);
            }
        }, null, false, false).show();
    }

    public void onAccept() {
        new CustomDialog(this, R.string.confirm,
                R.string.common_msg_confirm_receive, false, R.string.common_label_no,
                R.string.common_label_yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }, new CustomDialog.OnInputDialogListener() {
            @Override
            public void onClick(DialogInterface var1, int var2, String input) {
                updateTask(UpdateTaskRequest.TaskProgress.STATUS_DONG_Y);
            }
        }, null, false, false).show();
    }

    private void updateTask(@UpdateTaskRequest.TaskProgress int progress) {
        showLoadingDialog();
        UpdateTaskRequest request = new UpdateTaskRequest();
        request.setTaskStaffMngtId(mUpdateTask != null ? mUpdateTask.getTaskStaffMngtId() : mTaskInfo.getTaskStaffMngtId().intValue());
        request.setType(String.valueOf(TaskShopManagement.TaskType.TYPE_PHAT_SINH));
        request.setProgress(progress);

        DataRequest<UpdateTaskRequest> dataRequest = new DataRequest<>();
        dataRequest.setWsCode(WsCode.UpdateTask);
        dataRequest.setWsRequest(request);

        mCongViecRepository.updateTask(dataRequest)
                .subscribe(new MBCCSSubscribe<UpdateTaskResponse>() {
                    @Override
                    public void onSuccess(UpdateTaskResponse object) {
                        hideLoadingDialog();
                        setResult(RESULT_OK);
                        finish();
                    }

                    @Override
                    public void onError(BaseException error) {
                        hideLoadingDialog();
                        DialogUtils.showDialogError(ArisingTaskDetailActivity.this, error);
                    }
                });
    }

    public String getTime() {
        if (mUpdateTask != null) {
            if (!TextUtils.isEmpty(mUpdateTask.getCreateDate())
                    && !TextUtils.isEmpty(mUpdateTask.getEndDate())) {
                return mUpdateTask.getCreateDate() + " - " + mUpdateTask.getEndDate();
            }
        } else if (mTaskInfo != null) {
            if (!TextUtils.isEmpty(mTaskInfo.getCreateDate())
                    && !TextUtils.isEmpty(mTaskInfo.getEndDate())) {
                return mTaskInfo.getCreateDate() + " - " + mTaskInfo.getEndDate();
            }
        }
        return "";
    }

    public String getCreateDate() {
        if (mUpdateTask != null) {
            if (!TextUtils.isEmpty(
                    mUpdateTask.getCreateDate()))
                return mUpdateTask.getCreateDate();
        } else if (mTaskInfo != null) {
            if (!TextUtils.isEmpty(
                    mTaskInfo.getCreateDate()))
                return mTaskInfo.getCreateDate();
        }
        return "";
    }

    public String getStatus() {
        if (mUpdateTask != null) {
            switch (mUpdateTask.getStatusTaskStaff()) {
                case TaskShopManagement.TaskStatus.STATUS_NEW:
                    return getResources().getStringArray(R.array.task_update_status)[0];
                case TaskShopManagement.TaskStatus.STATUS_ASSIGNED:
                    return getResources().getStringArray(R.array.task_update_status)[1];
                case TaskShopManagement.TaskStatus.STATUS_FINISHED:
                    return getResources().getStringArray(R.array.task_update_status)[2];
                case TaskShopManagement.TaskStatus.STATUS_REJECTED:
                    return getResources().getStringArray(R.array.task_update_status)[3];
            }
        } else if (mTaskInfo != null) {
            switch (mTaskInfo.getStatusTaskStaff()) {
                case TaskShopManagement.TaskStatus.STATUS_NEW:
                    return getResources().getStringArray(R.array.task_update_status)[0];
                case TaskShopManagement.TaskStatus.STATUS_ASSIGNED:
                    return getResources().getStringArray(R.array.task_update_status)[1];
                case TaskShopManagement.TaskStatus.STATUS_FINISHED:
                    return getResources().getStringArray(R.array.task_update_status)[2];
                case TaskShopManagement.TaskStatus.STATUS_REJECTED:
                    return getResources().getStringArray(R.array.task_update_status)[3];
            }
        }
        return "";
    }

    public String getStaffName() {
        return UserRepository.getInstance().getUserInfo().getStaffInfo().getStaffName();
    }

    public String getTaskName() {
        if (mUpdateTask != null) {
            return mUpdateTask.getJobName();
        } else if (mTaskInfo != null)
            return mTaskInfo.getJobName();
        return "";
    }

    public String getDescription() {
        if (mUpdateTask != null) {
            return mUpdateTask.getJobDescription();
        } else if (mTaskInfo != null)
            return mTaskInfo.getJobDescription();
        return "";
    }

    public void close() {
        finish();
    }
}
