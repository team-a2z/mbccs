package com.viettel.mbccs.screen.assigntask;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;

import com.viettel.mbccs.data.model.InfoTaskExtend;
import com.viettel.mbccs.data.model.TaskShopManagement;
import com.viettel.mbccs.screen.assigntask.arising.detail.ArisingTaskDetailActivity;
import com.viettel.mbccs.screen.assigntask.arising.update.CloseArisingTaskActivity;
import com.viettel.mbccs.variable.Constants;

/**
 * Created by Anh Vu Viet on 7/2/2017.
 */

public class ListUpdateTaskActivity extends BaseListTaskActivity<ListUpdateTaskPresenter> {

    @Override
    protected void initData() {
        mPresenter = new ListUpdateTaskPresenter(this, this);
        mBinding.setPresenter(mPresenter);
        mBinding.drawer.animateOpen();
        mBinding.swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.doSearch(true);
            }
        });
    }

    @Override
    public void onAddClick() {
    }

    @Override
    public void onItemClicked(Object object) {
        try {
            InfoTaskExtend taskStaffManagement = (InfoTaskExtend) object;
            Intent intent = null;

            switch (taskStaffManagement.getStatusTaskStaff()) {
                case TaskShopManagement.TaskStatus.STATUS_NEW:
                    intent = new Intent(this, ArisingTaskDetailActivity.class);
                    intent.putExtra(Constants.BundleConstant.STOCK_VIEW_ONLY, false);

                    break;
                case TaskShopManagement.TaskStatus.STATUS_ASSIGNED:
                    intent = new Intent(this, CloseArisingTaskActivity.class);

                    break;
                case TaskShopManagement.TaskStatus.STATUS_FINISHED:
                    intent = new Intent(this, ArisingTaskDetailActivity.class);
                    intent.putExtra(Constants.BundleConstant.STOCK_VIEW_ONLY, true);

                    break;
                case TaskShopManagement.TaskStatus.STATUS_REJECTED:
                    intent = new Intent(this, ArisingTaskDetailActivity.class);
                    intent.putExtra(Constants.BundleConstant.STOCK_VIEW_ONLY, true);

                    break;
            }

            if (intent != null) {
                intent.putExtra(Constants.BundleConstant.TASK_INFO, taskStaffManagement);
                startActivityForResult(intent, REQUEST_CODE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
