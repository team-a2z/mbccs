package com.viettel.mbccs.screen.assigntask;

import android.content.Context;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;

import com.viettel.mbccs.R;
import com.viettel.mbccs.base.filterdialog.DialogFilter;
import com.viettel.mbccs.base.searchlistview.BaseSearchListViewPresenter;
import com.viettel.mbccs.data.model.InfoTaskExtend;
import com.viettel.mbccs.data.model.TaskShopManagement;
import com.viettel.mbccs.data.model.TaskStaffManagement;
import com.viettel.mbccs.data.source.CongViecRepository;
import com.viettel.mbccs.data.source.UserRepository;
import com.viettel.mbccs.screen.assigntask.adapters.AssignTaskAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by Anh Vu Viet on 7/2/2017.
 */

public abstract class BaseListTaskPresenter<T> extends BaseSearchListViewPresenter<T, BaseListTaskContract.ViewModel> implements AssignTaskAdapter.OnTaskClickListener {

    public ObservableField<String> taskTypeText = new ObservableField<>();

    public ObservableField<String> taskStatusText = new ObservableField<>();


    public ObservableInt taskType = new ObservableInt() {
        @Override
        public void set(int value) {
            super.set(value);
            switch (value) {
                case TaskShopManagement.TaskType.TYPE_CSKPP:
                    taskTypeText.set(mContext.getString(R.string.task_cskpp));
                    break;
                case TaskShopManagement.TaskType.TYPE_PHAT_SINH:
                    taskTypeText.set(mContext.getString(R.string.task_arising));
                    break;
            }
        }
    };

    public ObservableInt taskStatus = new ObservableInt() {
        @Override
        public void set(int value) {
            super.set(value);
            try {
                taskStatusText.set(mTaskStatusList.get(value));
            } catch (IndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        }
    };

    protected CongViecRepository mRepository;

    protected CompositeSubscription mSubscription = new CompositeSubscription();

    protected UserRepository mUserRepository;

    protected List<String> mTaskTypeList = new ArrayList<>();

    protected List<String> mTaskStatusList;

    public BaseListTaskPresenter(Context context, BaseListTaskContract.ViewModel viewModel, int defaultType) {
        super(context, viewModel);

        mUserRepository = UserRepository.getInstance();

        mTaskTypeList = Arrays.asList(mContext.getResources().getStringArray(R.array.task_type));
        mTaskStatusList = Arrays.asList(getStatusArray());
        taskStatusText.set(mTaskStatusList.get(0));
        switch (defaultType) {
            case TaskShopManagement.TaskType.TYPE_CSKPP:
                taskType.set(TaskShopManagement.TaskType.TYPE_CSKPP);
                break;
            case TaskShopManagement.TaskType.TYPE_PHAT_SINH:
                taskType.set(TaskShopManagement.TaskType.TYPE_PHAT_SINH);
                break;
        }

        mRepository = CongViecRepository.getInstance();
    }

    @Override
    public void onSearchSuccess() {
        mViewModel.onSearchSuccess();
    }

    @Override
    public void onSearchFail() {

    }

    @Override
    public String getSearchHint() {
        return mContext.getString(R.string.search_task_hint);
    }

    @Override
    public String getToolbarTitle() {
        return mContext.getString(R.string.quan_ly_giao_viec);
    }

    @Override
    public void onBackPressed() {
        mViewModel.onBackPressed();
    }

    @Override
    public String getItemCountString() {
        return null;
    }

    public RecyclerView.ItemDecoration getItemDecoration() {
        return new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL);
    }

    @Override
    public void onAddClick() {
        mViewModel.onAddClick();
    }

    @Override
    public void onTaskClick(TaskStaffManagement task) {
        mViewModel.onItemClicked(task);
    }

    @Override
    public void onTaskClick(InfoTaskExtend task) {
        mViewModel.onItemClicked(task);
    }

    public void chooseTaskType() {
        DialogFilter<String> dialogFilter = new DialogFilter<>();
        dialogFilter.setTitle(mContext.getString(R.string.common_warehousware_lable_find_code_stock));
        dialogFilter.setData(mTaskTypeList);
        dialogFilter.setOnDialogFilterListener(new DialogFilter.onDialogFilterListener<String>() {
            @Override
            public void onItemSelected(int position, String s) {
                switch (position) {
                    case 0:
                        taskType.set(TaskShopManagement.TaskType.TYPE_CSKPP);
                        break;
                    case 1:
                        taskType.set(TaskShopManagement.TaskType.TYPE_PHAT_SINH);
                        break;
                    default:
                        break;
                }
            }
        });
        dialogFilter.show(((AppCompatActivity) mContext).getSupportFragmentManager(), "");
    }

    public void chooseTaskStatus() {
        DialogFilter<String> dialogFilter = new DialogFilter<>();
        dialogFilter.setTitle(mContext.getString(R.string.common_warehousware_lable_find_code_stock));
        dialogFilter.setData(mTaskStatusList);
        dialogFilter.setOnDialogFilterListener(new DialogFilter.onDialogFilterListener<String>() {
            @Override
            public void onItemSelected(int position, String s) {
                taskStatus.set(position);
            }
        });
        dialogFilter.show(((AppCompatActivity) mContext).getSupportFragmentManager(), "");
    }

    public abstract String[] getStatusArray();
}
