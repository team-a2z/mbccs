package com.viettel.mbccs.screen.main.fragments.main;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.viettel.mbccs.data.model.Dashboard;
import com.viettel.mbccs.databinding.ItemProgressCircleStatisticBinding;
import com.viettel.mbccs.databinding.ItemProgressStatisticBinding;
import com.viettel.mbccs.databinding.ItemTextComplexStatisticBinding;
import com.viettel.mbccs.databinding.ItemTextStatisticBinding;
import com.viettel.mbccs.utils.ActivityUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by FRAMGIA\vu.viet.anh on 16/05/2017.
 */

public class MainRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int TYPE_TEXT = 1;

    public static final int TYPE_PROGRESS_CIRCLE = 2;

    public static final int TYPE_PROGRESS_BAR = 3;

    public static final int TYPE_TEXT_COMPLEX = 4;

    private Context mContext;

    private List<Dashboard> mList = new ArrayList<>();

    public MainRecyclerAdapter(Context context, List<Dashboard> list) {
        mContext = context;
        mList = list;
    }

    @Override
    public int getItemViewType(int position) {
        return Integer.parseInt(mList.get(position).getDashBoardType());
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_PROGRESS_BAR:
                ItemProgressStatisticBinding bindingBar =
                        ItemProgressStatisticBinding.inflate(LayoutInflater.from(mContext), parent,
                                false);
                return new ProgressBarViewHolder(bindingBar);
            case TYPE_PROGRESS_CIRCLE:
                ItemProgressCircleStatisticBinding bindingCircle =
                        ItemProgressCircleStatisticBinding.inflate(LayoutInflater.from(mContext),
                                parent, false);
                return new ProgressCircleViewHolder(bindingCircle);
            case TYPE_TEXT_COMPLEX:
                ItemTextComplexStatisticBinding bindingComplex =
                        ItemTextComplexStatisticBinding.inflate(LayoutInflater.from(mContext),
                                parent, false);
                return new TextComplexViewHolder(bindingComplex);
            case TYPE_TEXT:
            default:
                ItemTextStatisticBinding bindingText =
                        ItemTextStatisticBinding.inflate(LayoutInflater.from(mContext), parent,
                                false);
                return new TextViewHolder(bindingText);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case TYPE_PROGRESS_BAR:
                ((ProgressBarViewHolder) holder).bind(mList.get(position));
                break;
            case TYPE_PROGRESS_CIRCLE:
                ((ProgressCircleViewHolder) holder).bind(mList.get(position));
                break;
            case TYPE_TEXT_COMPLEX:
                ((TextComplexViewHolder) holder).bind(mList.get(position));
                break;
            case TYPE_TEXT:
            default:
                ((TextViewHolder) holder).bind(mList.get(position));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class TextViewHolder extends RecyclerView.ViewHolder {
        private ItemTextStatisticBinding binding;

        public TextViewHolder(ItemTextStatisticBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(final Dashboard item) {
            binding.setTextTop(item.getDashBoardName());
            try {
                binding.setTextBottom(item.getValue().get(0).getValue());
            } catch (IndexOutOfBoundsException | NullPointerException e) {
                binding.setTextBottom("0");
            }
            binding.executePendingBindings();
            binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ActivityUtils.gotoMenu(itemView.getContext(), item.getFunctionCode());
                }
            });
        }
    }

    public class ProgressBarViewHolder extends RecyclerView.ViewHolder {
        private ItemProgressStatisticBinding binding;

        public ProgressBarViewHolder(ItemProgressStatisticBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(final Dashboard item) {
            binding.setTextTop(item.getDashBoardName());
            try {
                binding.setDone(Float.parseFloat(item.getValue().get(0).getValue()));
            } catch (IndexOutOfBoundsException | NullPointerException e) {
                binding.setDone(0.0f);
            }
            binding.executePendingBindings();
            binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ActivityUtils.gotoMenu(itemView.getContext(), item.getFunctionCode());
                }
            });
        }
    }

    public class ProgressCircleViewHolder extends RecyclerView.ViewHolder {
        private ItemProgressCircleStatisticBinding binding;

        public ProgressCircleViewHolder(ItemProgressCircleStatisticBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(final Dashboard item) {
            binding.setTextTop(item.getDashBoardName());
            try {
                binding.setDone(Float.parseFloat(item.getValue().get(0).getValue()));
            } catch (IndexOutOfBoundsException | NullPointerException e) {
                binding.setDone(0.0f);
            }
            binding.executePendingBindings();
            binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ActivityUtils.gotoMenu(itemView.getContext(), item.getFunctionCode());
                }
            });
        }
    }

    public class TextComplexViewHolder extends RecyclerView.ViewHolder {
        private ItemTextComplexStatisticBinding binding;

        public TextComplexViewHolder(ItemTextComplexStatisticBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(final Dashboard item) {
            // TODO: 8/21/2017 Fake item
            binding.setTextTop(item.getDashBoardName());
            binding.setTextTopColor(Color.BLUE);
            binding.setTextBottom("Còn phải thu");
            binding.setTextBottomColor(Color.RED);
            binding.executePendingBindings();
            binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ActivityUtils.gotoMenu(itemView.getContext(), item.getFunctionCode());
                }
            });
        }
    }
}
