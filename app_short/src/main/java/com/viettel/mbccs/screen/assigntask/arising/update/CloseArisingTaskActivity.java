package com.viettel.mbccs.screen.assigntask.arising.update;

import android.content.DialogInterface;

import com.viettel.mbccs.R;
import com.viettel.mbccs.base.BaseDataBindActivity;
import com.viettel.mbccs.config.Config;
import com.viettel.mbccs.constance.WsCode;
import com.viettel.mbccs.data.model.InfoTaskExtend;
import com.viettel.mbccs.data.model.TaskShopManagement;
import com.viettel.mbccs.data.source.CongViecRepository;
import com.viettel.mbccs.data.source.remote.request.CloseTaskRequest;
import com.viettel.mbccs.data.source.remote.request.DataRequest;
import com.viettel.mbccs.data.source.remote.request.UpdateTaskRequest;
import com.viettel.mbccs.data.source.remote.response.BaseException;
import com.viettel.mbccs.data.source.remote.response.CloseTaskResponse;
import com.viettel.mbccs.databinding.ActivityArisingTaskFinishBinding;
import com.viettel.mbccs.utils.DateUtils;
import com.viettel.mbccs.utils.DialogUtils;
import com.viettel.mbccs.utils.rx.MBCCSSubscribe;
import com.viettel.mbccs.variable.Constants;
import com.viettel.mbccs.widget.CustomDialog;

import java.util.Date;
import java.util.Locale;

/**
 * Created by FRAMGIA\vu.viet.anh on 17/07/2017.
 */

public class CloseArisingTaskActivity
        extends BaseDataBindActivity<ActivityArisingTaskFinishBinding, CloseArisingTaskActivity> {

    private InfoTaskExtend mTaskStaffManagement;

    private CongViecRepository mCongViecRepository;

    @Override
    protected int getIdLayout() {
        return R.layout.activity_arising_task_finish;
    }

    @Override
    protected void initData() {
        try {
            mCongViecRepository = CongViecRepository.getInstance();
            mTaskStaffManagement = getIntent().getParcelableExtra(Constants.BundleConstant.TASK_INFO);
            mPresenter = this;
            mBinding.setPresenter(mPresenter);

            validateEndDate(mTaskStaffManagement);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void validateEndDate(InfoTaskExtend task) {
        try {
            Date currentDate = new Date();
            Date endDate = DateUtils.stringToDate(task.getEndDate(), DateUtils.TIMEZONE_FORMAT_SERVER, Locale.getDefault());

            if (endDate != null && endDate.before(currentDate)) {
                DialogUtils.showDialog(this, getString(R.string.common_msg_expired, getString(R.string.menu_task)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void done() {

        try {

            new CustomDialog(this, R.string.confirm,
                    R.string.common_msg_confirm_close_task, false, R.string.common_label_no,
                    R.string.common_label_yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }, new CustomDialog.OnInputDialogListener() {
                @Override
                public void onClick(DialogInterface var1, int var2, String input) {
                    closeTask();
                }
            }, null, false, false).show();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void closeTask() {
        try {

            CloseTaskRequest request = new CloseTaskRequest();
            request.setTaskStaffMngtId(mTaskStaffManagement.getTaskStaffMngtId());
            request.setType(String.valueOf(TaskShopManagement.TaskType.TYPE_PHAT_SINH));
            request.setProgress(UpdateTaskRequest.TaskProgress.STATUS_DONG_Y);
            request.setHasNIMS(Config.hasNIMS);

            DataRequest<CloseTaskRequest> dataRequest = new DataRequest<>();
            dataRequest.setWsCode(WsCode.CloseTask);
            dataRequest.setWsRequest(request);

            mCongViecRepository.closeTask(dataRequest)
                    .subscribe(new MBCCSSubscribe<CloseTaskResponse>() {
                        @Override
                        public void onSuccess(CloseTaskResponse object) {
                            hideLoadingDialog();
                            setResult(RESULT_OK);
                            finish();
                        }

                        @Override
                        public void onError(BaseException error) {
                            hideLoadingDialog();
                            DialogUtils.showDialogError(CloseArisingTaskActivity.this, error);
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getFinishDate() {
        return DateUtils.getCurrentDate();
    }

    public String getDueDate() {
        if (mTaskStaffManagement != null && mTaskStaffManagement.getEndDate() != null) {
            return mTaskStaffManagement.getEndDate();
        }
        return "";
    }

    public String getDescription() {
        if (mTaskStaffManagement != null) return mTaskStaffManagement.getJobDescription();
        return "";
    }
}
