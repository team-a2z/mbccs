package com.viettel.mbccs.screen.assigntask;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.viettel.mbccs.R;
import com.viettel.mbccs.constance.WsCode;
import com.viettel.mbccs.data.model.TaskStaffManagement;
import com.viettel.mbccs.data.source.remote.request.DataRequest;
import com.viettel.mbccs.data.source.remote.request.GetTaskPrepareAssignStaffRequest;
import com.viettel.mbccs.data.source.remote.response.BaseException;
import com.viettel.mbccs.data.source.remote.response.GetTaskPrepareAssignStaffResponse;
import com.viettel.mbccs.screen.assigntask.adapters.AssignTaskAdapter;
import com.viettel.mbccs.utils.DateUtils;
import com.viettel.mbccs.utils.DialogUtils;
import com.viettel.mbccs.utils.rx.MBCCSSubscribe;

import java.util.Arrays;

import rx.Subscription;

/**
 * Created by Anh Vu Viet on 5/13/2017.
 */

public class ListAssignTaskPresenter extends BaseListTaskPresenter<TaskStaffManagement> implements
        BaseListTaskContract.Presenter {

    public ListAssignTaskPresenter(Context context, BaseListTaskContract.ViewModel viewModel, int defaultTaskType) {
        super(context, viewModel, defaultTaskType);
    }

    @Override
    public void doSearch(boolean showProgress) {
        long fromDate = mViewModel.getFromDate();
        long toDate = mViewModel.getToDate();
        if (fromDate > toDate) {
            if (fromDate > toDate) {
                DialogUtils.showDialog(mContext,
                        mContext.getString(R.string.common_msg_error_greater_fields2, mContext.getString(R.string.common_label_to_date), mContext.getString(R.string.common_label_from_date)));
            }
            return;
        }

        if (showProgress)
            mViewModel.showLoading();

        listData.clear();

        GetTaskPrepareAssignStaffRequest request = new GetTaskPrepareAssignStaffRequest();
        request.setShopId(String.valueOf(mUserRepository.getUserInfo().getShop().getShopId()));
        request.setType(String.valueOf(taskType.get()));
        request.setFromDate(DateUtils.convertDateToString(fromDate, DateUtils
                .TIMEZONE_FORMAT_SERVER));
        request.setToDate(DateUtils.convertDateToString(toDate, DateUtils.TIMEZONE_FORMAT_SERVER));
        request.setStaffId(String.valueOf(mUserRepository.getUserInfo().getStaffInfo().getStaffId()));
        request.setStatus(String.valueOf(taskStatus.get() - 1 < 0 ? null : taskStatus.get() - 1));

        DataRequest<GetTaskPrepareAssignStaffRequest> dataRequest = new DataRequest<>();
        dataRequest.setWsCode(WsCode.GetTaskPrepareAssignStaff);
        dataRequest.setWsRequest(request);

        Subscription subscription = mRepository.getTaskPrepareAssignStaff(dataRequest).subscribe
                (new MBCCSSubscribe<GetTaskPrepareAssignStaffResponse>() {
                    @Override
                    public void onSuccess(GetTaskPrepareAssignStaffResponse object) {
                        onSearchSuccess();
                        if (object != null && object.getLstInfoTaskExtend() != null
                                && !object.getLstInfoTaskExtend().isEmpty())
                            listData.addAll(object.getLstInfoTaskExtend());

                        adapter.get().notifyDataSetChanged();
                        itemCount.set(listData.size());
                        isEmpty.set(listData.isEmpty());

                        onAdapterChangeSize(itemCount.get());
                    }

                    @Override
                    public void onError(BaseException error) {
                        mViewModel.hideLoading();
                        mViewModel.showErrorDialog(error);
                    }
                });
        mSubscription.add(subscription);
    }

    @Override
    protected RecyclerView.Adapter getListAdapter() {
        AssignTaskAdapter adapter = new AssignTaskAdapter(mContext, listData, Arrays.asList(getStatusArray()));
        adapter.setOnTaskClickListener(this);
        return adapter;
    }

    @Override
    public boolean getShouldShowAdd() {
        return true;
    }

    @Override
    public String[] getStatusArray() {
        return mContext.getResources().getStringArray(R.array.task_status_test);
    }
}
