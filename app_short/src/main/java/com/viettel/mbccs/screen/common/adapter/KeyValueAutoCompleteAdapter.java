package com.viettel.mbccs.screen.common.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import com.google.android.gms.location.places.AutocompleteFilter;
import com.viettel.mbccs.R;
import com.viettel.mbccs.data.model.KeyValue;
import com.viettel.mbccs.data.source.UserRepository;
import com.viettel.mbccs.databinding.ItemKeyValueAutoCompleteBinding;

import java.util.ArrayList;
import java.util.List;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by minhnx on 7/14/17.
 */

public abstract class KeyValueAutoCompleteAdapter extends ArrayAdapter<KeyValue> implements Filterable {

    private static final int LIMIT_RESULT = 6;

    private List<KeyValue> mResultList;
    private AutocompleteFilter mPlaceFilter;
    private Context mContext;

    private OnItemClickListener listener;
    //    private SearchProductRepository searchProductRepository;
    private UserRepository userRepository;
    private CompositeSubscription mSubscriptions;

    public KeyValueAutoCompleteAdapter(Context context, AutocompleteFilter filter) {
        super(context, R.layout.item_key_value_auto_complete, R.id.text_key);
        mPlaceFilter = filter;
        mContext = context;

//        searchProductRepository = SearchProductRepository.getInstance();
        userRepository = UserRepository.getInstance();
        mSubscriptions = new CompositeSubscription();
    }

    @Override
    public int getCount() {
        return mResultList.size();
    }

    @Override
    public KeyValue getItem(int position) {
        return mResultList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

//        View row = inflater.inflate(R.layout.item_product_auto_complete, null, true);
        final KeyValue item = getItem(position);

        ItemKeyValueAutoCompleteBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_key_value_auto_complete, parent, false);
        ItemKeyValueAutoCompletePresenter presenter = new ItemKeyValueAutoCompletePresenter(mContext, item);

        binding.setItem(presenter);

        binding.llItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null)
                    listener.onItemClick(item);

            }
        });

        return binding.getRoot();
    }

    /**
     * Returns the filter for the current set of autocomplete results.
     */
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();

                List<KeyValue> filterData = new ArrayList<>();

                if (constraint != null) {
                    filterData = getAutocomplete(constraint);
                }

                results.values = filterData;
                if (filterData != null) {
                    results.count = filterData.size();
                } else {
                    results.count = 0;
                }

                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                if (results != null && results.count > 0) {
                    // The API returned at least one result, update the data.
                    mResultList = (ArrayList<KeyValue>) results.values;
                    notifyDataSetChanged();
                } else {
                    // The API did not return any results, invalidate the data set.
                    notifyDataSetInvalidated();
                }
            }

            @Override
            public CharSequence convertResultToString(Object resultValue) {
                // Override this method to display a readable result in the AutocompleteTextView
                // when clicked.
                if (resultValue instanceof KeyValue) {
                    return ((KeyValue) resultValue).getKey();
                } else {
                    return super.convertResultToString(resultValue);
                }
            }
        };
    }

    private List<KeyValue> getAutocomplete(CharSequence constraint) {
        try {
            onFilter(constraint);

            return new ArrayList<>();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(KeyValue item);
    }

    public abstract void onFilter(CharSequence key);

    public void setItems(List<KeyValue> items) {
        mResultList = items;
        notifyDataSetChanged();
    }
}
