package com.viettel.mbccs.screen.resetpass;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.os.Handler;
import android.text.TextUtils;

import com.viettel.mbccs.R;
import com.viettel.mbccs.constance.WsCode;
import com.viettel.mbccs.data.model.UserInfo;
import com.viettel.mbccs.data.source.UserRepository;
import com.viettel.mbccs.data.source.remote.request.DataRequest;
import com.viettel.mbccs.data.source.remote.request.UserPassRequest;
import com.viettel.mbccs.data.source.remote.response.BaseException;
import com.viettel.mbccs.data.source.remote.response.DataResponse;
import com.viettel.mbccs.utils.Common;
import com.viettel.mbccs.utils.DialogUtils;
import com.viettel.mbccs.utils.StringUtils;
import com.viettel.mbccs.utils.ValidateUtils;
import com.viettel.mbccs.utils.rx.CustomMBCCSSubscribe;
import com.viettel.mbccs.utils.rx.MBCCSSubscribe;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by FRAMGIA\bui.dinh.viet on 16/05/2017.
 */

public class ResetPasswordPresenter implements ResetPasswordContract.Presenter {
    private ResetPasswordContract.ViewModel mViewModel;
    private Context mContext;
    public ObservableField<String> description;
    public ObservableBoolean isCodeSent;
    public ObservableBoolean isNeedVerify;
    public ObservableBoolean isOTPVerified;
    public ObservableBoolean isPassChanged;
    public ObservableBoolean isForgotPw;
    private CompositeSubscription mSubscription;
    public ObservableField<String> phone;
    public ObservableField<String> errorPhone = new ObservableField<>();
    public ObservableField<String> passwordOld = new ObservableField<>();
    public ObservableField<String> errorPasswordOld = new ObservableField<>();
    public ObservableField<String> passwordNew = new ObservableField<>();
    public ObservableField<String> errorPasswordNew = new ObservableField<>();
    public ObservableField<String> rePassword = new ObservableField<>();
    public ObservableField<String> errorRePassword = new ObservableField<>();
    public ObservableField<String> codeVerify;
    public ObservableField<String> titleActivity;
    public ObservableBoolean isPassOldStateHide;
    public ObservableBoolean isPassNewStateHide;
    private UserInfo mUserInfo;
    private DataRequest<UserPassRequest> mDataRequest = new DataRequest<>();
    private UserPassRequest mRequest = new UserPassRequest();

    public ResetPasswordPresenter(ResetPasswordContract.ViewModel viewModel, Context context) {
        mViewModel = viewModel;
        mContext = context;
        initFields();
    }

    private void initFields() {
        mSubscription = new CompositeSubscription();
        description = new ObservableField<>();
        codeVerify = new ObservableField<>();
        titleActivity = new ObservableField<>();
        isCodeSent = new ObservableBoolean();
        isPassOldStateHide = new ObservableBoolean();
        isPassNewStateHide = new ObservableBoolean();
        isNeedVerify = new ObservableBoolean();
        isOTPVerified = new ObservableBoolean();
        isPassChanged = new ObservableBoolean();
        isForgotPw = new ObservableBoolean();
        phone = new ObservableField<>();
        description.set(mContext.getString(R.string.description_reset_password));
        titleActivity.set(isForgotPw.get() ? mContext.getString(R.string.title_reset_password)
                : mContext.getString(R.string.title_input_password));
        isPassOldStateHide.set(true);
        isPassNewStateHide.set(true);
        mUserInfo = UserRepository.getInstance().getUserInfo();
    }

    @Override
    public void subscribe() {
    }

    @Override
    public void unSubscribe() {
        mSubscription.clear();
    }

    @Override
    public void sendCodeClick() {
        if (!isValidPhone()) return;
        mViewModel.showLoading();
        mRequest.setUsername(phone.get());
        mDataRequest.setWsCode(WsCode.ForgotPassword);
        mDataRequest.setWsRequest(mRequest);
        Subscription subscription = UserRepository.getInstance()
                .forgotPassword(mDataRequest)
                .subscribe(new MBCCSSubscribe<DataResponse>() {
                    @Override
                    public void onSuccess(DataResponse object) {
                        isCodeSent.set(true);
                        titleActivity.set(mContext.getString(R.string.title_get_password_again));
                        description.set(String.format(mContext.getString(R.string.we_sent_code),
                                StringUtils.changeTextToBold(phone.get())));
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                isNeedVerify.set(true);
                            }
                        }, 3000);
                    }

                    @Override
                    public void onError(BaseException error) {
                        DialogUtils.showDialogError(mContext, error);
                    }

                    @Override
                    public void onRequestFinish() {
                        mViewModel.hideLoading();
                    }
                });
        mSubscription.add(subscription);
    }

    public void resendCodeOTP() {
        mViewModel.showLoading();
        Subscription subscription = UserRepository.getInstance()
                .forgotPassword(mDataRequest)
                .subscribe(new MBCCSSubscribe<DataResponse>() {
                    @Override
                    public void onSuccess(DataResponse object) {

                    }

                    @Override
                    public void onError(BaseException error) {
                        DialogUtils.showDialogError(mContext, error);
                    }

                    @Override
                    public void onRequestFinish() {
                        mViewModel.hideLoading();
                    }
                });
        mSubscription.add(subscription);
    }

    public void sendPassToSMS() {
        mRequest.setUsername(phone.get());
        mRequest.setOtp(codeVerify.get());
        mDataRequest.setWsRequest(mRequest);
        mViewModel.showLoading();
        Subscription subscription = UserRepository.getInstance()
                .forgotPassword(mDataRequest)
                .subscribe(new MBCCSSubscribe<DataResponse>() {
                    @Override
                    public void onSuccess(DataResponse object) {
                        isOTPVerified.set(true);
                        titleActivity.set(
                                mContext.getString(R.string.title_input_password_success));
                        description.set(String.format(mContext.getString(R.string.we_sent_new_pass),
                                StringUtils.changeTextToBold(phone.get())));
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                onBackClick();
                            }
                        }, 3000);
                    }

                    @Override
                    public void onError(BaseException error) {
                        DialogUtils.showDialogError(mContext, error);
                    }

                    @Override
                    public void onRequestFinish() {
                        mViewModel.hideLoading();
                    }
                });
        mSubscription.add(subscription);
    }

    @Override
    public void changePassword(String password) {

    }

    @Override
    public void createNewPass() {

        if (!isValidPass()) return;

        mRequest.setUsername(mUserInfo.getStaffInfo().getStaffCode());
        mRequest.setPassold(passwordOld.get());
        mRequest.setPassnew(passwordNew.get());

        mViewModel.showLoading();
        Subscription subscription = UserRepository.getInstance()
                .changePassword(mRequest)
                .subscribe(new CustomMBCCSSubscribe<DataResponse>() {
                    @Override
                    public void onSuccess(DataResponse object) {
                        isPassChanged.set(true);
//                        mViewModel.showError(mContext.getString(R.string.change_password_msg_old_pass_incorrect));
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                onBackClick();
                            }
                        }, 3000);
                    }

                    @Override
                    public void onError(BaseException error) {
                        DialogUtils.showDialogError(mContext, error);
                    }

                    @Override
                    public void onRequestFinish() {
                        mViewModel.hideLoading();
                    }
                });
        mSubscription.add(subscription);
    }

    private boolean isValidPass() {
        //Validation Password field
        resetAllErrorPw();
        if (ValidateUtils.getErrorPass(passwordOld.get()) != null) {
            errorPasswordOld.set(ValidateUtils.getErrorPass(passwordOld.get()));
            return TextUtils.isEmpty(errorPasswordOld.get());
        }
        if (ValidateUtils.getErrorPass(passwordNew.get()) != null) {
            errorPasswordNew.set(ValidateUtils.getErrorPass(passwordNew.get()));
            return TextUtils.isEmpty(errorPasswordNew.get());
        }
        if (ValidateUtils.getErrorMatching2Pass(passwordNew.get(), rePassword.get()) != null) {
            errorRePassword.set(
                    ValidateUtils.getErrorMatching2Pass(passwordNew.get(), rePassword.get()));
        }
        return TextUtils.isEmpty(errorRePassword.get());
    }

    private void resetAllErrorPw() {
        errorPasswordOld.set("");
        errorPasswordNew.set("");
        errorRePassword.set("");
    }

    private boolean isValidPhone() {
        if (TextUtils.isEmpty(phone.get())) {
            errorPhone.set(mContext.getResources().getString(R.string.input_empty));
            return false;
        }
        if (!ValidateUtils.isPhoneNumber(phone.get())) {
            errorPhone.set(mContext.getResources().getString(R.string.not_phone));
            return false;
        }
        return true;
    }

    public void onBackClick() {
        if (isPassChanged.get()) {
            Common.goToLogin(mContext);
            return;
        }
        mViewModel.onBackClick();
    }

    public void eyePwOldClick() {
        isPassOldStateHide.set(!isPassOldStateHide.get());
    }

    public void eyePwNewClick() {
        isPassNewStateHide.set(!isPassNewStateHide.get());
    }
}
