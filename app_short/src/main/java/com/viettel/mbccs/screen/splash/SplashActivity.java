package com.viettel.mbccs.screen.splash;

import android.content.Intent;
import android.os.Build;
import android.support.v4.app.ActivityCompat;

import com.viettel.mbccs.R;
import com.viettel.mbccs.base.BaseActivity;
import com.viettel.mbccs.config.Config;
import com.viettel.mbccs.data.source.UserRepository;
import com.viettel.mbccs.screen.login.LoginActivity;
import com.viettel.mbccs.screen.main.MainActivity;
import com.viettel.mbccs.utils.LocaleUtils;

/**
 * Created by Anh Vu Viet on 5/17/2017.
 */

public class SplashActivity extends BaseActivity implements SplashContract.View {

    private SplashPresenter presenter;
    private UserRepository userRepository;

    @Override
    protected int getIdLayout() {
        return R.layout.activity_splash;
    }

    @Override
    protected void initData() {
        initDataBinder();
        presenter = new SplashPresenter(this, this);
        presenter.subscribe();
        presenter.checkDataProvince();

        userRepository = UserRepository.getInstance();

        initDefaultLanguage();
    }

    private void initDefaultLanguage() {
        try {

            if (userRepository.getLanguageFromSharePrefs() == null) {

                String languageCode = Config.DEFAULT_LANGUAGE;

                userRepository.saveLanguageToSharePrefs(languageCode);

                LocaleUtils.setLocale(this, languageCode);

                Intent refresh = new Intent(this, SplashActivity.class);
                startActivity(refresh);
                if (Build.VERSION.SDK_INT >= 16) {
                    finishAffinity();
                } else {
                    ActivityCompat.finishAffinity(this);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void gotoLogin() {
        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
        finish();
    }

    @Override
    public void gotoMain() {
        startActivity(new Intent(SplashActivity.this, MainActivity.class));
        //open1();
        finish();
    }

    /***
     * demo xuat kho
     */
    void open() {
//        Intent intent = new Intent(SplashActivity.this, ChiTietXuatKhoNhanVienActivity.class);
//        StockTrans stockTrans = new StockTrans();
//        stockTrans.setStockTransId(1237);
//        stockTrans.setToOwnerId(1232);
//        stockTrans.setCreateDateTime("2017-01-02");
//        stockTrans.setStockTransStatusName("hang moi");
//        Bundle bundle = new Bundle();
//        bundle.putParcelable(Constants.BundleConstant.STOCK_TRANS, stockTrans);
//        intent.putExtras(bundle);
//        startActivity(intent);
    }

    /***
     * Demo xuat tra hang
     */
    void open1() {
//        Intent intent = new Intent(SplashActivity.this, LapPhieuXuatTraHangActivity.class);
        //        StockTrans stockTrans = new StockTrans();
        //        stockTrans.setStockTransId(1237);
        //        stockTrans.setToOwnerId(1232);
        //        stockTrans.setCreateDateTime("2017-01-02");
        //        stockTrans.setStockTransStatusName("hang moi");
        //        Bundle bundle = new Bundle();
        //        bundle.putParcelable(Constants.BundleConstant.STOCK_TRANS, stockTrans);
        //        intent.putExtras(bundle);
//        startActivity(intent);
    }

    /***
     * demo nhap kho
     */
    void open2() {
//        Intent intent = new Intent(SplashActivity.this, NhapKhoCapDuoiDemo.class);
//        StockTrans stockTrans = new StockTrans();
//        stockTrans.setStockTransId(1237);
//        stockTrans.setToOwnerId(1232);
//        stockTrans.setCreateDateTime("2017-01-02");
//        stockTrans.setStockTransStatusName("hang moi");
//        Bundle bundle = new Bundle();
//        bundle.putParcelable(Constants.BundleConstant.STOCK_TRANS, stockTrans);
//        intent.putExtras(bundle);
//        startActivity(intent);
    }

    @Override
    public void gotoDownloadImage() {
        startActivity(new Intent(SplashActivity.this, DownloadDataActivity.class));
        finish();
    }
}
