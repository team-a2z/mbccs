package com.viettel.mbccs.screen.assigntask.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.viettel.mbccs.R;
import com.viettel.mbccs.data.model.InfoTaskExtend;
import com.viettel.mbccs.data.model.TaskShopManagement;
import com.viettel.mbccs.databinding.ItemAssignTaskBinding;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Da Qiao on 7/16/2017.
 */

public class UpdateTaskAdapter
        extends RecyclerView.Adapter<UpdateTaskAdapter.UpdateTaskViewHolder> {

    private List<InfoTaskExtend> mList = new ArrayList<>();

    private Context mContext;

    private AssignTaskAdapter.OnTaskClickListener mOnTaskClickListener;

    public UpdateTaskAdapter(Context context, List<InfoTaskExtend> list) {
        mContext = context;
        mList = list;
    }

    @Override
    public UpdateTaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new UpdateTaskViewHolder(
                ItemAssignTaskBinding.inflate(LayoutInflater.from(mContext), parent, false));
    }

    @Override
    public void onBindViewHolder(UpdateTaskViewHolder holder, int position) {
        holder.bind(mList.get(position));
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public AssignTaskAdapter.OnTaskClickListener getOnTaskClickListener() {
        return mOnTaskClickListener;
    }

    public void setOnTaskClickListener(AssignTaskAdapter.OnTaskClickListener onTaskClickListener) {
        mOnTaskClickListener = onTaskClickListener;
    }

    class UpdateTaskViewHolder extends RecyclerView.ViewHolder {

        private ItemAssignTaskBinding mBinding;

        public UpdateTaskViewHolder(ItemAssignTaskBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        public void bind(final InfoTaskExtend item) {
            mBinding.setTaskTitle(item.getJobName());
            mBinding.setTaskDescription(item.getJobDescription());
            mBinding.setCreatedDate(item.getCreateDate());
            switch (item.getStatusTaskStaff()) {
                case TaskShopManagement.TaskStatus.STATUS_NEW:
                    mBinding.setTaskStatus(
                            itemView.getResources().getString(R.string.not_accepted));
                    mBinding.setStatusBackground(Color.GRAY);
                    mBinding.setStatusBackgroundLight(false);
                    break;
                case TaskShopManagement.TaskStatus.STATUS_ASSIGNED:
                    mBinding.setTaskStatus(itemView.getResources().getString(R.string.inprogress));
                    mBinding.setStatusBackground(Color.YELLOW);
                    mBinding.setStatusBackgroundLight(true);
                    break;
                case TaskShopManagement.TaskStatus.STATUS_FINISHED:
                    mBinding.setTaskStatus(itemView.getResources().getString(R.string.finished));
                    mBinding.setStatusBackground(Color.GREEN);
                    mBinding.setStatusBackgroundLight(true);
                    break;
                case TaskShopManagement.TaskStatus.STATUS_REJECTED:
                    mBinding.setTaskStatus(itemView.getResources().getString(R.string.reject));
                    mBinding.setStatusBackground(Color.RED);
                    mBinding.setStatusBackgroundLight(false);
                    break;
            }
            mBinding.setOnClicked(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnTaskClickListener != null) mOnTaskClickListener.onTaskClick(item);
                }
            });
        }
    }
}
