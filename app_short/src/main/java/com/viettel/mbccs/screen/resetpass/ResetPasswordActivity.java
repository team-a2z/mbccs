package com.viettel.mbccs.screen.resetpass;

import android.widget.Toast;

import com.viettel.mbccs.R;
import com.viettel.mbccs.base.BaseDataBindActivity;
import com.viettel.mbccs.databinding.ActivityResetPasswordBinding;
import com.viettel.mbccs.variable.Constants;

/**
 * Created by FRAMGIA\bui.dinh.viet on 16/05/2017.
 */

public class ResetPasswordActivity
        extends BaseDataBindActivity<ActivityResetPasswordBinding, ResetPasswordPresenter>
        implements ResetPasswordContract.ViewModel {

    @Override
    public void showLoading() {
        showLoadingDialog();
    }

    @Override
    public void hideLoading() {
        hideLoadingDialog();
    }

    @Override
    public void changePasswordSuccess() {
        //TODO handle change password success
    }

    @Override
    public void onBackClick() {
        finish();
    }

    @Override
    protected int getIdLayout() {
        return R.layout.activity_reset_password;
    }

    @Override
    protected void initData() {
        mPresenter = new ResetPasswordPresenter(this, this);
        mBinding.setPresenter(mPresenter);
        mPresenter.subscribe();
        mPresenter.isForgotPw.set(isForgotPw());
    }

    private boolean isForgotPw() {
        return getIntent().getBooleanExtra(Constants.BundleConstant.EXTRA_FORGOT_PW, false);
    }

    @Override
    public void showError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }
}
