package com.viettel.mbccs.constance;

import android.support.annotation.StringDef;

/**
 * Created by nguyenhuyquyet on 9/18/17.
 */

@StringDef({SystemCode.CM_POS, SystemCode.CM_PRE})
public @interface SystemCode {
    String CM_POS = "CM_POS";
    String CM_PRE = "CM_PRE";
}
