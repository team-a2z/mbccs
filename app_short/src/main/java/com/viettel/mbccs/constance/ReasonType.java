package com.viettel.mbccs.constance;

import android.support.annotation.StringDef;

/**
 * Created by HuyQuyet on 6/14/17.
 */

@StringDef({ReasonType.HUY_DON_HANG, ReasonType.REG_CANCEL_VAS, ReasonType.EXP_IMP_GOODS, ReasonType.REG_INSTALLATION_ADDRESS })
public @interface ReasonType {
    String HUY_DON_HANG = "REJECT_ORDER";
    String REG_CANCEL_VAS = "REG_CANCEL_VAS";//TODO minhnx
    String EXP_IMP_GOODS = "STOCK_EXP_UNDER";
    String REG_INSTALLATION_ADDRESS = "513";
}
