package com.viettel.mbccs.constance;

import android.support.annotation.StringDef;

/**
 * Created by HuyQuyet on 5/16/17.
 */

@StringDef({ReturnMoneyStatus.APPROVALS, ReturnMoneyStatus.PENDING, ReturnMoneyStatus.REJECT})
public @interface ReturnMoneyStatus {
    String PENDING = "1";
    String APPROVALS = "2";
    String REJECT = "3";
}
