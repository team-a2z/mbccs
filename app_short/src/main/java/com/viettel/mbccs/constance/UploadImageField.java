package com.viettel.mbccs.constance;

import android.support.annotation.StringDef;

import com.viettel.mbccs.data.model.database.UploadImage;

/**
 * Created by nguyenhuyquyet on 7/29/17.
 */
@StringDef({UploadImageField.IMAGE_NAME, UploadImageField.CONTENT, UploadImageField.IMAGE_TYPE,
        UploadImageField.CUST_ID, UploadImageField.STATUS})
public @interface UploadImageField {
    String IMAGE_NAME = "image_name";
    String CONTENT = "content";
    String IMAGE_TYPE = "imageType";
    String CUST_ID = "custId";
    String STATUS = "status";
}
