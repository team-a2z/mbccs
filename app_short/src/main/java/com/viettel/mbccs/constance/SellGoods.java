package com.viettel.mbccs.constance;

/**
 * Created by minhnx on 8/26/17.
 */

public @interface SellGoods {
    int SHOW_SALE_PROGRAM = 1;
    int HIDE_SALE_PROGRAM = 2;
}
