package com.viettel.mbccs.constance;

/**
 * Created by FRAMGIA\bui.dinh.viet on 11/09/2017.
 */

public @interface SaleTranStatus {
    int SALE_TRANS_STATUS_NOT_BILLED = 2;
    int SALE_TRANS_STATUS_BILLED = 3;
    int SALE_TRANS_STATUS_CANCEL = 4;
}
