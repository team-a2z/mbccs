package com.viettel.mbccs.config;

import com.viettel.mbccs.constance.STEP_WAREHOUSE;
import com.viettel.mbccs.constance.SellGoods;

/**
 * Created by eo_cuong on 5/10/17.
 */

public class Config {
    public static final String END_POINT = "http://197.218.5.24:8123";
    public static final String END_POINT_RESETPW = "http://197.218.5.24:8123";
    public static final String API_KEY = "308FD5F2C770DE0356150AFD667D90FB";
    public static final String PREFIX = "258";
    public static final String APPCODE = "mbccs";
    public static final String hasNIMS = "0";
    public static final long ENVIROMENT_STEP = STEP_WAREHOUSE.STEP_3;
    public static final int TUOI = 15;
    public static final String DATE_FORMAT = "dd/MM/yyyy";
    public static final int MAX_IMAGE_WITH_COMPRESS = 640;
    public static final int MAX_IMAGE_HEIGHT_COMPRESS = 480;
    public static final int IMAGE_COMPRESS_QUANTITY = 75;
    public static final long STAFF_EXPORT_SHOP_REASON_ID = 200383L;
    public static final int SELL_GOODS_SHOW_PROGRAM = SellGoods.HIDE_SALE_PROGRAM;
    public static final int NUMBER_SELECT_IMAGE = 3;
    public static final String COUNTRY_NAME = "Mozambique";
    public static final String DB_AREAS_FILE_NAME = "db_area_seh.json";
    public static final String DEFAULT_LANGUAGE = "eng";
    public static final String DEFAULT_CURRENCY = "VND";
}
